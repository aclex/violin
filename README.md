Violin — Virtual instrument wrapper for GNU/Linux
=====================================================
Violin is a thin little wrapping bridge for running modules of [Steinberg VST format](https://github.com/steinbergmedia/vst3sdk/) compiled for Microsoft Windows OS in GNU/Linux environment using [Wine](https://winehq.org).

![logo](logo.svg)

Why?
====

It's basically a great [chicken or the egg](https://en.wikipedia.org/wiki/Chicken_or_the_egg) problem, that many of professional level DAW's, studios, applications and modules are written for other operating systems due to low popularity of audio production in GNU/Linux, while this popularity doesn't raise, as there're lack of proper tools… Though the picture slowly change and there are very high quality suites like [Ardour](http://ardour.org/) or [Bitwig](https://bitwig.com) and others available now, both free and proprietary, this is not really the case for a world of modules and plugins. The irony is that these pieces might be just as important in the production process, as the studios themselves.

To address this particular problem wrappers and bridges appear, allowing to run modules compiled for other operating systems as if it was native GNU/Linux ones. While it might look like a relief, two main challenges are generally perpetual with this approach: compatibility issues and performance penalty. These are what to be primarily addressed by all the wrapper implementations.

Why another wrapper?
====================

Although most of the wrappers do roughly the same job, the differences are in small implementation details. Violin was started as a modern C++ implementation summarizing all the recent programming approaches in relevant areas. Here are some of them:

* Use `pthread`'s process shared threading primitives for synchronizing between processes and preserving the better IPC performance
* Get rid of busy waiting-based synchronization from bad old days, reacting on the events instead
* Prefer the recommended interaction protocols (presumably on WinAPI side) instead of delay-driven parts
* Facilitate modern C++ template features to do as much work, as possible at compile time, simplify the code and reduce its size
* Implement an accurate shared memory management based on predefined message and buffer structures
* Use fresh C++17's `std::variant`-based IPC messages to avoid manual serializing and deserializing operations

What are the alternatives?
==========================

There are probably more of them, but here are some popular implementations:

- [Carla](https://github.com/falkTX/Carla)
- [Airwave](https://github.com/psycha0s/airwave/)
- [LinVst](https://github.com/osxmidi/linvst)

Protocols supported
===================

Violin supports only VST2 protocol at the moment with VST3 support in plan.

Dependencies
============

Violin needs the following to be built and work properly:
- `pthread`-compliant threading library
- `pkg-config` for configuration process (to find XCB)
- Gettext for message translations 
- Wine with 64-bit mode supported (a.k.a. `wine64`)
- XCB to interact with X server

How to build
============

Violin uses CMake as a build system, so building steps are common for CMake-based projects. Out of project tree do the following:
```shell
mkdir build && cd build
cmake ../
make
```

How to install
==============

By default installation prefix is `/usr/local` — use `CMAKE_INSTALL_PREFIX` variable to change it:
```shell
cmake -DCMAKE_INSTALL_PREFIX=<another-installation-prefix> ../
```

After the successful compilation, just issue under the root shell `make install` (or usually `sudo make install` as a normal user).

How to use
==========

Violin needs a pre-created wrapping shared object to work properly. This object should be placed in the directory with your GNU/Linux VST plugins being scanned by your DAW (please, consult its documentation to find the exact path).

### Creating a wrapping link

Use `violin-link` utility to create a wrapping link between your original Windows VST plugin, like this:

```shell
violin-link ~/.wine/c/Program\ Files/VstPlugins/plugin.dll /usr/lib/vst
```

In this case it will create `/usr/lib/vst/plugin.dll.so` wrapping shared object linked to an original Windows VST plugin at `~/.wine/c/Program Files/VstPlugins/plugin.dll`.

### Removing a link

To remove an existing link use `violin-unlink` with either wrapping shared object path or original Windows VST plugin path. Don't worry, the DLL itself won't ever be touched, just a wrapping link to it is removed.

### List registered links

Just issue `violin-list` to see what's already registered.
