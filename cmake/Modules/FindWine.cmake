# (cc) 2020, under Creative commons CC0 license

# It only searches for Wine executable and compiler wrappers, without
# trying to find include and library directories, as any Wine program
# compilation would end up using Wine compilers, which have all this
# properly set up, anyway.

include(FindPackageHandleStandardArgs)

find_program(Wine_C_COMPILER NAMES winegcc winegcc-stable)
find_program(Wine_CXX_COMPILER NAMES wineg++ wineg++-stable)

set(Wine_ARCH WIN64)
if (${CMAKE_SIZEOF_VOID_P} STREQUAL "4")
	set(Wine_ARCH WIN32)
endif()

if (Wine_FIND_COMPONENTS)
	list(GET Wine_FIND_COMPONENTS 0 Wine_ARCH)
endif()

if (Wine_ARCH STREQUAL "WIN32")
	find_program(Wine_EXECUTABLE NAMES wine)
else()
	find_program(Wine_EXECUTABLE NAMES wine64)
endif()

find_package_handle_standard_args(Wine DEFAULT_MSG Wine_EXECUTABLE Wine_C_COMPILER Wine_CXX_COMPILER)

