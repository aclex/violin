/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "name.h"

using namespace std;

using namespace violin;

int main(int, char**)
{
	const vector<string> result_skipping(split("foo, bar, ", ", "));
	const vector<string> expected_skipping { "foo", "bar" };
	if (result_skipping != expected_skipping)
	{
		cout << "control (skipping empty tokens): got: { ";
		for (auto i = 0; i < result_skipping.size(); ++i)
		{
			cout << '"' << result_skipping.at(i) << '"';

			if (i != result_skipping.size() - 1)
			{
				cout << ", ";
			}
		}

		cout << " }, expected_skipping: {";

		for (auto i = 0; i < expected_skipping.size(); ++i)
		{
			cout << '"' << expected_skipping.at(i) << '"';

			if (i != expected_skipping.size() - 1)
			{
				cout << ", ";
			}
		}

		cout << " }" << endl;
		return 1;
	}

	const vector<string> result_preserving(split("foo, bar, ", ", ", false));
	const vector<string> expected_preserving { "foo", "bar", "" };
	if (result_preserving != expected_preserving)
	{
		cout << "control (preserving empty tokens): got: { ";
		for (auto i = 0; i < result_preserving.size(); ++i)
		{
			cout << '"' << result_preserving.at(i) << '"';

			if (i != result_preserving.size() - 1)
			{
				cout << ", ";
			}
		}

		cout << " }, expected_preserving: {";

		for (auto i = 0; i < expected_preserving.size(); ++i)
		{
			cout << '"' << expected_preserving.at(i) << '"';

			if (i != expected_preserving.size() - 1)
			{
				cout << ", ";
			}
		}

		cout << " }" << endl;
		return 1;
	}

	return 0;
}
