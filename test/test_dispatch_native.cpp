/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include "vst_header_chooser.h"

#include "plugin/plugin.h"
#include "plugin/native_starter.h"

using namespace std;
using namespace std::chrono;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<native_starter> plugin;

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	intptr_t audio_master_callback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
	{
		cout << "audio_master_callback(" << opcode << ", " << index << ", " << value << ", " << ptr << ", " << opt << ')' << endl;

		return true;
	}

	void test_can_do(AEffect* ef)
	{
		string can_do_string;

		can_do_string = "coffee"s;
		const auto can_do_coffee { ef->dispatcher(ef, effCanDo, 0, 0, can_do_string.data(), 0.0f) };

		if (can_do_coffee != 0)
		{
			const string msg { "can_do_coffee: "s + to_string(can_do_coffee) + ", expected: "s + '0' };
			throw runtime_error(msg);
		}

		can_do_string = "tea"s;
		const auto can_do_tea { ef->dispatcher(ef, effCanDo, 0, 0, can_do_string.data(), 0.0f) };

		if (can_do_tea != 1)
		{
			const string msg { "can_do_tea: "s + to_string(can_do_tea) + ", expected: "s + '1' };
			throw runtime_error(msg);
		}

		can_do_string = "juice"s;
		const auto can_do_juice { ef->dispatcher(ef, effCanDo, 0, 0, can_do_string.data(), 0.0f) };

		if (can_do_juice != -1)
		{
			const string msg { "can_do_juice: "s + to_string(can_do_juice) + ", expected: "s + "-1" };
			throw runtime_error(msg);
		}
	}

	void test_unsupported(AEffect* ef)
	{
#ifndef USE_VST2SDK
		if (ef->dispatcher(ef, effBeginLoadBank, 0, 0, nullptr, 0.0f) != 0)
		{
			throw runtime_error("effBeginLoadBank somehow returns");
		}

		if (ef->dispatcher(ef, effBeginLoadProgram, 0, 0, nullptr, 0.0f) != 0)
		{
			throw runtime_error("effBeginLoadProgram somehow returns");
		}
#endif
	}
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(&audio_master_callback, string(CMAKE_BINARY_DIR) + "/test/mock/host_native/mock_host_native"), &deleter);

	auto* const ef(p->effect());

	ef->dispatcher(ef, effOpen, 0, 0, nullptr, 0.0f);
	ef->dispatcher(ef, effSetSampleRate, 0, 0, nullptr, 44100);
	ef->dispatcher(ef, effSetBlockSize, 0, 256, nullptr, 0.0f);
	ef->dispatcher(ef, effMainsChanged, 0, 1, nullptr, 0.0f);

	test_can_do(ef);
	test_unsupported(ef);

	return 0;
}
