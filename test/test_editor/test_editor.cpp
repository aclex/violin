/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include "vst_header_chooser.h"

#include <xcb/xcb.h>

#include "plugin/plugin.h"
#include "plugin/wine_starter.h"

#include "opcode.h"
#include "limit.h"

using namespace std;
using namespace std::chrono;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<wine_starter> plugin;

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	intptr_t audio_master_callback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
	{
		cout << "audio_master_callback(" << violin::vst2::master_opcodes.at(opcode) << ", " << index << ", " << value << ", " << ptr << ", " << opt << ')' << endl;

		if (opcode == audioMasterGetTime)
			return false;

		return true;
	}

	xcb_connection_t* g_conn = nullptr;
	xcb_intern_atom_reply_t* g_delete_window_atom;

	xcb_connection_t* xcb_init()
	{
		const auto result(xcb_connect(nullptr, nullptr));

		if (xcb_connection_has_error(result) > 0)
		{
			cerr << "Cannot open display" << endl;
			return nullptr;
		}

		return result;
	}

	xcb_intern_atom_reply_t* xcb_init_atom(xcb_connection_t* conn, xcb_window_t window)
	{
		xcb_intern_atom_cookie_t protocolCookie = xcb_intern_atom(conn, 1, 12, "WM_PROTOCOLS");
		xcb_intern_atom_reply_t* protocolReply = xcb_intern_atom_reply(conn, protocolCookie, 0);
		xcb_intern_atom_cookie_t closeCookie = xcb_intern_atom(conn, 0, 16, "WM_DELETE_WINDOW");
		xcb_intern_atom_reply_t* closeReply = xcb_intern_atom_reply(conn, closeCookie, 0);

		xcb_change_property(conn, XCB_PROP_MODE_REPLACE, window, protocolReply->atom, 4, 32, 1, reinterpret_cast<void*>(&(closeReply->atom)));

		free(protocolReply);

		return closeReply;
	}

	xcb_window_t create_window(xcb_connection_t* conn, const ERect& rect)
	{
		/* Get the first screen */
		const xcb_setup_t* const setup(xcb_get_setup(conn));
		const xcb_screen_iterator_t iter(xcb_setup_roots_iterator (setup));
		xcb_screen_t* const screen(iter.data);

		const auto x(rect.left), y(rect.top);
		const auto width(rect.right - rect.left);
		const auto height(rect.bottom - rect.top);

		const uint32_t mask(XCB_CW_EVENT_MASK);
		const uint32_t values[]
		{
			XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_POINTER_MOTION |
			XCB_EVENT_MASK_STRUCTURE_NOTIFY
		};

		/* Create the window */
		xcb_window_t window = xcb_generate_id(conn);
		xcb_create_window(conn, /* Connection */
			XCB_COPY_FROM_PARENT, /* depth (same as root) */
			window, /* window Id */
			screen->root, /* parent window */
			x, y, /* x, y */
			width, height, /* width, height */
			1, /* border_width */
			XCB_WINDOW_CLASS_INPUT_OUTPUT, /* class */
			screen->root_visual, /* visual */
			mask, values); /* masks */

		g_delete_window_atom = xcb_init_atom(conn, window);

		/* Map the window on the screen */
		xcb_map_window(conn, window);

		/* Make sure commands are sent before we pause so that the window gets shown */
		xcb_flush(conn);

		return window;
	}

	bool xcb_process_events(xcb_connection_t* conn)
	{
		const unique_ptr<xcb_generic_event_t> e(xcb_poll_for_event(conn));

		if (!e)
			return true;

		bool result(true);
		switch (e->response_type & ~0x80)
		{
		case XCB_EXPOSE:
			xcb_flush(conn);
			break;

		case XCB_CLIENT_MESSAGE:
			if (reinterpret_cast<xcb_client_message_event_t* const>(e.get())->data.data32[0] == g_delete_window_atom->atom)
			{
				cout << "window closed, going to exit…" << endl;
				result = false;
			}
			break;
		}

		return result;
	}

	void audio_thread(AEffect* const ef)
	{
		constexpr size_t len { 256 };

		static array<float, len> left_input_buffer, right_input_buffer;
		static array<float, len> left_output_buffer, right_output_buffer;
		float* channels_in[] = { left_input_buffer.data(), right_input_buffer.data() };
		float *channels_out[] = { left_output_buffer.data(), right_output_buffer.data() };

		ef->processReplacing(ef, channels_in, channels_out, len);
	}

	bool quit { };
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(&audio_master_callback, string(CMAKE_BINARY_DIR) + "/src/host/host.exe.so"), &deleter);

	g_conn = xcb_init();

	auto* const ef(p->effect());

	ef->dispatcher(ef, effGetVstVersion, 0, 0, nullptr, 0.0f);
#ifdef USE_VST2SDK
	ef->dispatcher(ef, effGetPlugCategory, 0, 0, nullptr, 0.0f);
#endif
	ef->dispatcher(ef, effOpen, 0, 0, nullptr, 0.0f);
	ef->dispatcher(ef, effSetSampleRate, 0, 0, nullptr, 96000);
	ef->dispatcher(ef, effSetBlockSize, 0, 1024, nullptr, 0.0f);
	char can_do_1[] = "receiveVstMidiEvent";
	ef->dispatcher(ef, effCanDo, 0, 0, can_do_1, 0.0f);
	char can_do_2[] = "sendVstMidiEvent";
	ef->dispatcher(ef, effCanDo, 0, 0, can_do_2, 0.0f);
	char can_do_3[] = "receiveVstSysexEvent";
	ef->dispatcher(ef, effCanDo, 0, 0, can_do_3, 0.0f);
#ifdef USE_VST2SDK
	ef->dispatcher(ef, effGetPlugCategory, 0, 0, nullptr, 0.0f);
#endif
	ef->dispatcher(ef, effMainsChanged, 0, 1, nullptr, 0.0f);
	ef->dispatcher(ef, effMainsChanged, 0, 1, nullptr, 0.0f);

	thread th([ef]()
			  {
				  while (!quit)
					  audio_thread(ef);
			  });

#ifdef USE_VST2SDK
	VstPinProperties pin_properties;
	ef->dispatcher(ef, effGetInputProperties, 0, 0, &pin_properties, 0.0f);
	ef->dispatcher(ef, effGetOutputProperties, 0, 0, &pin_properties, 0.0f);

	for (size_t i = 0; i < ef->numParams; ++i)
	{
		VstParameterProperties props;
		ef->dispatcher(ef, effGetParameterProperties, i, 0, &props, 0.0f);
		char name[violin::kVstExtMaxParamStrLen];
		ef->dispatcher(ef, effGetParamName, i, 0, name, 0.0f);
		char label[violin::kVstExtMaxParamStrLen];
		ef->dispatcher(ef, effGetParamLabel, i, 0, label, 0.0f);
		const float value = ef->getParameter(ef, i);
		char display[violin::kVstExtMaxParamStrLen];
		ef->dispatcher(ef, effGetParamDisplay, i, 0, display, 0.0f);
		const bool can_be_automated = ef->dispatcher(ef, effCanBeAutomated, i, 0, nullptr, 0.0f);
		ef->dispatcher(ef, effGetParameterProperties, i, 0, &props, 0.0f);
	}

	for (size_t i = 0; i < ef->numPrograms; ++i)
	{
		char name[kVstMaxProgNameLen];
		ef->dispatcher(ef, effGetProgramNameIndexed, i, 0, name, 0.0f);
	}

	for (int32_t i = 0; i < 127; ++i)
	{
		MidiKeyName mkn { 0, i, "", 0 };
		ef->dispatcher(ef, effGetMidiKeyName, 0, 0, &mkn, 0.0f);
	}
#endif

	char can_do_4[] = "MPE";
	ef->dispatcher(ef, effCanDo, 0, 0, can_do_4, 0.0f);
#ifdef USE_VST2SDK
	ef->dispatcher(ef, effStartProcess, 0, 0, nullptr, 0.0f);
	ef->dispatcher(ef, effGetTailSize, 0, 0, nullptr, 0.0f);
#endif

	ERect* rect;

	ef->dispatcher(ef, effEditGetRect, 0, 0, &rect, 0.0f);

	const auto parent_window_handle(create_window(g_conn, *rect));
	ef->dispatcher(ef, effEditOpen, 0, 0, reinterpret_cast<void*>(parent_window_handle), 0.0f);
	ef->dispatcher(ef, effEditGetRect, 0, 0, &rect, 0.0f);

	const auto before(system_clock::now());
	system_clock::time_point last_idle_call_ts { };

	while (xcb_process_events(g_conn))
	{
		if (system_clock::now() - last_idle_call_ts > 100ms)
		{
			ef->dispatcher(ef, effEditIdle, 0, 0, nullptr, 0.0f);
			last_idle_call_ts = system_clock::now();
		}
		xcb_flush(g_conn);
	}

	cout << "after cycle" << endl;

	ef->dispatcher(ef, effEditClose, 0, 0, nullptr, 0.0f);

	quit = true;

	th.join();

	if (g_conn)
	{
		xcb_disconnect(g_conn);
		free(g_delete_window_atom);
	}

	cout << "after disconnect" << endl;

	return 0;
}
