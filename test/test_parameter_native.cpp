/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include "vst_header_chooser.h"

#include "plugin/plugin.h"
#include "plugin/native_starter.h"

using namespace std;
using namespace std::chrono;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<native_starter> plugin;

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	intptr_t audio_master_callback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
	{
		cout << "audio_master_callback(" << opcode << ", " << index << ", " << value << ", " << ptr << ", " << opt << ')' << endl;

		return true;
	}
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(&audio_master_callback, string(CMAKE_BINARY_DIR) + "/test/mock/host_native/mock_host_duplex_native"), &deleter);

	auto* const ef(p->effect());

	const float v1 = ef->getParameter(ef, 1);

	if (v1 != 7.5f)
		return 1;

	ef->setParameter(ef, 2, 3.25f);

	const float v2 = ef->getParameter(ef, 2);

	if (v2 != 3.25f)
		return 2;

	return 0;
}
