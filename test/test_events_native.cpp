/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include "plugin/plugin.h"
#include "plugin/native_starter.h"

using namespace std;
using namespace std::chrono;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<native_starter> plugin;

	bool master_received(false);

	std::aligned_storage_t<256, alignof(VstEvents)> storage;
	VstEvents* events = new (&storage) VstEvents;

	array<VstEvent, 10> event_array;

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	void initialize_events() noexcept
	{
		for (auto i = 0; i < event_array.size(); ++i)
		{
			auto& ev(event_array[i]);
#ifdef USE_VST2SDK
			ev.type = kVstMidiType;
			ev.byteSize = 2;
			ev.deltaFrames = i;
#endif
			events->events[i] = &ev;
		}
		events->numEvents = 10;
		events->reserved = 0;
	}

	intptr_t audio_master_callback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
	{
		return true;
	}
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(audio_master_callback, string(CMAKE_BINARY_DIR) + "/test/mock/host_native/mock_host_native"), &deleter);

	initialize_events();

	auto* const ef(p->effect());

	const auto result = ef->dispatcher(ef, effProcessEvents, 0, 0, events, 0.0f);
	cout << "result: " << result << endl;

	if (!result)
	{
		cerr << "test was failed" << endl;
		return 1;
	}

	return 0;
}
