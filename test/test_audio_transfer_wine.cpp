/*
   Violin
   Copyright (C) 2018  Alexey Chernov

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <iostream>

#include "plugin/plugin.h"
#include "plugin/wine_starter.h"

using namespace std;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<wine_starter> plugin;

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	constexpr const size_t buffer_size(4);

	float if0[] = {0, 1, 2, 4};
	float if1[] = {8, 10, 12, 16};
	float of0[] = {0, 0, 0, 0};
	float of1[] = {0, 0, 0, 0};

	float* finputs[] = {if0, if1};
	float* foutputs[] = {of0, of1};

	double id0[] = {0, -4, -8, -10};
	double id1[] = {-64, -56, -32, -24};
	double od0[] = {0, 0, 0, 0};
	double od1[] = {0, 0, 0, 0};

	double* dinputs[] = {id0, id1};
	double* doutputs[] = {od0, od1};
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(nullptr, string(CMAKE_BINARY_DIR) + "/test/mock/host_wine/mock_host_wine.exe.so"), &deleter);

	auto* const ef(p->effect());

	if (!((ef->flags & effFlagsCanReplacing)))
	{
		return 1;
	}

	ef->dispatcher(ef, effSetBlockSize, 0, buffer_size, nullptr, 0.0f);
	ef->dispatcher(ef, effOpen, 0, 0, nullptr, 0.0f);

	ef->processReplacing(ef, finputs, foutputs, buffer_size);

	if (!equal(if0, if0 + buffer_size, of0) || !equal(if1, if1 + buffer_size, of1))
	{
		cerr << "floating got: ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << of0[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << ", ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << of1[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << endl;

		cerr << "floating exp: ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << if0[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << ", ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << if1[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << endl;

		return 2;
	}

	ef->processDoubleReplacing(ef, dinputs, doutputs, buffer_size);

	if (!equal(id0, id0 + buffer_size, od0) || !equal(id1, id1 + buffer_size, od1))
	{
		cerr << "double got: ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << od0[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << ", ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << od1[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << endl;

		cerr << "double exp: ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << id0[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << ", ";
		for (size_t i = 0; i < buffer_size; ++i)
		{
			cerr << id1[i];
			if (i != buffer_size - 1)
				cerr << ' ';
		}
		cerr << endl;

		return 3;
	}

	return 0;
}
