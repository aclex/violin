/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "name.h"

using namespace std;

using namespace violin;

int main(int, char**)
{
	const string result(join(vector<string>{"foo", string(), "bar", string()}, ", "));
	const string expected("foo, bar");
	if (result != expected)
	{
		cout << "control: got: \"" << result << "\", expected: \"" << expected << '\"' << endl;
		return 1;
	}

	return 0;
}
