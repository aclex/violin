/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <array>

#include "log.h"

using namespace std;

using namespace violin::log;

int main(int, char**)
{
	initialize(0, nullptr); // wrapper case
	print("test");
	char* f { new char(4) };
	f[0] = 'f';
	f[1] = 'o';
	f[2] = 'o';
	f[3] = 0;
	char* b { new char(4) };
	b[0] = 'b';
	b[1] = 'a';
	b[2] = 'r';
	b[3] = 0;
	array<char*, 2> argv { f, b };
	initialize(2, argv.data());
	print("tost");

	delete[] f;
	delete[] b;

	return 0;
}
