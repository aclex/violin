/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TEST_MOCK_EDITOR_FORWARDING_H
#define VIOLIN_TEST_MOCK_EDITOR_FORWARDING_H

#include "threading_traits/std.h"

namespace violin
{
	namespace test
	{
		namespace mock
		{
			template<class Host> class editor_forwarding
			{
			public:
				typedef threading_traits::std threading_traits;

				editor_forwarding() noexcept :
					m_quitting(false)
				{

				}

				void initialize()
				{

				}

				void* open_editor_pseudo_parent_window()
				{
					return nullptr;
				}

				ERect* editor_window_rect() const noexcept
				{
					return nullptr;
				}

				void* x11_editor_window_handle() const noexcept
				{
					return nullptr;
				}

				void close_editor_window()
				{

				}

				void event_loop()
				{
					while (!m_quitting.load());
				}

				void quit()
				{
					m_quitting.store(true);
				}

				void save_last_rect(ERect* const)
				{

				}

				std::intptr_t dispatch_in_gui_thread(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					return accessor::direct_dispatch_to_plugin(this, opcode, index, value, ptr, opt);
				}

			private:
				class accessor : public Host
				{
				public:
					static std::intptr_t direct_dispatch_to_plugin(editor_forwarding* const h, std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
					{
						std::intptr_t (Host::* const fn)(std::int32_t, std::int32_t, std::intptr_t, void*, float)(&accessor::direct_dispatch_to_plugin);
						return (static_cast<Host*>(h)->*fn)(opcode, index, value, ptr, opt);
					}

				private:
					using Host::direct_dispatch_to_plugin;
				};

				std::atomic<bool> m_quitting;
			};
		}
	}
}

#endif // VIOLIN_TEST_MOCK_EDITOR_FORWARDING_H
