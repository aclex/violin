/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TEST_MOCK_PROCESSING_H
#define VIOLIN_TEST_MOCK_PROCESSING_H

#include <unordered_map>
#include <string>
#include <algorithm>

#include "vst_header_chooser.h"

#include "plugin/info.h"

namespace violin
{
	namespace test
	{
		namespace mock
		{
			template<class Host> class processing
			{
			public:
				explicit processing(const std::string&) :
					m_rect(std::make_unique<ERect>()),
					m_time_info(std::make_unique<VstTimeInfo>())
#ifdef USE_VST2SDK
					, m_output_properties(std::make_unique<VstPinProperties>())
#endif
				{
					m_rect->left = 1;
					m_rect->right = 2;
					m_rect->bottom = 3;
					m_rect->top = 4;

					m_time_info->samplePos = 1;
					m_time_info->sampleRate = 96000;
					m_time_info->nanoSeconds = 2;
					m_time_info->ppqPos = 3;
					m_time_info->tempo = 4;
					m_time_info->barStartPos = 5;
					m_time_info->cycleStartPos = 6;
					m_time_info->cycleEndPos = 7;
					m_time_info->timeSigNumerator = 8;
					m_time_info->timeSigDenominator = 12;
					m_time_info->smpteOffset = 9;
					m_time_info->smpteFrameRate = 10;
					m_time_info->samplesToNextClock = 11;
					m_time_info->flags = 0xff00;

#ifdef USE_VST2SDK
					std::strcpy(m_output_properties->label, "test");
					m_output_properties->flags = kVstPinIsActive | kVstPinIsStereo | kVstPinUseSpeaker;
					m_output_properties->arrangementType = kSpeakerArr30Cine;
					std::strcpy(m_output_properties->shortLabel, "Xab");
					std::memset(m_output_properties->future, 0, sizeof(m_output_properties->future));
#endif

					m_parameters[1] = 7.5f;
				}

				template<typename SampleType> void process(SampleType** inputs, SampleType** outputs, std::size_t size) noexcept
				{
					static constexpr SampleType k(0.25);

					const plugin::info& info(static_cast<Host*>(this)->get_info());
					const std::size_t channel_no(std::min(info.numInputs, info.numOutputs));

					for (std::size_t i = 0; i < channel_no; ++i)
					{
						for (std::size_t j = 0; j < size; ++j)
						{
							outputs[i][j] = inputs[i][j];
						}
					}
				}

				plugin::info get_info() const noexcept
				{
					return violin::plugin::info
					{
						0,
						0,
						2,
						2,
						effFlagsCanReplacing,
						0,
						4,
						1000
						};
				}

				bool has_editor() const noexcept
				{
					return static_cast<const Host*>(this)->get_info().flags & effFlagsHasEditor;
				}

				std::size_t input_count() const noexcept
				{
					return get_info().numInputs;
				}

				std::size_t output_count() const noexcept
				{
					return get_info().numOutputs;
				}

				std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					switch (opcode)
					{
					case effCanDo:
					{
						const std::string can_do_string(static_cast<const char*>(ptr));

						if (can_do_string == "coffee")
							return 0;

						if (can_do_string == "tea")
							return 1;

						if (can_do_string == "juice")
							return -1;

						break;
					}

					case effEditGetRect:
						*reinterpret_cast<ERect**>(ptr) = m_rect.get();
						break;

					case effGetParamDisplay:
					{
						const auto time_info(accessor::dispatch_to_master(this, audioMasterGetTime, 0, 0xfe00, nullptr, 0.0f));
						return check_time_info(reinterpret_cast<VstTimeInfo*>(time_info));
					}

					case effProcessEvents:
						return check_events(reinterpret_cast<VstEvents*>(ptr));

#ifdef USE_VST2SDK
					case effSetSpeakerArrangement:
						return check_speaker_arrangement(reinterpret_cast<VstSpeakerArrangement*>(value), static_cast<VstSpeakerArrangement*>(ptr));

					case effGetOutputProperties:
						return send_output_properties(reinterpret_cast<VstPinProperties*>(ptr));
#endif

					case effGetChunk:
						return send_chunk(reinterpret_cast<unsigned char const **>(ptr));

					case effSetChunk:
						return check_chunk(reinterpret_cast<unsigned char*>(ptr), static_cast<std::size_t>(value));
					}

					return true;
				}

				float get_parameter(std::int32_t index)
				{
					const auto& it { m_parameters.find(index) };
					return it != std::cend(m_parameters) ? it->second : 0.0f;
				}

				void set_parameter(std::int32_t index, float value)
				{
					m_parameters[index] = value;
				}

			protected:
				class accessor : public Host
				{
				public:
					static std::intptr_t dispatch_to_master(processing* const h, std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
					{
						std::intptr_t (Host::* const fn)(std::int32_t, std::int32_t, std::intptr_t, void*, float)(&accessor::dispatch_to_master);
						return (static_cast<Host*>(h)->*fn)(opcode, index, value, ptr, opt);
					}

				private:
					using Host::dispatch_to_master;
				};

			private:
				bool check_time_info(VstTimeInfo* info)
				{
					if (m_time_info->samplePos == info->samplePos &&
						m_time_info->sampleRate == info->sampleRate &&
						m_time_info->nanoSeconds == info->nanoSeconds &&
						m_time_info->ppqPos == info->ppqPos &&
						m_time_info->tempo == info->tempo &&
						m_time_info->barStartPos == info->barStartPos &&
						m_time_info->cycleStartPos == info->cycleStartPos &&
						m_time_info->cycleEndPos == info->cycleEndPos &&
						m_time_info->timeSigNumerator == info->timeSigNumerator &&
						m_time_info->timeSigDenominator == info->timeSigDenominator &&
						m_time_info->smpteOffset == info->smpteOffset &&
						m_time_info->smpteFrameRate == info->smpteFrameRate &&
						m_time_info->samplesToNextClock == info->samplesToNextClock &&
						m_time_info->flags == info->flags)
					{
						return true;
					}
					else
					{
						return false;
					}
				}

				bool check_events(VstEvents* events)
				{
					if (events->numEvents == 10 && events->reserved == 0)
					{
						for (auto i = 0; i < events->numEvents; ++i)
						{
							auto& ev(*events->events[i]);

#ifdef USE_VST2SDK
							if (ev.byteSize != 2 || ev.deltaFrames != i)
							{
								return false;
							}
#endif
						}

						return true;
					}
					else
					{
						return false;
					}
				}

#ifdef USE_VST2SDK
				bool check_speaker_arrangement(VstSpeakerArrangement* input, VstSpeakerArrangement* output)
				{
					if (input->type == kSpeakerArr51 && input->numChannels == 2 &&
						output->type == kSpeakerArrStereoCLfe && output->numChannels == 7)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
#endif

				std::size_t send_chunk(unsigned char const ** const data_ptr)
				{
					*data_ptr = s_test_chunk.data();
					return s_test_chunk.size();
				}

				bool check_chunk(unsigned char* const data, const std::size_t size)
				{
					if (size != s_test_chunk.size())
					{
						return false;
					}

					return std::equal(s_test_chunk.cbegin(), s_test_chunk.cend(), data);
				}

#ifdef USE_VST2SDK
				bool send_output_properties(VstPinProperties* const properties)
				{
					*properties = *m_output_properties;
					return true;
				}
#endif

				const std::unique_ptr<ERect> m_rect;
				const std::unique_ptr<VstTimeInfo> m_time_info;
				static constexpr const std::array<unsigned char, 24> s_test_chunk =
				{{
					0x00, 0x17, 0x01, 0x16,
					0x02, 0x15, 0x03, 0x14,
					0x04, 0x13, 0x05, 0x12,
					0x06, 0x11, 0x07, 0x10,
					0x08, 0x0f, 0x09, 0x0e,
					0x0a, 0x0d, 0x0b, 0x0c
				}};
#ifdef USE_VST2SDK
				const std::unique_ptr<VstPinProperties> m_output_properties;
#endif

				std::unordered_map<int, float> m_parameters;
			};

			template<class Host> class gui_processing : public processing<Host>
			{
			public:
				using processing<Host>::processing;

				plugin::info get_info() const noexcept
				{
					auto info(processing<Host>::get_info());
					info.flags |= effFlagsHasEditor;

					return info;
				}
			};

			template<class Host> class processing_duplex : public processing<Host>
			{
			public:
				using violin::test::mock::processing<Host>::processing;

				std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					if (opcode == effGetProgramName)
					{
						return processing<Host>::accessor::dispatch_to_master(this, audioMasterVersion, 0, 0, nullptr, 0.0f);
					}
					else if (opcode == effMainsChanged)
					{
						return processing<Host>::accessor::dispatch_to_master(this, audioMasterGetCurrentProcessLevel, 0, value, nullptr, 0.0f);
					}

					return true;
				}
			};
		}
	}
}

#endif // VIOLIN_TEST_MOCK_PROCESSING_H
