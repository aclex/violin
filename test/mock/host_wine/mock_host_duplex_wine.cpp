/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "host/host.h"

#include "mock/processing.h"
#include "mock/editor_forwarding.h"

using namespace std;

using namespace violin::host;
using namespace violin::test;

namespace
{
	typedef basic_host<mock::processing_duplex, mock::editor_forwarding> mock_host;
}

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "Usage: " << argv[0] << " PLUGIN_PATH SHM_NAME_SUFFIX" << endl;
		return -1;
	}

	violin::log::initialize(argc, argv);

	cout << "name suffix passed: " << argv[2] << endl;

	mock_host h(argv[1], "test", argv[2]);

	h.run();

	cout << "exit application successfully" << endl;

	return 0;
}
