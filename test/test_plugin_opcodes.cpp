/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#define VST_FORCE_DEPRECATED 0

#include "vst_header_chooser.h"

#include "opcode.h"

using namespace std;

using namespace violin::vst2;

int main(int, char**)
{
	if (plugin_opcodes.at(effOpen) != "effOpen")
	{
		cout << "expected: \"effOpen\", found: \"" << plugin_opcodes.at(effOpen) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effClose) != "effClose")
	{
		cout << "expected: \"effClose\", found: \"" << plugin_opcodes.at(effClose) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetProgram) != "effSetProgram")
	{
		cout << "expected: \"effSetProgram\", found: \"" << plugin_opcodes.at(effSetProgram) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetProgram) != "effGetProgram")
	{
		cout << "expected: \"effGetProgram\", found: \"" << plugin_opcodes.at(effGetProgram) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetProgramName) != "effSetProgramName")
	{
		cout << "expected: \"effSetProgramName\", found: \"" << plugin_opcodes.at(effSetProgramName) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetProgramName) != "effGetProgramName")
	{
		cout << "expected: \"effGetProgramName\", found: \"" << plugin_opcodes.at(effGetProgramName) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetParamLabel) != "effGetParamLabel")
	{
		cout << "expected: \"effGetParamLabel\", found: \"" << plugin_opcodes.at(effGetParamLabel) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetParamDisplay) != "effGetParamDisplay")
	{
		cout << "expected: \"effGetParamDisplay\", found: \"" << plugin_opcodes.at(effGetParamDisplay) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetParamName) != "effGetParamName")
	{
		cout << "expected: \"effGetParamName\", found: \"" << plugin_opcodes.at(effGetParamName) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetSampleRate) != "effSetSampleRate")
	{
		cout << "expected: \"effSetSampleRate\", found: \"" << plugin_opcodes.at(effSetSampleRate) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetBlockSize) != "effSetBlockSize")
	{
		cout << "expected: \"effSetBlockSize\", found: \"" << plugin_opcodes.at(effSetBlockSize) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effMainsChanged) != "effMainsChanged")
	{
		cout << "expected: \"effMainsChanged\", found: \"" << plugin_opcodes.at(effMainsChanged) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditGetRect) != "effEditGetRect")
	{
		cout << "expected: \"effEditGetRect\", found: \"" << plugin_opcodes.at(effEditGetRect) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditOpen) != "effEditOpen")
	{
		cout << "expected: \"effEditOpen\", found: \"" << plugin_opcodes.at(effEditOpen) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditClose) != "effEditClose")
	{
		cout << "expected: \"effEditClose\", found: \"" << plugin_opcodes.at(effEditClose) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditIdle) != "effEditIdle")
	{
		cout << "expected: \"effEditIdle\", found: \"" << plugin_opcodes.at(effEditIdle) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditTop) != "effEditTop")
	{
		cout << "expected: \"effEditTop\", found: \"" << plugin_opcodes.at(effEditTop) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetChunk) != "effGetChunk")
	{
		cout << "expected: \"effGetChunk\", found: \"" << plugin_opcodes.at(effGetChunk) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetChunk) != "effSetChunk")
	{
		cout << "expected: \"effSetChunk\", found: \"" << plugin_opcodes.at(effSetChunk) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effProcessEvents) != "effProcessEvents")
	{
		cout << "expected: \"effProcessEvents\", found: \"" << plugin_opcodes.at(effProcessEvents) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetEffectName) != "effGetEffectName")
	{
		cout << "expected: \"effGetEffectName\", found: \"" << plugin_opcodes.at(effGetEffectName) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetVendorString) != "effGetVendorString")
	{
		cout << "expected: \"effGetVendorString\", found: \"" << plugin_opcodes.at(effGetVendorString) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetProductString) != "effGetProductString")
	{
		cout << "expected: \"effGetProductString\", found: \"" << plugin_opcodes.at(effGetProductString) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetVendorVersion) != "effGetVendorVersion")
	{
		cout << "expected: \"effGetVendorVersion\", found: \"" << plugin_opcodes.at(effGetVendorVersion) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effCanDo) != "effCanDo")
	{
		cout << "expected: \"effCanDo\", found: \"" << plugin_opcodes.at(effCanDo) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetVstVersion) != "effGetVstVersion")
	{
		cout << "expected: \"effGetVstVersion\", found: \"" << plugin_opcodes.at(effGetVstVersion) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effBeginLoadBank) != "effBeginLoadBank")
	{
		cout << "expected: \"effBeginLoadBank\", found: \"" << plugin_opcodes.at(effBeginLoadBank) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effBeginLoadProgram) != "effBeginLoadProgram")
	{
		cout << "expected: \"effBeginLoadProgram\", found: \"" << plugin_opcodes.at(effBeginLoadProgram) << "\"" << endl;
		return 1;
	}

#ifdef USE_VST2SDK
	if (plugin_opcodes.at(effEditDraw) != "effEditDraw")
	{
		cout << "expected: \"effEditDraw\", found: \"" << plugin_opcodes.at(effEditDraw) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditMouse) != "effEditMouse")
	{
		cout << "expected: \"effEditMouse\", found: \"" << plugin_opcodes.at(effEditMouse) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditKey) != "effEditKey")
	{
		cout << "expected: \"effEditKey\", found: \"" << plugin_opcodes.at(effEditKey) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditSleep) != "effEditSleep")
	{
		cout << "expected: \"effEditSleep\", found: \"" << plugin_opcodes.at(effEditSleep) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effIdentify) != "effIdentify")
	{
		cout << "expected: \"effIdentify\", found: \"" << plugin_opcodes.at(effIdentify) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effCanBeAutomated) != "effCanBeAutomated")
	{
		cout << "expected: \"effCanBeAutomated\", found: \"" << plugin_opcodes.at(effCanBeAutomated) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effString2Parameter) != "effString2Parameter")
	{
		cout << "expected: \"effString2Parameter\", found: \"" << plugin_opcodes.at(effString2Parameter) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetNumProgramCategories) != "effGetNumProgramCategories")
	{
		cout << "expected: \"effGetNumProgramCategories\", found: \"" << plugin_opcodes.at(effGetNumProgramCategories) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetProgramNameIndexed) != "effGetProgramNameIndexed")
	{
		cout << "expected: \"effGetProgramNameIndexed\", found: \"" << plugin_opcodes.at(effGetProgramNameIndexed) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effCopyProgram) != "effCopyProgram")
	{
		cout << "expected: \"effCopyProgram\", found: \"" << plugin_opcodes.at(effCopyProgram) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effConnectInput) != "effConnectInput")
	{
		cout << "expected: \"effConnectInput\", found: \"" << plugin_opcodes.at(effConnectInput) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effConnectOutput) != "effConnectOutput")
	{
		cout << "expected: \"effConnectOutput\", found: \"" << plugin_opcodes.at(effConnectOutput) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetInputProperties) != "effGetInputProperties")
	{
		cout << "expected: \"effGetInputProperties\", found: \"" << plugin_opcodes.at(effGetInputProperties) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetOutputProperties) != "effGetOutputProperties")
	{
		cout << "expected: \"effGetOutputProperties\", found: \"" << plugin_opcodes.at(effGetOutputProperties) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetPlugCategory) != "effGetPlugCategory")
	{
		cout << "expected: \"effGetPlugCategory\", found: \"" << plugin_opcodes.at(effGetPlugCategory) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetCurrentPosition) != "effGetCurrentPosition")
	{
		cout << "expected: \"effGetCurrentPosition\", found: \"" << plugin_opcodes.at(effGetCurrentPosition) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetDestinationBuffer) != "effGetDestinationBuffer")
	{
		cout << "expected: \"effGetDestinationBuffer\", found: \"" << plugin_opcodes.at(effGetDestinationBuffer) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effOfflineNotify) != "effOfflineNotify")
	{
		cout << "expected: \"effOfflineNotify\", found: \"" << plugin_opcodes.at(effOfflineNotify) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effOfflinePrepare) != "effOfflinePrepare")
	{
		cout << "expected: \"effOfflinePrepare\", found: \"" << plugin_opcodes.at(effOfflinePrepare) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effOfflineRun) != "effOfflineRun")
	{
		cout << "expected: \"effOfflineRun\", found: \"" << plugin_opcodes.at(effOfflineRun) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effProcessVarIo) != "effProcessVarIo")
	{
		cout << "expected: \"effProcessVarIo\", found: \"" << plugin_opcodes.at(effProcessVarIo) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetSpeakerArrangement) != "effSetSpeakerArrangement")
	{
		cout << "expected: \"effSetSpeakerArrangement\", found: \"" << plugin_opcodes.at(effSetSpeakerArrangement) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetBlockSizeAndSampleRate) != "effSetBlockSizeAndSampleRate")
	{
		cout << "expected: \"effSetBlockSizeAndSampleRate\", found: \"" << plugin_opcodes.at(effSetBlockSizeAndSampleRate) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetBypass) != "effSetBypass")
	{
		cout << "expected: \"effSetBypass\", found: \"" << plugin_opcodes.at(effSetBypass) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetErrorText) != "effGetErrorText")
	{
		cout << "expected: \"effGetErrorText\", found: \"" << plugin_opcodes.at(effGetErrorText) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effVendorSpecific) != "effVendorSpecific")
	{
		cout << "expected: \"effVendorSpecific\", found: \"" << plugin_opcodes.at(effVendorSpecific) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetTailSize) != "effGetTailSize")
	{
		cout << "expected: \"effGetTailSize\", found: \"" << plugin_opcodes.at(effGetTailSize) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effIdle) != "effIdle")
	{
		cout << "expected: \"effIdle\", found: \"" << plugin_opcodes.at(effIdle) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetIcon) != "effGetIcon")
	{
		cout << "expected: \"effGetIcon\", found: \"" << plugin_opcodes.at(effGetIcon) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetViewPosition) != "effSetViewPosition")
	{
		cout << "expected: \"effSetViewPosition\", found: \"" << plugin_opcodes.at(effSetViewPosition) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetParameterProperties) != "effGetParameterProperties")
	{
		cout << "expected: \"effGetParameterProperties\", found: \"" << plugin_opcodes.at(effGetParameterProperties) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effKeysRequired) != "effKeysRequired")
	{
		cout << "expected: \"effKeysRequired\", found: \"" << plugin_opcodes.at(effKeysRequired) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditKeyDown) != "effEditKeyDown")
	{
		cout << "expected: \"effEditKeyDown\", found: \"" << plugin_opcodes.at(effEditKeyDown) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEditKeyUp) != "effEditKeyUp")
	{
		cout << "expected: \"effEditKeyUp\", found: \"" << plugin_opcodes.at(effEditKeyUp) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetEditKnobMode) != "effSetEditKnobMode")
	{
		cout << "expected: \"effSetEditKnobMode\", found: \"" << plugin_opcodes.at(effSetEditKnobMode) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetMidiProgramName) != "effGetMidiProgramName")
	{
		cout << "expected: \"effGetMidiProgramName\", found: \"" << plugin_opcodes.at(effGetMidiProgramName) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetCurrentMidiProgram) != "effGetCurrentMidiProgram")
	{
		cout << "expected: \"effGetCurrentMidiProgram\", found: \"" << plugin_opcodes.at(effGetCurrentMidiProgram) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetMidiProgramCategory) != "effGetMidiProgramCategory")
	{
		cout << "expected: \"effGetMidiProgramCategory\", found: \"" << plugin_opcodes.at(effGetMidiProgramCategory) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effHasMidiProgramsChanged) != "effHasMidiProgramsChanged")
	{
		cout << "expected: \"effHasMidiProgramsChanged\", found: \"" << plugin_opcodes.at(effHasMidiProgramsChanged) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetMidiKeyName) != "effGetMidiKeyName")
	{
		cout << "expected: \"effGetMidiKeyName\", found: \"" << plugin_opcodes.at(effGetMidiKeyName) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effBeginSetProgram) != "effBeginSetProgram")
	{
		cout << "expected: \"effBeginSetProgram\", found: \"" << plugin_opcodes.at(effBeginSetProgram) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effEndSetProgram) != "effEndSetProgram")
	{
		cout << "expected: \"effEndSetProgram\", found: \"" << plugin_opcodes.at(effEndSetProgram) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetSpeakerArrangement) != "effGetSpeakerArrangement")
	{
		cout << "expected: \"effGetSpeakerArrangement\", found: \"" << plugin_opcodes.at(effGetSpeakerArrangement) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effShellGetNextPlugin) != "effShellGetNextPlugin")
	{
		cout << "expected: \"effShellGetNextPlugin\", found: \"" << plugin_opcodes.at(effShellGetNextPlugin) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effStartProcess) != "effStartProcess")
	{
		cout << "expected: \"effStartProcess\", found: \"" << plugin_opcodes.at(effStartProcess) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effStopProcess) != "effStopProcess")
	{
		cout << "expected: \"effStopProcess\", found: \"" << plugin_opcodes.at(effStopProcess) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetTotalSampleToProcess) != "effSetTotalSampleToProcess")
	{
		cout << "expected: \"effSetTotalSampleToProcess\", found: \"" << plugin_opcodes.at(effSetTotalSampleToProcess) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetPanLaw) != "effSetPanLaw")
	{
		cout << "expected: \"effSetPanLaw\", found: \"" << plugin_opcodes.at(effSetPanLaw) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effSetProcessPrecision) != "effSetProcessPrecision")
	{
		cout << "expected: \"effSetProcessPrecision\", found: \"" << plugin_opcodes.at(effSetProcessPrecision) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetNumMidiInputChannels) != "effGetNumMidiInputChannels")
	{
		cout << "expected: \"effGetNumMidiInputChannels\", found: \"" << plugin_opcodes.at(effGetNumMidiInputChannels) << "\"" << endl;
		return 1;
	}

	if (plugin_opcodes.at(effGetNumMidiOutputChannels) != "effGetNumMidiOutputChannels")
	{
		cout << "expected: \"effGetNumMidiOutputChannels\", found: \"" << plugin_opcodes.at(effGetNumMidiOutputChannels) << "\"" << endl;
		return 1;
	}
#endif

	return 0;
}
