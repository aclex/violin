/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "name.h"

using namespace std;

using namespace violin;

int main(int, char**)
{
	const string plugin_name("MyLovelyPlugin");
	const string control_name(create_shm_name(control_shm_name_prefix(plugin_name), string(), "abd456ff"));
	const string control_expected("/violin_MyLovelyPlugin_control_abd456ff");
	if (control_name != control_expected)
	{
		cout << "control: got: \"" << control_name << "\", expected: \"" << control_expected << '\"' << endl;
		return 1;
	}

	const string audio_name(create_shm_name(audio_shm_name_prefix(plugin_name), string(), "abd456ff"));
	const string audio_expected("/violin_MyLovelyPlugin_audio_abd456ff");
	if (audio_name != audio_expected)
	{
		cout << "audio: got: \"" << audio_name << "\", expected: \"" << audio_expected << '\"' << endl;
		return 2;
	}

	return 0;
}
