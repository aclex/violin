/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "name.h"

using namespace std;

using namespace violin;

namespace
{
	void test_get()
	{
		const string result(chunk_shm_name_prefix<transfer::chunk::request_type::get>("foo"));
		const string expected("/violin_foo_get_chunk");
		if (result != expected)
		{
			const string msg { "get: got: \""s + result + "\", expected: \""s + expected + '\"' };
			throw runtime_error(msg);
		}
	}

	void test_set()
	{
		const string result(chunk_shm_name_prefix<transfer::chunk::request_type::set>("foo"));
		const string expected("/violin_foo_set_chunk");
		if (result != expected)
		{
			const string msg { "set: got: \""s + result + "\", expected: \""s + expected + '\"' };
			throw runtime_error(msg);
		}
	}
}

int main(int, char**)
{
	test_get();
	test_set();

	return 0;
}
