/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include "plugin/plugin.h"
#include "plugin/native_starter.h"

using namespace std;
using namespace std::chrono;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<native_starter> plugin;

	bool master_received(false);

	const unique_ptr<VstTimeInfo> time_info(make_unique<VstTimeInfo>());

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	void initialize_time_info() noexcept
	{
		time_info->samplePos = 1;
		time_info->sampleRate = 96000;
		time_info->nanoSeconds = 2;
		time_info->ppqPos = 3;
		time_info->tempo = 4;
		time_info->barStartPos = 5;
		time_info->cycleStartPos = 6;
		time_info->cycleEndPos = 7;
		time_info->timeSigNumerator = 8;
		time_info->timeSigDenominator = 12;
		time_info->smpteOffset = 9;
		time_info->smpteFrameRate = 10;
		time_info->samplesToNextClock = 11;
		time_info->flags = 0xff00;
	}

	intptr_t audio_master_callback(AEffect* effect, int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
	{
		if (opcode ==audioMasterGetTime)
		{
			cout << "audioMasterGetTime" << endl;
			return reinterpret_cast<intptr_t>(time_info.get());
		}

		return true;
	}
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(audio_master_callback, string(CMAKE_BINARY_DIR) + "/test/mock/host_native/mock_host_native"), &deleter);

	initialize_time_info();

	auto* const ef(p->effect());

	const auto result = ef->dispatcher(ef, effGetParamDisplay, 0, 0, nullptr, 0.0f);
	cout << "result: " << result << endl;

	if (!result)
	{
		cerr << "test was failed" << endl;
		return 1;
	}

	return 0;
}
