/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "name.h"

using namespace std;

using namespace violin;

namespace
{
	void test_normal_path()
	{
		const string result(basename("foo/bar.txt"));
		const string expected("bar.txt");
		if (result != expected)
		{
			const string msg { "normal_path: got: \""s + result + "\", expected: \""s + expected + '\"' };
			throw runtime_error(msg);
		}
	}

	void test_local_path()
	{
		const string result(basename("bar.txt"));
		const string expected("bar.txt");
		if (result != expected)
		{
			const string msg { "local_path: got: \""s + result + "\", expected: \""s + expected + '\"' };
			throw runtime_error(msg);
		}
	}

	void test_dir_path()
	{
		const string result(basename("foo/bar.txt/"));
		const string expected;
		if (result != expected)
		{
			const string msg { "dir_path: got: \""s + result + "\", expected: \""s + expected + '\"' };
			throw runtime_error(msg);
		}
	}
}

int main(int, char**)
{
	test_normal_path();
	test_local_path();
	test_dir_path();

	return 0;
}
