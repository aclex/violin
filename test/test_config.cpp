/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <typeinfo>

#include "config.h"

using namespace std;

using namespace violin;

namespace
{
	constexpr const char wrapper_path[] { "/tmp/test.so" };
	constexpr const char plugin_path[] { "/tmp/test.dll" };
	constexpr const char new_wrapper_path[] { "foo" };
	constexpr const char new_plugin_path[] { "bar" };
}

int main(int, char**)
{
	try
	{
		config cfg_ne("nonexistent.json");

		if (!cfg_ne.empty())
		{
			cerr << "Expected empty configuration after opening non-existent file." << endl;
			return 1;
		}

		config cfg_empty("empty.json");

		if (!cfg_empty.empty())
		{
			cerr << "Expected empty configuration after opening empty file." << endl;
			return 2;
		}

		config cfg_non_json("test_config.cpp");

		if (!cfg_non_json.empty())
		{
			cerr << "Expected empty configuration after opening non-JSON file." << endl;
			return 3;
		}

		config cfg_misfmt("misformatted.json");

		if (cfg_misfmt.empty())
		{
			cerr << "Expected non-empty configuration after opening JSON file in non-expected format." << endl;
			return 4;
		}

		{
			config cfg_correct_json("correct_config.json");

			if (cfg_correct_json.empty())
			{
				cerr << "Expected non-empty configuration after opening JSON file with expected configuration format." << endl;
				return 5;
			}

			const auto& record(cfg_correct_json.plugin_path(wrapper_path));

			if (!record || record.value() != plugin_path)
			{
				if (!record)
				{
					cerr << "Expected record " << wrapper_path << " : " << plugin_path << " in configuration, but found nothing." << endl;

					return 6;
				}
				else
				{
					cerr << "Expected record " << wrapper_path << " : " << plugin_path << " in configuration, but found " << wrapper_path << " : " << record.value() << '.' << endl;

					return 7;
				}
			}

			cfg_correct_json.add_mapping(new_wrapper_path, new_plugin_path);
		}

		{
			config cfg_correct_json("correct_config.json");

			const auto& record(cfg_correct_json.plugin_path(new_wrapper_path));

			if (!record || record.value() != new_plugin_path)
			{
				if (!record)
				{
					cerr << "Expected record " << new_wrapper_path << " : " << new_plugin_path << " in configuration, but found nothing." << endl;

					return 8;
				}
				else
				{
					cerr << "Expected record " << new_wrapper_path << " : " << new_plugin_path << " in configuration, but found " << new_wrapper_path << " : " << record.value() << '.' << endl;

					return 9;
				}
			}

			cfg_correct_json.remove_mapping(new_wrapper_path);
		}

		{
			config cfg_correct_json("correct_config.json");

			const auto& record(cfg_correct_json.plugin_path(new_wrapper_path));

			if (record)
			{
				cerr << "Not expected removed record with key " << new_wrapper_path << " in configuration, but found " << new_wrapper_path << " : " << record.value() << '.' << endl;

				return 10;
			}
		}

		{
			config cfg_correct_json("correct_config.json");

			cfg_correct_json.add_mapping(new_wrapper_path, new_plugin_path);
			cfg_correct_json.add_mapping(new_wrapper_path, plugin_path);


			const auto& record(cfg_correct_json.plugin_path(new_wrapper_path));

			if (!record)
			{
				cerr << "Expected added record " << new_wrapper_path << " : " << plugin_path << " in configuration, but found nothing." << endl;

				return 11;
			}
			else if (record.value() != plugin_path)
			{
				cerr << "Expected added record " << new_wrapper_path << " : " << plugin_path << " in configuration, but found " << new_wrapper_path << " : " << record.value() << '.' << endl;

				return 12;
			}
		}

		{
			config cfg_correct_json("correct_config.json");

			cfg_correct_json.add_mapping(wrapper_path, new_plugin_path);

			const auto& record(cfg_correct_json.wrapper_path(new_plugin_path));

			if (!record)
			{
				cerr << "Expected added record " << wrapper_path << " : " << new_plugin_path << " in configuration, but found nothing." << endl;

				return 13;
			}
		}

		{
			config cfg_correct_json("correct_config.json");

			const auto& lst { cfg_correct_json.list_mappings() };

			const unordered_map<string, string> expected
			{
				{ "foo", "/tmp/test.dll" },
				{ "/tmp/test.so", "bar" }
			};

			if (lst != expected)
			{
				cerr << "Expected and actual list mismatch." << endl;
				cerr << "Expected:" << endl;

				for (const auto& p : expected)
					cerr << p.first << " : " << p.second << endl;

				cerr << "Received:" << endl;

				for (const auto& p : lst)
					cerr << p.first << " : " << p.second << endl;

				return 14;
			}
		}
	}
	catch (const exception& e)
	{
		cerr << "Config test error: exception caught: " << typeid(e).name() << ", cause: " << e.what() << endl;
		return 1;
	}

	return 0;
}
