/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <thread>
#include <chrono>
#include <iostream>

#include "plugin/plugin.h"
#include "plugin/native_starter.h"

using namespace std;
using namespace std::chrono;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<native_starter> plugin;

	bool master_received(false);

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(nullptr, string(CMAKE_BINARY_DIR) + "/test/mock/host_native/mock_host_duplex_native"), &deleter);

	auto* const ef(p->effect());

	MidiKeyName input
	{
		0,
		0,
		"",
		0,
		0
	};

	const auto result = ef->dispatcher(ef, effGetMidiKeyName, 0, 0, &input, 0.0f);
	cout << "result: " << result << endl;

	return 0;
}
