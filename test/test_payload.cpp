/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <iostream>

#include "message/payload/payload.h"

using namespace std;

using namespace violin::message;

namespace
{
	typedef payload<int> my_payload;

	void test_null_init()
	{
		my_payload m(0, 0, 0, nullptr, 0.0f);

		void* const check_ptr { nullptr };

		m.copy(check_ptr); // should lead to UB if initialized incorrectly

		if (*m.payload_ptr() != 0)
			throw runtime_error("Incorrect initialization result: expected 0 inside, found " + to_string(*m.payload_ptr()));
	}

	void test_normal_init()
	{
		int input { 7 };

		my_payload m(0, 0, 0, &input, 0.0f);

		int output { };

		m.copy(&output);

		if (*m.payload_ptr() != input)
			throw runtime_error("Incorrect initialization result: expected " + to_string(input) + " inside, found " + to_string(*m.payload_ptr()));
	}

	void test_copy()
	{
		int input { 7 };

		my_payload m(0, 0, 0, &input, 0.0f);

		int output { };

		m.copy(&output);

		if (output != input)
			throw runtime_error("Incorrect copying result 1: expected " + to_string(input) + " as output, found " + to_string(output));

		output = 8;

		const intptr_t check_ptr { reinterpret_cast<intptr_t>(&output) };

		m.copy(check_ptr);

		if (output != input)
			throw runtime_error("Incorrect copying result 2: expected " + to_string(input) + " as output, found " + to_string(output));

	}

	void test_set_null()
	{
		class my_derived_payload : public my_payload
		{
		public:
			using my_payload::my_payload;
			using my_payload::set_null;
		};

		int input { 7 };

		my_derived_payload m(0, 0, 0, &input, 0.0f);
		m.set_null();

		void* const check_ptr { nullptr };

		m.copy(check_ptr); // should lead to UB if `set_null()` doesn't work
	}
}

int main(int, char**)
{
	test_null_init();
	test_normal_init();
	test_copy();
	test_set_null();

	return 0;
}
