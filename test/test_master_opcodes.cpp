/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#define VST_FORCE_DEPRECATED 0

#include "vst_header_chooser.h"

#include "opcode.h"

using namespace std;

using namespace violin::vst2;

int main(int, char**)
{
	if (master_opcodes.at(audioMasterAutomate) != "audioMasterAutomate")
	{
		cout << "expected: \"audioMasterAutomate\", found: \"" << master_opcodes.at(audioMasterAutomate) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterVersion) != "audioMasterVersion")
	{
		cout << "expected: \"audioMasterVersion\", found: \"" << master_opcodes.at(audioMasterVersion) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterCurrentId) != "audioMasterCurrentId")
	{
		cout << "expected: \"audioMasterCurrentId\", found: \"" << master_opcodes.at(audioMasterCurrentId) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterIdle) != "audioMasterIdle")
	{
		cout << "expected: \"audioMasterIdle\", found: \"" << master_opcodes.at(audioMasterIdle) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterPinConnected) != "audioMasterPinConnected")
	{
		cout << "expected: \"audioMasterPinConnected\", found: \"" << master_opcodes.at(audioMasterPinConnected) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterWantMidi) != "audioMasterWantMidi")
	{
		cout << "expected: \"audioMasterWantMidi\", found: \"" << master_opcodes.at(audioMasterWantMidi) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetTime) != "audioMasterGetTime")
	{
		cout << "expected: \"audioMasterGetTime\", found: \"" << master_opcodes.at(audioMasterGetTime) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterProcessEvents) != "audioMasterProcessEvents")
	{
		cout << "expected: \"audioMasterProcessEvents\", found: \"" << master_opcodes.at(audioMasterProcessEvents) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterSetTime) != "audioMasterSetTime")
	{
		cout << "expected: \"audioMasterSetTime\", found: \"" << master_opcodes.at(audioMasterSetTime) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterTempoAt) != "audioMasterTempoAt")
	{
		cout << "expected: \"audioMasterTempoAt\", found: \"" << master_opcodes.at(audioMasterTempoAt) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetNumAutomatableParameters) != "audioMasterGetNumAutomatableParameters")
	{
		cout << "expected: \"audioMasterGetNumAutomatableParameters\", found: \"" << master_opcodes.at(audioMasterGetNumAutomatableParameters) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetParameterQuantization) != "audioMasterGetParameterQuantization")
	{
		cout << "expected: \"audioMasterGetParameterQuantization\", found: \"" << master_opcodes.at(audioMasterGetParameterQuantization) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterIOChanged) != "audioMasterIOChanged")
	{
		cout << "expected: \"audioMasterIOChanged\", found: \"" << master_opcodes.at(audioMasterIOChanged) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterNeedIdle) != "audioMasterNeedIdle")
	{
		cout << "expected: \"audioMasterNeedIdle\", found: \"" << master_opcodes.at(audioMasterNeedIdle) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterSizeWindow) != "audioMasterSizeWindow")
	{
		cout << "expected: \"audioMasterSizeWindow\", found: \"" << master_opcodes.at(audioMasterSizeWindow) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetSampleRate) != "audioMasterGetSampleRate")
	{
		cout << "expected: \"audioMasterGetSampleRate\", found: \"" << master_opcodes.at(audioMasterGetSampleRate) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetBlockSize) != "audioMasterGetBlockSize")
	{
		cout << "expected: \"audioMasterGetBlockSize\", found: \"" << master_opcodes.at(audioMasterGetBlockSize) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetInputLatency) != "audioMasterGetInputLatency")
	{
		cout << "expected: \"audioMasterGetInputLatency\", found: \"" << master_opcodes.at(audioMasterGetInputLatency) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetOutputLatency) != "audioMasterGetOutputLatency")
	{
		cout << "expected: \"audioMasterGetOutputLatency\", found: \"" << master_opcodes.at(audioMasterGetOutputLatency) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetPreviousPlug) != "audioMasterGetPreviousPlug")
	{
		cout << "expected: \"audioMasterGetPreviousPlug\", found: \"" << master_opcodes.at(audioMasterGetPreviousPlug) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetNextPlug) != "audioMasterGetNextPlug")
	{
		cout << "expected: \"audioMasterGetNextPlug\", found: \"" << master_opcodes.at(audioMasterGetNextPlug) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterWillReplaceOrAccumulate) != "audioMasterWillReplaceOrAccumulate")
	{
		cout << "expected: \"audioMasterWillReplaceOrAccumulate\", found: \"" << master_opcodes.at(audioMasterWillReplaceOrAccumulate) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetCurrentProcessLevel) != "audioMasterGetCurrentProcessLevel")
	{
		cout << "expected: \"audioMasterGetCurrentProcessLevel\", found: \"" << master_opcodes.at(audioMasterGetCurrentProcessLevel) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetAutomationState) != "audioMasterGetAutomationState")
	{
		cout << "expected: \"audioMasterGetAutomationState\", found: \"" << master_opcodes.at(audioMasterGetAutomationState) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOfflineStart) != "audioMasterOfflineStart")
	{
		cout << "expected: \"audioMasterOfflineStart\", found: \"" << master_opcodes.at(audioMasterOfflineStart) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOfflineRead) != "audioMasterOfflineRead")
	{
		cout << "expected: \"audioMasterOfflineRead\", found: \"" << master_opcodes.at(audioMasterOfflineRead) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOfflineWrite) != "audioMasterOfflineWrite")
	{
		cout << "expected: \"audioMasterOfflineWrite\", found: \"" << master_opcodes.at(audioMasterOfflineWrite) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOfflineGetCurrentPass) != "audioMasterOfflineGetCurrentPass")
	{
		cout << "expected: \"audioMasterOfflineGetCurrentPass\", found: \"" << master_opcodes.at(audioMasterOfflineGetCurrentPass) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOfflineGetCurrentMetaPass) != "audioMasterOfflineGetCurrentMetaPass")
	{
		cout << "expected: \"audioMasterOfflineGetCurrentMetaPass\", found: \"" << master_opcodes.at(audioMasterOfflineGetCurrentMetaPass) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterSetOutputSampleRate) != "audioMasterSetOutputSampleRate")
	{
		cout << "expected: \"audioMasterSetOutputSampleRate\", found: \"" << master_opcodes.at(audioMasterSetOutputSampleRate) << "\"" << endl;
		return 1;
	}

#ifdef USE_VST2SDK
	if (master_opcodes.at(audioMasterGetOutputSpeakerArrangement) != "audioMasterGetOutputSpeakerArrangement")
	{
		cout << "expected: \"audioMasterGetOutputSpeakerArrangement\", found: \"" << master_opcodes.at(audioMasterGetOutputSpeakerArrangement) << "\"" << endl;
		return 1;
	}
#endif

	if (master_opcodes.at(audioMasterGetVendorString) != "audioMasterGetVendorString")
	{
		cout << "expected: \"audioMasterGetVendorString\", found: \"" << master_opcodes.at(audioMasterGetVendorString) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetProductString) != "audioMasterGetProductString")
	{
		cout << "expected: \"audioMasterGetProductString\", found: \"" << master_opcodes.at(audioMasterGetProductString) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetVendorVersion) != "audioMasterGetVendorVersion")
	{
		cout << "expected: \"audioMasterGetVendorVersion\", found: \"" << master_opcodes.at(audioMasterGetVendorVersion) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterVendorSpecific) != "audioMasterVendorSpecific")
	{
		cout << "expected: \"audioMasterVendorSpecific\", found: \"" << master_opcodes.at(audioMasterVendorSpecific) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterSetIcon) != "audioMasterSetIcon")
	{
		cout << "expected: \"audioMasterSetIcon\", found: \"" << master_opcodes.at(audioMasterSetIcon) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterCanDo) != "audioMasterCanDo")
	{
		cout << "expected: \"audioMasterCanDo\", found: \"" << master_opcodes.at(audioMasterCanDo) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetLanguage) != "audioMasterGetLanguage")
	{
		cout << "expected: \"audioMasterGetLanguage\", found: \"" << master_opcodes.at(audioMasterGetLanguage) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOpenWindow) != "audioMasterOpenWindow")
	{
		cout << "expected: \"audioMasterOpenWindow\", found: \"" << master_opcodes.at(audioMasterOpenWindow) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterCloseWindow) != "audioMasterCloseWindow")
	{
		cout << "expected: \"audioMasterCloseWindow\", found: \"" << master_opcodes.at(audioMasterCloseWindow) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetDirectory) != "audioMasterGetDirectory")
	{
		cout << "expected: \"audioMasterGetDirectory\", found: \"" << master_opcodes.at(audioMasterGetDirectory) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterUpdateDisplay) != "audioMasterUpdateDisplay")
	{
		cout << "expected: \"audioMasterUpdateDisplay\", found: \"" << master_opcodes.at(audioMasterUpdateDisplay) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterBeginEdit) != "audioMasterBeginEdit")
	{
		cout << "expected: \"audioMasterBeginEdit\", found: \"" << master_opcodes.at(audioMasterBeginEdit) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterEndEdit) != "audioMasterEndEdit")
	{
		cout << "expected: \"audioMasterEndEdit\", found: \"" << master_opcodes.at(audioMasterEndEdit) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterOpenFileSelector) != "audioMasterOpenFileSelector")
	{
		cout << "expected: \"audioMasterOpenFileSelector\", found: \"" << master_opcodes.at(audioMasterOpenFileSelector) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterCloseFileSelector) != "audioMasterCloseFileSelector")
	{
		cout << "expected: \"audioMasterCloseFileSelector\", found: \"" << master_opcodes.at(audioMasterCloseFileSelector) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterEditFile) != "audioMasterEditFile")
	{
		cout << "expected: \"audioMasterEditFile\", found: \"" << master_opcodes.at(audioMasterEditFile) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetChunkFile) != "audioMasterGetChunkFile")
	{
		cout << "expected: \"audioMasterGetChunkFile\", found: \"" << master_opcodes.at(audioMasterGetChunkFile) << "\"" << endl;
		return 1;
	}

	if (master_opcodes.at(audioMasterGetInputSpeakerArrangement) != "audioMasterGetInputSpeakerArrangement")
	{
		cout << "expected: \"audioMasterGetInputSpeakerArrangement\", found: \"" << master_opcodes.at(audioMasterGetInputSpeakerArrangement) << "\"" << endl;
		return 1;
	}


	return 0;
}
