/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <iostream>

#include "message/payload/payload.h"
#include "message/payload/e_rect.h"

using namespace std;

using namespace violin::message;

namespace
{
	typedef payload<ERect> my_payload;

	void test_copy_null()
	{
		my_payload m(0, 0, 0, nullptr, 0.0f);

		ERect* output { nullptr };

		m.copy(&output);

		if (output)
			throw runtime_error("Incorrect ERect null copying result: expected nullptr output.");
	}

	void test_copy_to_null()
	{
		ERect rect { 0, 0, 0, 0 };

		my_payload m(0, 0, 0, &rect, 0.0f);

		m.copy(nullptr); // leads to UB, if copy is incorrect
	}

	void test_copy()
	{
		ERect rect { 0, 0, 0, 0 };

		my_payload m(0, 0, 0, &rect, 0.0f);

		ERect* output;

		m.copy(&output);

		if (output != m.payload_ptr())
			throw runtime_error("Incorrect ERect copying result: output and payload pointer mispatch.");
	}
}

int main(int, char**)
{
	test_copy();
	test_copy_null();
	test_copy_to_null();

	return 0;
}
