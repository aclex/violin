/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <iostream>

#include "plugin/plugin.h"
#include "plugin/native_starter.h"

using namespace std;

using namespace violin::plugin;

namespace
{
	typedef basic_plugin<native_starter> plugin;

	void deleter(plugin* p) noexcept
	{
		auto* const ef(p->effect());
		ef->dispatcher(ef, effClose, 0, 0, nullptr, 0);
	}

	constexpr std::array<unsigned char, 24> test_chunk
	{{
		0x00, 0x17, 0x01, 0x16,
		0x02, 0x15, 0x03, 0x14,
		0x04, 0x13, 0x05, 0x12,
		0x06, 0x11, 0x07, 0x10,
		0x08, 0x0f, 0x09, 0x0e,
		0x0a, 0x0d, 0x0b, 0x0c
	}};
}

int main(int, char**)
{
	const unique_ptr<plugin, decltype(&deleter)> p(new plugin(nullptr, string(CMAKE_BINARY_DIR) + "/test/mock/host_native/mock_host_native"), &deleter);

	auto* const ef(p->effect());

	unsigned char* chunk;

	const auto result = ef->dispatcher(ef, effSetChunk, 0, test_chunk.size(), const_cast<unsigned char*>(test_chunk.data()), 0.0f);
	cout << "result: " << result << endl;

	if (!result)
	{
		cerr << "chunks mispatch" << endl;
		return 1;
	}


	return 0;
}
