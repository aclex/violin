/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "host/wine/processing.h"

using namespace std;

using namespace violin::host;

namespace
{
	class test_wine_loading : public wine::processing<test_wine_loading>
	{
	public:
		explicit test_wine_loading(const string& vst_dll_path) :
			wine::processing<test_wine_loading>(vst_dll_path)
		{
			cout << "test_wine_loading::ctr" << endl;
		}

		intptr_t dispatch_to_master(int32_t opcode, int32_t index, intptr_t value, void* ptr, float opt)
		{
			cout << "dispatch_to_master(" << opcode << ", " << index << ", " << value << ", " << ptr << ", " << opt << ')' << endl;
			return true;
		}
	};
}

int main(int, char**)
{
	cout << "start" << endl;
	test_wine_loading t("/tmp/test.dll");
	cout << "after construction" << endl;

	return 0;
}
