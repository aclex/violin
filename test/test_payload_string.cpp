/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <iostream>

#include "message/payload/payload.h"
#include "message/payload/string.h"

using namespace std;

using namespace violin::message;

namespace
{
	typedef payload<std::string> my_payload;

	void test_ctor_std_string()
	{
		const string input { "test" };

		my_payload m(input);

		if (input != m.c_str() || input != m.payload_ptr())
		{
			throw runtime_error("test_ctor_std_string: strings mismatch: input: \""s + input + "\", payload found: \""s + m.c_str() + "\", payload_ptr: \"" + m.payload_ptr() + "\".");
		}

		if (input.c_str() == m.c_str() || input.c_str() == m.payload_ptr())
		{
			throw runtime_error("test_ctor_std_string: ctor appears to be shallow: pointers are equal.");
		}
	}

	void test_ctor_const_char()
	{
		const char input[] = { "test" };

		my_payload m(input);

		if (string(input) != m.c_str() || string(input) != m.payload_ptr())
		{
			throw runtime_error("test_ctor_const_char: strings mismatch: input: "s + input + ", payload found: "s + m.c_str() + '.');
		}

		if (input == m.c_str() || input == m.payload_ptr())
		{
			throw runtime_error("test_ctor_const_char: ctor appears to be shallow: pointers are equal.");
		}
	}

	void test_ctor_const_char_null()
	{
		my_payload m(nullptr);

		if (!m.c_str() || !m.payload_ptr())
		{
			throw runtime_error("test_ctor_const_char_null: uninitialized payload.");
		}

		if (string(m.c_str()) != string { })
		{
			throw runtime_error("test_ctor_const_char_null: payload initialized with garbage.");
		}
	}

	void test_ctor_default()
	{
		array<char, 5> input { "test" };

		my_payload m(0, 0, 0, input.data(), 0.0f);

		if (string(input.data()) != m.c_str() || string(input.data()) != m.payload_ptr())
		{
			throw runtime_error("test_ctor_default: input and payload mismatch: input: "s + input.data() + ", payload: "s + m.c_str() + '.');
		}
	}

	void test_copy_to_null()
	{
		const string input { "test" };

		my_payload m(input);

		m.copy(nullptr, 5);
	}

	void test_copy_limited()
	{
		const string input { "test" };

		my_payload m(input);

		array<char, 5> output;

		m.copy(output.data(), 2);

		if (input.substr(0, 2) != output.data())
		{
			throw runtime_error("test_copy_limited: input and output mismatch: input: "s + input.substr(0, 2) + ", output: "s + output.data() + '.');
		}
	}

	void test_copy()
	{
		const string input { "test" };

		my_payload m(input);

		array<char, 5> output;

		m.copy(output.data());

		if (input != output.data())
		{
			throw runtime_error("test_copy: input and output mismatch: input: "s + input + ", output: "s + output.data() + '.');
		}
	}

	void test_overflow()
	{
		const string input(256, 'a');

		my_payload m(input);

		array<char, 128> output;

		m.copy(output.data());

		if (input.substr(0, 64) != output.data())
		{
			throw runtime_error("test_overflow: input and output mismatch: input: "s + input.substr(0, 64) + ", output: "s + output.data() + '.');
		}
	}
}

int main(int, char**)
{
	test_ctor_std_string();
	test_ctor_const_char();
	test_ctor_const_char_null();
	test_ctor_default();
	test_copy_to_null();
	test_copy_limited();
	test_copy();

	return 0;
}
