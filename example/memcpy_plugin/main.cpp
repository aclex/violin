#include "plugin.h"

using namespace std;

extern "C"
{
	AEffect* VSTPluginMain(audioMasterCallback audio_master_callback);
}

namespace
{
	unique_ptr<memcpy_plugin> g_plugin;
}

AEffect* VSTPluginMain(audioMasterCallback audio_master_callback)
{
	g_plugin = make_unique<memcpy_plugin>(audio_master_callback);

	return g_plugin->effect();
}
