#include "plugin.h"

#include "plugin/info.h"

using namespace std;

using namespace violin;

memcpy_plugin::memcpy_plugin(audioMasterCallback audio_master_callback) noexcept :
	wrapper<memcpy_plugin>(audio_master_callback)
{
	const violin::plugin::info inf
	{
		0,
		0,
		2,
		2,
		effFlagsCanReplacing,
		0,
		4,
		1000
	};

	update_info(inf);
}
