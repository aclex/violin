#include <vector>
#include <cstring>

#include "plugin/wrapper.h"

class memcpy_plugin : public violin::plugin::wrapper<memcpy_plugin>
{
public:
	explicit memcpy_plugin(audioMasterCallback audio_master_callback) noexcept;

	std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
	{
		return 0;
	}

	float get_parameter(std::int32_t index)
	{
		return 0;
	}

	void set_parameter(std::int32_t index, float value)
	{

	}

	template<typename SampleType> void process(SampleType** inputs, SampleType** outputs, std::size_t size) noexcept
	{
		std::vector<std::unique_ptr<SampleType[]>> channels;

		for (int i = 0; i < 2; ++i)
		{
			auto channel(std::make_unique<SampleType[]>(size));
			std::copy(inputs[i], inputs[i] + size, channel.get());

			channels.push_back(std::move(channel));
		}

		for (int j = 0; j < size; ++j)
		{
			channels[0][j] *= 0.65;
		}

		for (int i = 0; i < 2; ++i)
		{
			std::copy(channels[i].get(), channels[i].get() + size, outputs[i]);
		}
	}
};
