#include <memory>

#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

#include "vst_header_chooser.h"

using namespace std;

extern "C"
{
	AEffect* VSTPluginMain(audioMasterCallback audio_master_callback);
	AEffect* mainStub(audioMasterCallback audioMasterProc) asm ("main");
}

namespace
{
	unique_ptr<AEffect> g_plugin;
}

void signalHandler(int signum)
{
	if(signum == SIGCHLD) {
	}
	else {
	}
}

AEffect* VSTPluginMain(audioMasterCallback audio_master_callback)
{
	// FIXME Without this signal handler the Renoise tracker is unable to start the child
	// winelib application.
	// signal(SIGCHLD, signalHandler);

	const pid_t pid1(fork());
	int status;

	if (pid1)
	{
		waitpid(pid1, &status, 0);
	}
	else if (!pid1)
	{
		const pid_t pid2(fork());

		if (pid2)
		{
			return nullptr;
		}
		else if (!pid2)
		{
			execl("/bin/sh", "/bin/sh", "/usr/local/bin/exec_rotate.sh", nullptr);
		}
		else
		{
			return nullptr;
		}
	}

	g_plugin = make_unique<AEffect>();

	g_plugin->magic = kEffectMagic;
	g_plugin->dispatcher = nullptr;
	g_plugin->getParameter = nullptr;
	g_plugin->setParameter = nullptr;
	g_plugin->processReplacing = nullptr;
	g_plugin->processDoubleReplacing = nullptr;

	g_plugin->numPrograms = 0;
	g_plugin->numParams = 0;
	g_plugin->numInputs = 2;
	g_plugin->numOutputs = 2;
	g_plugin->flags = effFlagsCanReplacing;
	g_plugin->uniqueID = 4;

	return g_plugin.get();
}

// Deprecated main() stub which is still used by some hosts
AEffect* mainStub(audioMasterCallback audioMasterProc)
{
	return VSTPluginMain(audioMasterProc);
}
