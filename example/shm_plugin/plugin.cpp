#include "plugin.h"

#include "plugin/info.h"

using namespace std;

using namespace violin;

shm_plugin::shm_plugin(audioMasterCallback audio_master_callback, mutex* m,
	condition_variable* cv,
	processing_state* st,
	const vector<float*>& remote_inputs,
	const vector<float*>& remote_outputs) noexcept :
	wrapper<shm_plugin>(audio_master_callback),
	m_mutex(m),
	m_cv(cv),
	m_st(st),
	m_remote_inputs(remote_inputs),
	m_remote_outputs(remote_outputs)
{
	const violin::plugin::info inf
	{
		0,
		0,
		2,
		2,
		effFlagsCanReplacing,
		0,
		4,
		1000
	};

	update_info(inf);
}
