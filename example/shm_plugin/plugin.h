#include <vector>
#include <mutex>
#include <condition_variable>
#include <cstring>
#include <fstream>
#include <iostream>
#include <thread>

#include "plugin/wrapper.h"

#include "processing_state.h"

class shm_plugin : public violin::plugin::wrapper<shm_plugin>
{
public:
	explicit shm_plugin(audioMasterCallback audio_master_callback, std::mutex* m,
		std::condition_variable* cv,
		violin::processing_state* st,
		const std::vector<float*>& remote_inputs, const std::vector<float*>& remote_outputs
		) noexcept;

	std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
	{
		return 0;
	}

	float get_parameter(std::int32_t index)
	{
		return 0;
	}

	void set_parameter(std::int32_t index, float value)
	{

	}

	template<typename SampleType> void process(SampleType** inputs, SampleType** outputs, std::size_t size) noexcept
	{
		// std::cout << "process " << static_cast<int>(*m_st) << " begin" << std::endl;
		// std::cout << "size: " << size << std::endl;
		{
			std::unique_lock<std::mutex> lock(*m_mutex);
			// std::cout << "process " << static_cast<int>(*m_st) << " locked 1" << std::endl;

			for (int i = 0; i < 2; ++i)
			{
				std::copy(inputs[i], inputs[i] + size, m_remote_inputs[i]);
			}

			*m_st = violin::processing_state::process;

			m_cv->notify_one();
			// std::cout << "process " << static_cast<int>(*m_st) << " notified 1" << std::endl;
		}

		{
			std::unique_lock<std::mutex> lock(*m_mutex);
			// std::cout << "process " << static_cast<int>(*m_st) << " locked 2, going to wait…" << std::endl;
			m_cv->wait(lock, [this]{ return *m_st == violin::processing_state::output; });
			// std::cout << "process " << static_cast<int>(*m_st) << " awake 2" << std::endl;

			for (int i = 0; i < 2; ++i)
			{
				std::copy(m_remote_outputs[i], m_remote_outputs[i] + size, outputs[i]);
			}

			*m_st = violin::processing_state::input;

			// std::cout << "process " << static_cast<int>(*m_st) << " finished, going to return…" << std::endl;
		}
		// std::cout << "process  " << static_cast<int>(*m_st) << "returning" << std::endl;
	}

private:
	std::mutex* m_mutex;
	std::condition_variable* m_cv;
	violin::processing_state* const m_st;
	std::ofstream m_log;
	const std::vector<float*> m_remote_inputs, m_remote_outputs;
};
