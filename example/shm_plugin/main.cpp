#include <fstream>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#include "process_shared.h"
#include "processing_state.h"

#include "plugin.h"

using namespace std;

using namespace violin;

extern "C"
{
	AEffect* VSTPluginMain(audioMasterCallback audio_master_callback);
}

namespace
{
	constexpr char name[] = "shm_plugin";

	unique_ptr<shm_plugin> g_plugin;
}

AEffect* VSTPluginMain(audioMasterCallback audio_master_callback)
{
	// signal(SIGCHLD, SIG_IGN);
	ofstream log("/tmp/shm_plugin_main.log");

	constexpr size_t block_size = 256;
	constexpr size_t map_size = 4 * block_size * sizeof(float) + sizeof(mutex) + sizeof(condition_variable) + sizeof(processing_state);

	log << "forked fine, on main" << endl;

	const int shm_fd = shm_open(name, O_CREAT | O_RDWR, 0600);

	if (shm_fd == -1)
	{
		return nullptr;
	}

	ftruncate(shm_fd, map_size);

	const int pid = fork();

	switch (pid)
	{
	case -1:
		return nullptr;

	case 0:
		execlp("wine64", "wine64", "/mnt/work/lab/vst/violin/build/example/shm_host/shm_host.exe.so", nullptr);

		// never reach here
		return nullptr;
	}

	unsigned char* ptr = static_cast<unsigned char*>(mmap(nullptr, map_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0));

	vector<float*> inputs, outputs;

	size_t offset = 0;
	for (size_t i = 0; i < 2; ++i)
	{
		float* const p(new (ptr + offset) float[block_size]);
		inputs.push_back(move(p));
		offset += sizeof(float) * block_size;
	}

	for (size_t i = 0; i < 2; ++i)
	{
		float* const p(new (ptr + offset) float[block_size]);
		outputs.push_back(move(p));
		offset += sizeof(float) * block_size;
	}

	mutex* m = new (ptr + offset) mutex;
	offset += sizeof(mutex);

	set_process_shared(*m);

	condition_variable* cv = new (ptr + offset) condition_variable;
	offset += sizeof(condition_variable);

	set_process_shared(*cv);

	processing_state* st = new (ptr + offset) processing_state;
	offset += sizeof(processing_state);

	*st = processing_state::input;

	log << "initialized structures" << endl;

	g_plugin = make_unique<shm_plugin>(audio_master_callback, m, cv, st, inputs, outputs);

	log << "created plugin instance" << endl;

	return g_plugin->effect();
}
