#include <iostream>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "process_shared.h"
#include "processing_state.h"

using namespace std;
using namespace std::chrono;

using namespace violin;

namespace
{
	constexpr char name[] = "shm_plugin";

	void process(const vector<float*>& inputs, const vector<float*>& outputs, std::int32_t size) noexcept
	{
		for (int i = 0; i < 2; ++i)
		{
			for (int j = 0; j < size; ++j)
			{
				outputs[i][j] = inputs[i][j] * 0.1;
			}
		}
	}
}

int main(int argc, char** argv)
{
	constexpr size_t block_size = 256;
	constexpr size_t map_size = 4 * block_size * sizeof(float) + sizeof(mutex) + sizeof(condition_variable) + sizeof(processing_state);

	const int shm_fd = shm_open(name, O_RDWR, 0600);

	if (shm_fd == -1)
	{
		cerr << "can't open shared memory file" << endl;
		return -1;
	}

	unsigned char* ptr = static_cast<unsigned char*>(mmap(nullptr, map_size, PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0));

	vector<float*> inputs, outputs;

	size_t offset = 0;
	for (size_t i = 0; i < 2; ++i)
	{
		float* const p(new (ptr + offset) float[block_size]);
		inputs.push_back(move(p));
		offset += sizeof(float) * block_size;
	}

	for (size_t i = 0; i < 2; ++i)
	{
		float* const p(new (ptr + offset) float[block_size]);
		outputs.push_back(move(p));
		offset += sizeof(float) * block_size;
	}

	mutex* m = reinterpret_cast<mutex*>(ptr + offset);
	offset += sizeof(mutex);

	condition_variable* cv = reinterpret_cast<condition_variable*>(ptr + offset);
	offset += sizeof(condition_variable);

	volatile processing_state* st = reinterpret_cast<volatile processing_state*>(ptr + offset);
	offset += sizeof(processing_state);

	while(true)
	{
		{
			// cout << static_cast<int>(*st) << " going to lock" << endl;
			unique_lock<mutex> lock(*m);
			// cout << static_cast<int>(*st) << " locked, going to wait…" << endl;
			cv->wait(lock, [st]{ return *st == processing_state::process; });
			// cout << static_cast<int>(*st) << " awake, processing…" << endl;

			process(inputs, outputs, block_size);

			*st = processing_state::output;
			cv->notify_one();

			// cout << static_cast<int>(*st) << " notified" << endl;
		}
	}
}
