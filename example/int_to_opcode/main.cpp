/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <map>

#include "opcode.h"

using namespace std;

using namespace violin::vst2;

int main(int argc, char* argv[])
{
	int opcode { 255 };
	int opcode_type { };

	if (argc > 1)
	{
		if (string(argv[1]) == "-h" || string(argv[1]) == "--help")
		{
			cout << "Usage: int_to_code [-h|--help] [OPCODE_TYPE] [OPCODE_NUMBER]" << endl;
			return 0;
		}

		opcode_type = atoi(argv[1]);

		if (argc > 2)
		{
			opcode = atoi(argv[2]);
		}
	}

	while ((opcode_type != 1 && opcode_type != 2) || opcode == 255)
	{
		if (opcode_type != 1 && opcode_type != 2)
		{
			cout << "Enter opcode type (1 - plugin, 2 - master): ";
			cin >> opcode_type;
		}

		cout << "Enter opcode number: ";
		cin >> opcode;
	}

	cout << "Opcode value is: ";
	switch (opcode_type)
	{
	case 1:
		cout << plugin_opcodes.at(opcode) << endl;
		break;

	case 2:
		cout << master_opcodes.at(opcode) << endl;
		break;

	default:
		break;
	}

	return 0;
}
