/*
  Violin
  Copyright (C) 2018-2020  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "log.h"

#include <fstream>
#include <thread>
#include <mutex>


using namespace std;

using namespace violin;
using namespace violin::log;

namespace
{
	const char g_log_path[] { "/var/log/violin.log" };
	string g_prefix;

	mutex g_print_mutex;

	std::basic_ostream<char>& log_sink()
	{
		static std::ofstream g_log { g_log_path, ios::app };

		return g_log;
	}
}

void violin::log::initialize(int argc, char* argv[])
{
	if (!argc)
	{
		g_prefix = "violin:wrapper:" + violin::basename(violin::so_path());
	}

	else
	{
		const auto plugin_name_fn = [argc, &argv]()
		{
			try
			{
				if (argc > 1)
				{
					const auto& dll_path { violin::dll_path(argv[1]) };

					if (dll_path)
						return violin::basename(dll_path.value());
				}
				
				return "<unknown>"s;
			}
			catch (const std::runtime_error&) // dll not registered
			{
				return "<unregistered>"s;
			}
		};

		g_prefix = "violin:host:" + plugin_name_fn();
	}
}

bool violin::log::initialized() noexcept
{
	return !g_prefix.empty();
}

void violin::log::print(const string& s)
{
	lock_guard<mutex> lock(g_print_mutex);
	log_sink() << g_prefix << '[' << this_thread::get_id() << "]: " << s << endl;
}
