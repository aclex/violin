/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_HMODULE_WRAPPER_H
#define VIOLIN_HOST_HMODULE_WRAPPER_H

#include <string>
#include <utility>
#include <stdexcept>

#include <wine/windows/windows.h>

#include "resource.h"

namespace violin
{
	namespace host
	{
		class hmodule_wrapper : protected resource
		{
		public:
			explicit hmodule_wrapper(const std::string& path) :
				m_hmodule(LoadLibrary(path.c_str()))
			{
				if (!m_hmodule)
				{
					const std::string& msg = "Can't load module at path " + path + '.';
					throw std::runtime_error(msg);
				}
			}

			hmodule_wrapper(const hmodule_wrapper&) = delete;
			hmodule_wrapper(hmodule_wrapper&&) = default;

			hmodule_wrapper& operator=(const hmodule_wrapper&) = delete;

			HMODULE handle() const
			{
				return m_hmodule;
			}

			~hmodule_wrapper() noexcept
			{
				if (owning())
				{
					FreeLibrary(m_hmodule);
				}
			}

		private:
			const HMODULE m_hmodule;
		};
	}
}

#endif // VIOLIN_HOST_HMODULE_WRAPPER_H
