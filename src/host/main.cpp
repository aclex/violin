/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>

#include "name.h"
#include "log.h"

#include "host/host.h"

#include "host/wine/processing.h"
#include "host/wine/editor_forwarding.h"

using namespace std;

using namespace violin::host;

namespace
{
	typedef basic_host<wine::processing, wine::editor_forwarding> host;
}


int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		cout << "Usage: " << argv[0] << " PLUGIN_PATH SHM_NAME_SUFFIX" << endl;
		return -1;
	}

	try
	{
		const string& wrapper_path(argv[1]);
		const auto& dll_path { violin::dll_path(wrapper_path) };

		if (!dll_path)
		{
			const auto& msg { "No registered plugin for wrapper at path \""s + wrapper_path + "\" found."s };
			cerr << msg << endl;
			return -2;
		}

		const string& plugin_path(dll_path.value());
		const string& shm_name_suffix(argv[2]);

		violin::log::initialize(argc, argv);

		violin::log::print("***** new session *****");
		violin::log::print(string("name suffix passed: ") + argv[2]);

		host h(wrapper_path, plugin_path, shm_name_suffix);

		h.run();
	}
	catch (const exception& e)
	{
		violin::log::print(string("host cannot start: ") + e.what());
		violin::log::print("----- exit due to failure -----");
		return -1;
	}

	violin::log::print("----- exit successfully -----");

	return 0;
}
