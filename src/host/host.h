/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_HOST_H
#define VIOLIN_HOST_HOST_H

#include <string>
#include <memory>
#include <algorithm>
#include <atomic>
#include <cstddef>

#include "vst_header_chooser.h"

#include "shared_cell.h"
#include "name.h"

#include "shm/channel.h"

#include "message/message.h"

#include "transfer/dispatch.h"

#include "host/transfer/control.h"
#include "host/transfer/audio.h"

namespace violin
{
	namespace plugin
	{
		struct info;
	}

	namespace host
	{
		template<template<class> class Processing, template<class> class EditorForwarding>
		class basic_host : public violin::host::transfer::control::control<basic_host<Processing, EditorForwarding>, typename EditorForwarding<basic_host<Processing, EditorForwarding>>::threading_traits::thread_type>,
			public violin::host::transfer::audio<basic_host<Processing, EditorForwarding>>,
			public Processing<basic_host<Processing, EditorForwarding>>,
			public EditorForwarding<basic_host<Processing, EditorForwarding>>
		{
		public:
			typedef typename EditorForwarding<basic_host<Processing, EditorForwarding>>::threading_traits threading_traits;

			basic_host(const std::string& wrapper_plugin_path, const std::string& original_plugin_path, const std::string& shm_name_suffix) :
				transfer::control::control<basic_host, typename threading_traits::thread_type>(plugin_name(wrapper_plugin_path), shm_name_suffix),
				transfer::audio<basic_host>(plugin_name(wrapper_plugin_path), shm_name_suffix),
				Processing<basic_host>(original_plugin_path),
				m_remote_dispatcher(this->to_master()),
				m_time_info(std::make_unique<VstTimeInfo>())
			{
				EditorForwarding<basic_host>::initialize();

				if (this->has_editor())
				{
					this->start_listening();
				}
			}

			void run()
			{
				if (!this->has_editor())
				{
					this->listen();
				}
				else
				{
					EditorForwarding<basic_host>::event_loop();
				}
			}

		protected:
			std::intptr_t dispatch_to_master(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
			{

				switch (opcode)
				{
				case audioMasterGetVendorString:
				case audioMasterGetProductString:
					return m_remote_dispatcher.get<std::string>(opcode, index, value, ptr, opt);

				case audioMasterCanDo:
					return m_remote_dispatcher.set<std::string>(opcode, index, value, ptr, opt);

				case audioMasterGetTime:
				{
					const auto result { m_remote_dispatcher.get<VstTimeInfo>(opcode, index, value, m_time_info.get(), opt) };

					if (result)
						return reinterpret_cast<std::intptr_t>(m_time_info.get());
					else
						return 0;
				}

				case audioMasterProcessEvents:
					return m_remote_dispatcher.set<VstEvents>(opcode, index, value, ptr, opt);

				default:
					return m_remote_dispatcher.process(opcode, index, value, ptr, opt);
				}
			}

			template<class MessageType, typename... Args> void send_to_plugin_part(Args... args)
			{
				m_remote_dispatcher.set<MessageType>(std::forward<Args>(args)...);
			}

		protected:
			void visit(violin::message::message& m) noexcept
			{
				std::visit([this](auto&& arg) { this->process(arg); }, m);
			}

			template<typename PayloadType> bool process(violin::message::dispatch<PayloadType>& m) noexcept
			{
				typename violin::message::dispatch<PayloadType>::unfolder_type u(m);
				u.set_result(dispatch_to_plugin(u.opcode(), u.index(), u.value(), u.ptr(), u.opt()));

				return true;
			}

			bool process(violin::message::request_info& m) noexcept
			{
				m.info = this->get_info();

				return true;
			}

			bool process(violin::message::get_parameter& m) noexcept
			{
				m.value = this->get_parameter(m.index);

				return true;
			}

			bool process(const violin::message::set_parameter& m) noexcept
			{
				this->set_parameter(m.index, m.value);

				return true;
			}

			template<typename MessageType> bool process(const MessageType&) noexcept
			{
				return false;
			}

			using Processing<basic_host>::process;

			std::intptr_t direct_dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
			{
				return Processing<basic_host>::dispatch_to_plugin(opcode, index, value, ptr, opt);
			}

			std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
			{
				switch (opcode)
				{
				case effOpen:
					this->open_audio();
					break;

				case effSetBlockSize:
					this->set_block_size(value);
					break;

				case effEditOpen:
				case effEditClose:
				case effEditIdle:
				case effEditGetRect:
					return this->dispatch_in_gui_thread(opcode, index, value, ptr, opt);
				}

				std::intptr_t result(direct_dispatch_to_plugin(opcode, index, value, ptr, opt));

				switch (opcode)
				{
				case effClose:
					quit();
					result = true;
					break;
				}

				return result;
			}

		private:
			void quit()
			{
				EditorForwarding<basic_host>::quit();
				violin::host::transfer::control::control<basic_host, typename threading_traits::thread_type>::quit();
				violin::host::transfer::audio<basic_host>::quit();
			}

			void set_buffer_size(std::size_t buffer_size);

			const std::unique_ptr<VstTimeInfo> m_time_info;

			violin::transfer::dispatch::remote_dispatcher m_remote_dispatcher;
		};
	}
}

#endif // VIOLIN_HOST_HOST_H
