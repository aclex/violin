/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_CONTROL_H
#define VIOLIN_HOST_CONTROL_H

#include "transfer/control.h"

namespace violin
{
	namespace host
	{
		namespace transfer
		{
			namespace control
			{
				class channel : public violin::transfer::control::basic_channel
				{
				public:
					explicit channel(const std::string& plugin_name, const std::string& shm_name_infix, const std::string& shm_name_suffix, const std::size_t stream_count) :
						m_shm_channel(create_shm_name(plugin_name, shm_name_infix, shm_name_suffix),
							calculate_channel_size(stream_count))
					{

					}

					violin::transfer::control::message_cell* map_cell()
					{
						return m_shm_channel.map<violin::transfer::control::message_cell>();
					}

				private:
					shm::channel<shm::creation::disabled, shm::resizing::remap> m_shm_channel;
				};

				template<class Host, class Thread> class control : public violin::transfer::control::listener<Host>
				{
				public:
					control(const std::string& plugin_name, const std::string& shm_name_suffix, const std::string& shm_name_infix = std::string()) :
						m_channel(plugin_name, shm_name_infix, shm_name_suffix, 2),
						m_to_plugin(m_channel.map_cell()),
						m_to_master(m_channel.map_cell())
					{

					}

					void start_listening()
					{
						m_listening_thread = Thread(&violin::transfer::control::listener<Host>::listen, this, m_to_plugin);
					}

					~control() noexcept
					{
						if (m_listening_thread.joinable())
						{
							m_listening_thread.join();
						}
					}

				protected:
					violin::transfer::control::message_cell* to_plugin()
					{
						return m_to_plugin;
					}

					violin::transfer::control::message_cell* to_master()
					{
						return m_to_master;
					}

					void listen()
					{
						this->listen(m_to_plugin);
					}

					void quit() noexcept
					{
						m_to_plugin->quit();
					}

				private:
					using violin::transfer::control::listener<Host>::listen;

					channel m_channel;
					Thread m_listening_thread;

					violin::transfer::control::message_cell * const m_to_plugin, * const m_to_master;
				};
			}
		}
	}
}

#endif // VIOLIN_HOST_CONTROL_H
