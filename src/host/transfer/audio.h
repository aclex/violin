/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_TRANSFER_AUDIO_H
#define VIOLIN_HOST_TRANSFER_AUDIO_H

#include <thread>
#include <cstddef>

#include "shared_state.h"
#include "processing_state.h"
#include "processing_format.h"
#include "name.h"

#include "shm/channel.h"

#include "transfer/audio.h"

namespace violin
{
	namespace host
	{
		namespace transfer
		{
			template<class Host> class audio : public violin::transfer::audio<Host>
			{
			public:
				audio(const std::string& plugin_name, const std::string& shm_name_suffix, const std::string& shm_name_infix = std::string()) :
					m_plugin_audio_channel(this->create_shm_name(plugin_name, shm_name_infix, shm_name_suffix), this->initial_channel_size()),
					m_buffer_initialized(),
					m_audio_open_requested()
				{

				}

				void open_audio()
				{
					m_audio_open_requested = true;

					if (m_buffer_initialized)
					{
						start_audio();
					}
				}

				void start_audio()
				{
					m_audio_thread = std::thread(&audio::run_audio, static_cast<Host*>(this));
					m_audio_open_requested = false;
				}

				void set_block_size(std::size_t buffer_size)
				{
					resize_audio_buffer(buffer_size);
					m_buffer_initialized = true;

					if (m_audio_open_requested)
					{
						start_audio();
					}
				}

				void resize_audio_buffer(std::size_t buffer_size)
				{
					violin::transfer::audio<Host>::resize_audio_buffer(m_plugin_audio_channel, buffer_size);
				}

				void initialize_state()
				{
					this->m_audio_state = m_plugin_audio_channel.map<typename violin::transfer::audio<Host>::state_type>();
					this->m_processing_format = m_plugin_audio_channel.map<processing_format>();
					this->m_processing_size = m_plugin_audio_channel.map<std::size_t>();
				}

				void run_audio()
				{
					while (true)
					{
						{
							auto tr(this->m_audio_state->request_transition(processing_state::process, processing_state::output));

							if (tr.quitting())
							{
								break;
							}

							switch (*(this->m_processing_format))
							{
							case processing_format::process_float:
								accessor::process(this, this->m_audio_buffer->template inputs<float>(), this->m_audio_buffer->template outputs<float>(), *(this->m_processing_size));
								break;

							case processing_format::process_double:
								accessor::process(this, this->m_audio_buffer->template inputs<double>(), this->m_audio_buffer->template outputs<double>(), *(this->m_processing_size));
								break;
							}
						}
					}
				}

				void quit()
				{
					if (this->m_audio_state)
						this->m_audio_state->quit();
				}

				~audio() noexcept
				{
					if (m_audio_thread.joinable())
					{
						m_audio_thread.join();
					}
				}

			private:
				struct accessor : public Host
				{
					template<typename SampleType> static void process(audio* const h, SampleType** inputs, SampleType** outputs, std::size_t size)
					{
						void (Host::* const fn)(SampleType**, SampleType**, std::size_t)(&Host::process);
						(static_cast<Host*>(h)->*fn)(inputs, outputs, size);
					}

				private:
					using Host::process;
				};

				shm::channel<shm::creation::disabled, shm::resizing::remap> m_plugin_audio_channel;
				std::thread m_audio_thread;

				bool m_buffer_initialized, m_audio_open_requested;
			};
		}
	}
}

#endif // VIOLIN_HOST_TRANSFER_AUDIO_H
