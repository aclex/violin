/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_WINE_EDITOR_FORWARDING_H
#define VIOLIN_HOST_WINE_EDITOR_FORWARDING_H

#include <atomic>

#include <windows.h>

#include "threading_traits/winapi.h"

#include "static_binder.h"

#include "message/parent_notify.h"

namespace violin
{
	namespace host
	{
		namespace wine
		{
			template<class Host> class editor_forwarding : private static_binder<UINT, editor_forwarding<Host>*>
			{
			public:
				typedef threading_traits::winapi threading_traits;

				editor_forwarding() :
					m_win_module(GetModuleHandle(nullptr)),
					m_win_class_atom(),
					m_hwnd(),
					m_message_hook_handle(),
					m_quitting(false),
					m_initialized(),
					m_opened(),
					m_shown()
				{

				}

				void initialize()
				{
					if (static_cast<Host*>(this)->has_editor())
					{
						force_create_message_queue();
						register_dispatch_message();
						save_gui_thread_handle();
						register_message_hook();

						m_initialized = register_window_class();
					}
				}

				bool editor_opened() const noexcept
				{
					return m_opened;
				}

				void* open_editor_pseudo_parent_window()
				{
					if (m_initialized)
					{
						close_editor_window();

						const ERect& rect(last_rect());

						m_hwnd = CreateWindowExA(WS_EX_TOOLWINDOW,
							reinterpret_cast<LPCTSTR>(m_win_class_atom), "Plugin",
							WS_POPUP,
							rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top,
							0, 0,
							m_win_module, 0);

						if (m_hwnd)
						{
							m_opened = true;
							bind_window_object(m_hwnd);
						}

						return m_hwnd;
					}

					return nullptr;
				}

				ERect* editor_window_rect() noexcept
				{
					ERect* rect;
					accessor::direct_dispatch_to_plugin(this, effEditGetRect, 0, 0, &rect, 0.0f);

					return rect;
				}

				ERect last_rect() noexcept
				{
					if (!m_last_rect)
						save_last_rect(editor_window_rect());

					return *m_last_rect;
				}

				void save_last_rect(ERect* const rect)
				{
					if (!rect)
						return;

					m_last_rect = std::make_unique<ERect>(*rect);
				}

				void close_editor_window()
				{
					if (m_opened)
					{
						DestroyWindow(m_hwnd);
						m_opened = false;
					}
				}

				void event_loop()
				{
					while (!m_quitting.load())
					{
						MSG msg;
						const BOOL bRet(GetMessage(&msg, 0, 0, 0));

						if (!bRet)
							break;

						if (bRet > 0)
						{
							if (!msg.hwnd)
							{
								try_process_visit_message(&msg);
							}
							else
							{
								TranslateMessage(&msg);
								DispatchMessage(&msg);
							}
						}
					}
				}

				void quit()
				{
					m_quitting.store(true);

					if (!m_hwnd)
						while(!PostThreadMessage(m_gui_thread_handle, WM_QUIT, 0, 0));
					else
						while(!PostMessage(m_hwnd, WM_QUIT, 0, 0));
				}

				std::intptr_t dispatch_in_gui_thread(const std::int32_t opcode, const std::int32_t index, const std::intptr_t value, void* const ptr, const float opt) noexcept
				{
					if (!static_cast<Host*>(this)->has_editor())
					{
						return false;
					}

					{
						auto tr { m_message_cell.request_transition(processing_state::input, processing_state::process) };

						tr.value() = violin::message::dispatch<> { opcode, index, value, ptr, opt };

						if (!m_hwnd)
							while(!PostThreadMessage(m_gui_thread_handle, m_dispatch_msg_id, 0, 0));
						else
							while(!PostMessage(m_hwnd, m_dispatch_msg_id, 0, 0));
					}

					{
						auto tr { m_message_cell.request_transition(processing_state::output, processing_state::input) };

						switch (opcode)
						{
						case effEditGetRect:
							save_last_rect(*static_cast<ERect**>(ptr));
							break;
						}

						return tr.value().extract_result();
					}
				}

				~editor_forwarding() noexcept
				{
					if (m_initialized)
					{
						this->unbind(m_dispatch_msg_id);
						UnregisterClass(s_window_class_name, m_win_module);
					}
				}

			private:
				class accessor : public Host
				{
				public:
					static std::intptr_t direct_dispatch_to_plugin(editor_forwarding* const h, std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
					{
						std::intptr_t (Host::* const fn)(std::int32_t, std::int32_t, std::intptr_t, void*, float)(&accessor::direct_dispatch_to_plugin);
						return (static_cast<Host*>(h)->*fn)(opcode, index, value, ptr, opt);
					}

					template<class MessageType, typename... Args>
					static void send_to_plugin_part(editor_forwarding* const h, Args... args)
					{
						void (Host::* const fn)(Args...)(&accessor::template send_to_plugin_part<MessageType, Args...>);
						(static_cast<Host*>(h)->*fn)(std::forward<Args>(args)...);
					}

				private:
					using Host::direct_dispatch_to_plugin;
					using Host::send_to_plugin_part;
				};

				void register_dispatch_message()
				{
					m_dispatch_msg_id = RegisterWindowMessage(s_dispatch_message_name);
					editor_forwarding::bind(m_dispatch_msg_id, this);
				}

				static void force_create_message_queue()
				{
					MSG msg;
					auto ret = PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
				}

				void bind_plugin_window(HWND plugin_hwnd)
				{
					m_plugin_hwnd = plugin_hwnd;
					bind_window_object(plugin_hwnd);
					const auto result(SetWindowLongPtr(plugin_hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(window_proc)));
					m_plugin_window_proc = reinterpret_cast<WNDPROC>(result);
				}

				void send_parent_notify()
				{
					accessor::template send_to_plugin_part<violin::message::parent_notify>(this,
						this->last_rect(),
						x11_window_handle(m_hwnd));
				}

				static unsigned long x11_window_handle(HWND hwnd) noexcept
				{
					return reinterpret_cast<unsigned long>(GetPropA(hwnd, s_wine_whole_window_xid_property_name));
				}

				void do_dispatch_in_gui_thread()
				{
					auto tr { m_message_cell.request_transition(processing_state::process, processing_state::output) };

					typename violin::message::dispatch<>::unfolder_type u(tr.value());

					std::int32_t opcode { u.opcode() };
					std::int32_t index { u.index() };
					std::intptr_t value { u.value() };
					void* ptr { u.ptr() };
					float opt { u.opt() };

					switch (u.opcode())
					{
					case effEditOpen:
						ptr = open_editor_pseudo_parent_window();
						break;
					}

					u.set_result(accessor::direct_dispatch_to_plugin(this, opcode, index, value, ptr, opt));
				}

				static LRESULT CALLBACK window_proc(HWND hwnd, UINT message, WPARAM w_param, LPARAM l_param)
				{
					editor_forwarding* const obj(static_cast<editor_forwarding*>(GetPropA(hwnd, s_object_property_name)));

					switch (message)
					{
					case WM_PARENTNOTIFY:
					{
						if (w_param == WM_CREATE)
						{
							if (obj && obj->m_hwnd == hwnd)
							{
								const HWND plugin_hwnd(reinterpret_cast<HWND>(l_param));

								obj->bind_plugin_window(plugin_hwnd);
								obj->send_parent_notify();
							}
						}
						break;
					}

					case WM_WINDOWPOSCHANGING:
					{
						const LONG style(GetWindowLongA(hwnd, GWL_STYLE));

						if (style & WS_CHILD)
						{
							if (obj && obj->m_hwnd == hwnd && !obj->m_shown)
							{
								obj->m_shown = true;
								ShowWindow(obj->m_hwnd, SW_SHOW);
								UpdateWindow(obj->m_hwnd);
							}
						}

						break;
					}

					case WM_DESTROY:
					{
						if (obj)
							obj->reset_window();

						break;
					}

					default:
						if (obj && message == obj->m_dispatch_msg_id)
						{
							obj->trigger_visit_event();
						}

						break;
					}

					if (obj)
						return obj->default_window_proc(hwnd, message, w_param, l_param);

					return DefWindowProc(hwnd, message, w_param, l_param);
				}

				LRESULT CALLBACK default_window_proc(HWND hwnd, UINT message, WPARAM w_param, LPARAM l_param)
				{
					if (hwnd == m_plugin_hwnd)
						return CallWindowProc(m_plugin_window_proc, hwnd, message, w_param, l_param);
					else
						return DefWindowProc(hwnd, message, w_param, l_param);
				}

				static LRESULT CALLBACK message_proc(int n_code, WPARAM w_param, LPARAM l_param)
				{
					if (n_code < 0)
						return CallNextHookEx(0, n_code, w_param, l_param);

					MSG* const msg(reinterpret_cast<MSG*>(l_param));

					if (!msg)
						return CallNextHookEx(0, n_code, w_param, l_param);

					if (!try_process_visit_message(msg))
						return CallNextHookEx(0, n_code, w_param, l_param);

					return true;
				}

				static bool try_process_visit_message(const MSG* const msg)
				{
					editor_forwarding* const obj(editor_forwarding::find(msg->message));

					if (!obj)
						return false;

					obj->trigger_visit_event();

					return true;
				}

				void trigger_visit_event()
				{
					if (!m_quitting.load())
						do_dispatch_in_gui_thread();
				}

				void save_gui_thread_handle()
				{
					m_gui_thread_handle = GetCurrentThreadId();
				}

				bool register_window_class()
				{
					if (m_win_class_atom)
					{
						return true;
					}

					WNDCLASSEX wclass { };
					wclass.style = CS_HREDRAW | CS_VREDRAW;
					wclass.cbSize = sizeof(WNDCLASSEX);
					wclass.lpfnWndProc = &editor_forwarding::window_proc;
					wclass.hInstance = m_win_module;
					wclass.hIcon = LoadIcon(nullptr, s_window_class_name);
					wclass.hCursor = LoadCursor(nullptr, IDC_ARROW);
					wclass.lpszClassName = s_window_class_name;

					m_win_class_atom = RegisterClassExA(&wclass);

					return m_win_class_atom || GetLastError() == ERROR_CLASS_ALREADY_EXISTS;
				}

				bool bind_window_object(HWND hwnd)
				{
					assert(hwnd);
					return SetPropA(hwnd, s_object_property_name, this);
				}

				bool register_message_hook()
				{

					return true;
				}

				bool unregister_message_hook()
				{
					if (!m_message_hook_handle)
						return true;

					return UnhookWindowsHookEx(m_message_hook_handle);
				}

				void reset_window()
				{
					m_hwnd = HWND { };
					m_plugin_hwnd = HWND { };
					m_shown = false;
					m_opened = false;
				}

				shared_cell<violin::message::dispatch<>, processing_state, state_process_scope::local, editor_forwarding::threading_traits> m_message_cell;

				HINSTANCE m_win_module;
				ATOM m_win_class_atom;
				HWND m_hwnd, m_plugin_hwnd;
				HHOOK m_message_hook_handle;
				WNDPROC m_plugin_window_proc;

				std::unique_ptr<ERect> m_last_rect;

				DWORD m_gui_thread_handle;
				UINT m_dispatch_msg_id;

				std::atomic<bool> m_quitting;

				bool m_initialized;
				bool m_opened;
				bool m_shown;

				static constexpr const char* s_window_class_name = "violin_wrapper";
				static constexpr const char* s_dispatch_message_name = "dispatch_violin_message";
				static constexpr const char* s_object_property_name = "violin_object";

				static constexpr const char* s_wine_whole_window_xid_property_name = "__wine_x11_whole_window";
			};
		}
	}
}

#endif // VIOLIN_HOST_WINE_EDITOR_FORWARDING_H
