/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_WINE_PROCESSING_H
#define VIOLIN_HOST_WINE_PROCESSING_H

#include <string>

#include "host/hmodule_wrapper.h"
#include "host/wrapper.h"

namespace violin
{
	namespace host
	{
		namespace wine
		{
			template<class Host> class processing : public wrapper<Host>
			{
			public:
				explicit processing(const std::string& vst_dll_path) :
					processing(hmodule_wrapper(vst_dll_path))
				{

				}

			private:
				explicit processing(hmodule_wrapper&& dll) :
					wrapper<Host>(find_vst_main(dll)),
					m_dll(std::move(dll))
				{

				}

				static typename wrapper<Host>::vst_main_func find_vst_main(const hmodule_wrapper& dll)
				{
					typename wrapper<Host>::vst_main_func result = reinterpret_cast<typename wrapper<Host>::vst_main_func>(GetProcAddress(dll.handle(), "VSTPluginMain"));

					if (!result)
					{
						result = reinterpret_cast<typename wrapper<Host>::vst_main_func>(GetProcAddress(dll.handle(), "main"));

						if (!result)
							throw std::runtime_error("Can't find entry point of VST plugin.");
					}

					return result;
				}

				const hmodule_wrapper m_dll;
			};
		}
	}
}

#endif // VIOLIN_HOST_WINE_PROCESSING_H
