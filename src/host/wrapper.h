/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_HOST_WRAPPER_H
#define VIOLIN_HOST_WRAPPER_H

#include <string>
#include <unordered_map>
#include <cstddef>
#include <cstdint>

#include "vst_header_chooser.h"

#include "plugin/info.h"

namespace violin
{
	namespace host
	{
		struct info;

		template<class Host> class wrapper
		{
		public:
			typedef AEffect* (VST_CALL_CONV *vst_main_func)(audioMasterCallback);

			explicit wrapper(vst_main_func vst_main) :
				m_vst_main(vst_main),
				m_effect(vst_main(&wrapper::audio_master_callback))
			{
				bind(m_effect, static_cast<Host*>(this));
			}

			plugin::info get_info() const noexcept
			{
				return violin::plugin::info
				{
					m_effect->numPrograms,
					m_effect->numParams,
					m_effect->numInputs,
					m_effect->numOutputs,
					m_effect->flags,
#ifdef USE_VST2SDK
					m_effect->initialDelay,
#else
					0,
#endif
					m_effect->uniqueID,
#ifdef USE_VST2SDK
					m_effect->version
#else
					0
#endif
				};
			}

			bool has_editor() const noexcept
			{
				return m_effect->flags & effFlagsHasEditor;
			}

			std::size_t input_count() const noexcept
			{
				return m_effect->numInputs;
			}

			std::size_t output_count() const noexcept
			{
				return m_effect->numOutputs;
			}

			std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
			{
				return m_effect->dispatcher(m_effect, opcode, index, value, ptr, opt);
			}

			float get_parameter(std::int32_t index)
			{
				return m_effect->getParameter(m_effect, index);
			}

			void set_parameter(std::int32_t index, float value)
			{
				m_effect->setParameter(m_effect, index, value);
			}

			void process(float** inputs, float** outputs, std::size_t size)
			{
				m_effect->processReplacing(m_effect, inputs, outputs, static_cast<std::int32_t>(size));
			}

			void process(double** inputs, double** outputs, std::size_t size)
			{
				m_effect->processDoubleReplacing(m_effect, inputs, outputs, static_cast<std::int32_t>(size));
			}

		private:
			class accessor : public Host
			{
			public:
				static std::intptr_t dispatch_to_master(wrapper* const h, std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					std::intptr_t (Host::* const fn)(std::int32_t, std::int32_t, std::intptr_t, void*, float)(&accessor::dispatch_to_master);
					return (static_cast<Host*>(h)->*fn)(opcode, index, value, ptr, opt);
				}

			private:
				using Host::dispatch_to_master;
			};

			static void bind(AEffect* const effect, Host* const host) noexcept
			{
				s_host_binding.emplace(effect, host);
			}

			static Host* unwrap(AEffect* const effect) noexcept
			{
				const auto cit = s_host_binding.find(effect);

				if (cit != s_host_binding.cend())
				{
					return cit->second;
				}
				else
				{
					return nullptr;
				}
			}

			static std::intptr_t VST_CALL_CONV audio_master_callback(AEffect* effect, std::int32_t opcode, std::int32_t index, intptr_t value, void* ptr, float opt)
			{
				if (!effect)
					return true;

				Host* const p(unwrap(effect));

				if (!p)
					return true;

				return accessor::dispatch_to_master(p, opcode, index, value, ptr, opt);
			}

			static std::unordered_map<AEffect*, Host*> s_host_binding;

			const vst_main_func m_vst_main;
			AEffect* const m_effect;
		};

		template<class Host> std::unordered_map<AEffect*, Host*> wrapper<Host>::s_host_binding;
	}
}

#endif // VIOLIN_HOST_WRAPPER_H
