/*
  Violin
  Copyright (C) 2018-2020  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_CONFIG_H
#define VIOLIN_CONFIG_H

#include <string>
#include <unordered_map>
#include <optional>

#include <peli/json/value.h>
#include <peli/json/iomanip.h>

namespace violin
{
	class config
	{
	public:
		config(const std::string& config_file_path = path());
		bool empty() const;
		std::optional<std::string> plugin_path(const std::string& wrapper_path) const noexcept;
		std::optional<std::string> wrapper_path(const std::string& plugin_path) const noexcept;
		void add_mapping(const std::string& wrapper_path, const std::string& plugin_path) noexcept;
		void remove_mapping(const std::string& wrapper_path) noexcept;
		std::unordered_map<std::string, std::string> list_mappings() noexcept;
		static std::string path();
		void save();
		~config();

	private:
		const std::string m_config_file_path;
		peli::json::value m_config_file;
		bool m_modified;
	};
}

#endif // VIOLIN_CONFIG_H
