/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_LIMIT_H
#define VIOLIN_LIMIT_H

#include <algorithm>
#include <cstddef>

#include "vst_header_chooser.h"

namespace violin
{
	constexpr const std::size_t kVstExtMaxParamStrLen = 32;
	constexpr std::size_t max_name_length = 64;

#ifdef USE_VST2SDK
	constexpr const std::size_t max_transfer_string_length = std::max(
		{
			static_cast<std::size_t>(kVstMaxProgNameLen),
			static_cast<std::size_t>(kVstMaxParamStrLen),
			static_cast<std::size_t>(kVstMaxVendorStrLen),
			static_cast<std::size_t>(kVstMaxProductStrLen),
			static_cast<std::size_t>(kVstMaxEffectNameLen),
			static_cast<std::size_t>(kVstMaxNameLen),
			static_cast<std::size_t>(kVstMaxLabelLen),
			static_cast<std::size_t>(kVstMaxShortLabelLen),
			static_cast<std::size_t>(kVstMaxCategLabelLen),
			static_cast<std::size_t>(kVstMaxFileNameLen),
			kVstExtMaxParamStrLen
		});
#else
	constexpr const std::size_t max_transfer_string_length = 64;
#endif
}

#endif // VIOLIN_LIMIT_H
