/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_WINE_THREAD_H
#define VIOLIN_WINE_THREAD_H

#include <memory>
#include <type_traits>
#include <functional>
#include <stdexcept>
#include <system_error>

#include <windows.h>

namespace violin
{
	namespace wine
	{
		class thread
		{
		public:
			typedef HANDLE native_handle_type;

			class id
			{
			public:
				id() noexcept : m_handle() { }
				explicit id(native_handle_type h) noexcept : m_handle(h) { }

				native_handle_type handle() const noexcept { return m_handle; }

				bool operator==(const id& a) const noexcept { return m_handle == a.m_handle; }
				bool operator!=(const id& a) const noexcept { return !(*this == a); }
				bool operator<(const id& a) const noexcept { return m_handle < a.m_handle; }
				bool operator<=(const id& a) const noexcept { return *this == a || *this < a; }
				bool operator>(const id& a) const noexcept { return m_handle > a.m_handle; }
				bool operator>=(const id& a) const noexcept { return *this == a || *this > a; }

				explicit operator bool() const noexcept { return m_handle; }

				template<typename Ostream> friend Ostream& operator<<(Ostream& os, const id& a)
				{
					os << a.m_handle;
					return os;
				}

			private:
				native_handle_type m_handle;
			};

			thread() noexcept { }
			template<class Function, class... Args>
			explicit thread(Function&& f, Args&&... args) :
				m_thread_proc(create_thread_proc(std::forward<Function>(f), std::forward<Args>(args)...))
			{
				m_id = id(CreateThread(nullptr, 0, wine_thread_proc, m_thread_proc.get(), 0, 0));

				if (m_id)
					m_joinable = true;
			}

			thread(thread&& other) noexcept
			{
				using std::swap;
				swap(*this, other);
			}

			thread(const thread&) = delete;

			thread& operator=(const thread&) = delete;

			thread& operator=(thread&& other) noexcept
			{
				using std::swap;
				swap(*this, other);

				return *this;
			}

			bool joinable() const noexcept
			{
				return m_joinable;
			}

			id get_id() const noexcept { return m_id; }
			native_handle_type native_handle() const noexcept { return m_id.handle(); }

			void join()
			{
				if (m_id.handle() == GetCurrentThread())
				{
					throw std::system_error(std::make_error_code(std::errc::resource_deadlock_would_occur));
				}

				if (!m_id)
				{
					throw std::system_error(std::make_error_code(std::errc::no_such_process));
				}

				if (!joinable())
				{
					throw std::system_error(std::make_error_code(std::errc::invalid_argument));
				}

				WaitForSingleObject(m_id.handle(), INFINITE);

				m_joinable = false;
			}

			~thread()
			{
				if (joinable())
				{
					std::terminate();
				}

				if (m_id)
					CloseHandle(m_id.handle());
			}

		private:
			friend void swap(thread&, thread&) noexcept;

			template<class T> std::decay_t<T> static constexpr decay_copy(T&& v) noexcept
			{
				return std::forward<T>(v);
			}

			template<class Function, class... Args>
			std::unique_ptr<std::function<void ()>> create_thread_proc(Function&& f, Args&&... args)
			{
				return std::make_unique<std::function<void ()>>(
					[f, args...]()
					{
						std::invoke(f, args...);
					});

			}

			DWORD WINAPI static wine_thread_proc(LPVOID param)
			{
				std::function<void ()>* thread_proc(static_cast<std::function<void ()>*>(param));
				(*thread_proc)();

				return 0;
			}

			std::unique_ptr<std::function<void ()>> m_thread_proc;
			id m_id;
			bool m_joinable { false };
		};

		void swap(thread& a, thread& b) noexcept
		{
			using std::swap;

			swap(a.m_thread_proc, b.m_thread_proc);
			swap(a.m_id, b.m_id);
			swap(a.m_joinable, b.m_joinable);
		}
	}
}

#endif // VIOLIN_WINE_THREAD_H
