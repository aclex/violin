/*
  Violin
  Copyright (C) 2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_WINE_CRITICAL_SECTION_H
#define VIOLIN_WINE_CRITICAL_SECTION_H

#include <synchapi.h>

namespace violin
{
	namespace wine
	{
		class condition_variable;

		class critical_section
		{
		public:
			critical_section()
			{
				InitializeCriticalSection(&m_critical_section);
			}

			critical_section(const critical_section&) = delete;
			critical_section& operator=(const critical_section&) = delete;

			void lock()
			{
				EnterCriticalSection(&m_critical_section);
			}

			bool try_lock()
			{
				return TryEnterCriticalSection(&m_critical_section);
			}

			void unlock()
			{
				LeaveCriticalSection(&m_critical_section);
			}

			~critical_section() noexcept
			{
				DeleteCriticalSection(&m_critical_section);
			}

		private:
			friend class wine::condition_variable;

			CRITICAL_SECTION m_critical_section;
		};
	}
}

#endif // VIOLIN_WINE_CRITICAL_SECTION_H
