/*
  Violin
  Copyright (C) 2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_WINE_CONDITION_VARIABLE_H
#define VIOLIN_WINE_CONDITION_VARIABLE_H

#include <condition_variable>
#include <chrono>

#include <synchapi.h>

namespace violin
{
	namespace wine
	{
		class condition_variable
		{
		public:
			condition_variable()
			{
				InitializeConditionVariable(&m_cond);
			}

			condition_variable(const condition_variable&) = delete;
			condition_variable& operator=(const condition_variable&) = delete;

			void notify_one() noexcept
			{
				WakeConditionVariable(&m_cond);
			}

			void notify_all() noexcept
			{
				WakeAllConditionVariable(&m_cond);
			}

			void wait(std::unique_lock<critical_section>& lock)
			{
				SleepConditionVariableCS(&m_cond, &lock.mutex()->m_critical_section, INFINITE);
			}
			template<class Predicate>
			void wait(std::unique_lock<critical_section>& lock, Predicate pred)
			{
				while (!pred())
					wait(lock);
			}

			template<class Clock, class Duration>
			std::cv_status wait_until(std::unique_lock<critical_section>& lock, const std::chrono::time_point<Clock, Duration>& timeout_time)
			{
				using namespace std::chrono;

				const auto dw_ms { duration_cast<milliseconds>(timeout_time - Clock::now()).count() };
				const auto result { SleepConditionVariableCS(&m_cond, &lock.mutex()->m_critical_section, dw_ms) };

				return to_cv_status(result);
			}

			template<class Clock, class Duration, class Predicate>
			bool wait_until(std::unique_lock<critical_section>& lock, const std::chrono::time_point<Clock, Duration>& timeout_time, Predicate pred)
			{
				while (!pred())
				{
					if (wait_until(lock, timeout_time) == std::cv_status::timeout)
					{
						return pred();
					}
				}

				return true;
			}

			template<class Rep, class Period>
			std::cv_status wait_for(std::unique_lock<critical_section>& lock, const std::chrono::duration<Rep, Period>& rel_time)
			{
				return wait_until(lock, std::chrono::steady_clock::now() + rel_time);
			}

			template<class Rep, class Period, class Predicate>
			bool wait_for(std::unique_lock<critical_section>& lock, const std::chrono::duration<Rep, Period>& rel_time, Predicate pred)
			{
				return wait_until(lock, std::chrono::steady_clock::now() + rel_time, std::move(pred));
			}

		private:
			constexpr static std::cv_status to_cv_status(bool winapi_status) noexcept
			{
				if (winapi_status)
					return std::cv_status::no_timeout;
				else
					return std::cv_status::timeout;
			}
			CONDITION_VARIABLE m_cond;
		};
	}
}

#endif // VIOLIN_WINE_CONDITION_VARIABLE_H
