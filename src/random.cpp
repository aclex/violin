/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "random.h"

#include <random>

using namespace std;

using namespace violin;


namespace
{
	default_random_engine& engine()
	{
		static random_device s_rd;
		static default_random_engine s_el(s_rd());

		return s_el;
	}
}

unsigned int violin::random_int(unsigned int max)
{
	uniform_int_distribution<unsigned int> dist(0, max);

	return dist(engine());
}

string violin::random_hex_string(size_t length)
{
	string result(length, 'v');

	static constexpr const char alphanum[] = "0123456789abcdef";

	for (size_t i = 0; i < length; ++i)
	{
		result[i] = alphanum[random_int(sizeof(alphanum) - 2)];
	}

	return result;
}
