/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "memory_mapping.h"

#include <algorithm>
#include <stdexcept>
#include <cstring>

#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

using namespace std;

using namespace violin;

memory_mapping::memory_mapping(const int fd, const size_t size) :
	m_size(size),
	m_offset(),
	m_ptr(mmap(fd, size)),
	m_fd(fd)
{

}

void memory_mapping::resize(const size_t new_size)
{
	if (!m_ptr && !m_size) // not `mmap`-ed yet due to zero initial size
	{
		m_ptr = mmap(m_fd, new_size);
		m_size = new_size;
		return;
	}

	const auto ptr(::mremap(m_ptr, m_size, new_size, MREMAP_MAYMOVE));

	if (ptr == MAP_FAILED)
	{
		const string& msg = "Remapping failed: " + string(strerror(errno));
		if (errno == EINVAL)
			throw invalid_argument(msg);
		else
			throw runtime_error(msg);
	}

	m_ptr = static_cast<unsigned char*>(ptr);
}

vector<unsigned char> memory_mapping::read() const
{
	return vector<unsigned char>(m_ptr, m_ptr + m_size);
}

void memory_mapping::write(void* const ptr) noexcept
{
	unsigned char* const cptr(static_cast<unsigned char* const>(ptr));
	copy(cptr, cptr + m_size, m_ptr);
}

unsigned char* memory_mapping::mmap(const int fd, const size_t size)
{
	if (!size)
	{
		// will receive EINVAL with zero size anyway, so don't even try and return `nullptr`
		return nullptr;
	}

	unsigned char* const result(static_cast<unsigned char*>(::mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)));

	if (result == MAP_FAILED)
	{
		const string& msg = "Mapping failed: " + string(strerror(errno));
		if (errno == EINVAL)
			throw invalid_argument(msg);
		else
			throw runtime_error(msg);
	}

	return result;
}

memory_mapping::~memory_mapping() noexcept
{
	if (!owning())
	{
		return;
	}

	munmap(m_ptr, m_size);
}
