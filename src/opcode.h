/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_OPCODE_H
#define VIOLIN_OPCODE_H

#include <map>
#include <string>

#include "vst_header_chooser.h"

namespace violin
{
	namespace vst2
	{
		const std::map<unsigned char, std::string> plugin_opcodes
		{
			{ 0, "effOpen" },
			{ 1, "effClose" },
			{ 2, "effSetProgram" },
			{ 3, "effGetProgram" },
			{ 4, "effSetProgramName" },
			{ 5, "effGetProgramName" },
			{ 6, "effGetParamLabel" },
			{ 7, "effGetParamDisplay" },
			{ 8, "effGetParamName" },
			{ 9, "effGetVu" },
			{ 10, "effSetSampleRate" },
			{ 11, "effSetBlockSize" },
			{ 12, "effMainsChanged" },
			{ 13, "effEditGetRect" },
			{ 14, "effEditOpen" },
			{ 15, "effEditClose" },
			{ 16, "effEditDraw" },
			{ 17, "effEditMouse" },
			{ 18, "effEditKey" },
			{ 19, "effEditIdle" },
			{ 20, "effEditTop" },
			{ 21, "effEditSleep" },
			{ 22, "effIdentify" },
			{ 23, "effGetChunk" },
			{ 24, "effSetChunk" },
			{ 25, "effProcessEvents" },
			{ 26, "effCanBeAutomated" },
			{ 27, "effString2Parameter" },
			{ 28, "effGetNumProgramCategories" },
			{ 29, "effGetProgramNameIndexed" },
			{ 30, "effCopyProgram" },
			{ 31, "effConnectInput" },
			{ 32, "effConnectOutput" },
			{ 33, "effGetInputProperties" },
			{ 34, "effGetOutputProperties" },
			{ 35, "effGetPlugCategory" },
			{ 36, "effGetCurrentPosition" },
			{ 37, "effGetDestinationBuffer" },
			{ 38, "effOfflineNotify" },
			{ 39, "effOfflinePrepare" },
			{ 40, "effOfflineRun" },
			{ 41, "effProcessVarIo" },
			{ 42, "effSetSpeakerArrangement" },
			{ 43, "effSetBlockSizeAndSampleRate" },
			{ 44, "effSetBypass" },
			{ 45, "effGetEffectName" },
			{ 46, "effGetErrorText" },
			{ 47, "effGetVendorString" },
			{ 48, "effGetProductString" },
			{ 49, "effGetVendorVersion" },
			{ 50, "effVendorSpecific" },
			{ 51, "effCanDo" },
			{ 52, "effGetTailSize" },
			{ 53, "effIdle" },
			{ 54, "effGetIcon" },
			{ 55, "effSetViewPosition" },
			{ 56, "effGetParameterProperties" },
			{ 57, "effKeysRequired" },
			{ 58, "effGetVstVersion" },
			{ 59, "effEditKeyDown" },
			{ 60, "effEditKeyUp" },
			{ 61, "effSetEditKnobMode" },
			{ 62, "effGetMidiProgramName" },
			{ 63, "effGetCurrentMidiProgram" },
			{ 64, "effGetMidiProgramCategory" },
			{ 65, "effHasMidiProgramsChanged" },
			{ 66, "effGetMidiKeyName" },
			{ 67, "effBeginSetProgram" },
			{ 68, "effEndSetProgram" },
			{ 69, "effGetSpeakerArrangement" },
			{ 70, "effShellGetNextPlugin" },
			{ 71, "effStartProcess" },
			{ 72, "effStopProcess" },
			{ 73, "effSetTotalSampleToProcess" },
			{ 74, "effSetPanLaw" },
			{ 75, "effBeginLoadBank" },
			{ 76, "effBeginLoadProgram" },
			{ 77, "effSetProcessPrecision" },
			{ 78, "effGetNumMidiInputChannels" },
			{ 79, "effGetNumMidiOutputChannels" }
		};

		const std::map<unsigned char, std::string> master_opcodes
		{
			{ 0, "audioMasterAutomate" },
			{ 1, "audioMasterVersion" },
			{ 2, "audioMasterCurrentId" },
			{ 3, "audioMasterIdle" },
			{ 4, "audioMasterPinConnected" },
			{ 6, "audioMasterWantMidi" },
			{ 7, "audioMasterGetTime" },
			{ 8, "audioMasterProcessEvents" },
			{ 9, "audioMasterSetTime" },
			{ 10, "audioMasterTempoAt" },
			{ 11, "audioMasterGetNumAutomatableParameters" },
			{ 12, "audioMasterGetParameterQuantization" },
			{ 13, "audioMasterIOChanged" },
			{ 14, "audioMasterNeedIdle" },
			{ 15, "audioMasterSizeWindow" },
			{ 16, "audioMasterGetSampleRate" },
			{ 17, "audioMasterGetBlockSize" },
			{ 18, "audioMasterGetInputLatency" },
			{ 19, "audioMasterGetOutputLatency" },
			{ 20, "audioMasterGetPreviousPlug" },
			{ 21, "audioMasterGetNextPlug" },
			{ 22, "audioMasterWillReplaceOrAccumulate" },
			{ 23, "audioMasterGetCurrentProcessLevel" },
			{ 24, "audioMasterGetAutomationState" },
			{ 25, "audioMasterOfflineStart" },
			{ 26, "audioMasterOfflineRead" },
			{ 27, "audioMasterOfflineWrite" },
			{ 28, "audioMasterOfflineGetCurrentPass" },
			{ 29, "audioMasterOfflineGetCurrentMetaPass" },
			{ 30, "audioMasterSetOutputSampleRate" },
			{ 31, "audioMasterGetOutputSpeakerArrangement" },
			{ 32, "audioMasterGetVendorString" },
			{ 33, "audioMasterGetProductString" },
			{ 34, "audioMasterGetVendorVersion" },
			{ 35, "audioMasterVendorSpecific" },
			{ 36, "audioMasterSetIcon" },
			{ 37, "audioMasterCanDo" },
			{ 38, "audioMasterGetLanguage" },
			{ 39, "audioMasterOpenWindow" },
			{ 40, "audioMasterCloseWindow" },
			{ 41, "audioMasterGetDirectory" },
			{ 42, "audioMasterUpdateDisplay" },
			{ 43, "audioMasterBeginEdit" },
			{ 44, "audioMasterEndEdit" },
			{ 45, "audioMasterOpenFileSelector" },
			{ 46, "audioMasterCloseFileSelector" },
			{ 47, "audioMasterEditFile" },
			{ 48, "audioMasterGetChunkFile" },
			{ 49, "audioMasterGetInputSpeakerArrangement" }
		};

		constexpr bool is_gui_command(const unsigned char opcode) noexcept
		{
			switch (opcode)
			{
			case effEditOpen:
			case effEditClose:
			case effEditGetRect:
			case effEditIdle:
				return true;

			default:
				return false;
			}
		}
	}
}

#endif // VIOLIN_OPCODE_H
