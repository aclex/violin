/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_NAME_H
#define VIOLIN_NAME_H

#include <string>
#include <vector>
#include <optional>

#include "limit.h"

#include "transfer/chunk/request_type.h"

namespace violin
{
#ifdef USE_VST2SDK
	constexpr std::size_t max_effect_name_length = kVstMaxEffectNameLen;
#else
	constexpr std::size_t max_effect_name_length = max_transfer_string_length;
#endif

	std::string basename(const std::string& path);
	std::string dirname(const std::string& path);
	std::string plugin_name(const std::string& plugin_path);
	std::string so_path();
	std::optional<std::string> dll_path(const std::string& wrapper_path);

	constexpr char shm_name_delimiter() noexcept { return '_'; }

	std::string control_shm_name_prefix(const std::string& plugin_name);
	std::string audio_shm_name_prefix(const std::string& plugin_name);
	template<transfer::chunk::request_type t> std::string chunk_shm_name_prefix(const std::string& plugin_name);

	template<> std::string chunk_shm_name_prefix<transfer::chunk::request_type::get>(const std::string& plugin_name);
	template<> std::string chunk_shm_name_prefix<transfer::chunk::request_type::set>(const std::string& plugin_name);

	constexpr const std::size_t shm_name_suffix_length(8);

	std::string generate_shm_name_suffix();

	template<typename DelimType>
	std::string join(const std::vector<std::string>& strings, DelimType delim)
	{
		std::string result;
		result.reserve(strings.size() * 10);

		for (auto it = strings.cbegin(); it != strings.cend(); ++it)
		{
			if (it->empty())
				continue;

			if (it != strings.cbegin())
				result += delim;

			result += *it;
		}

		return result;
	}

	// inspired by Alec Thomas's answer on Stack Overflow:
	// https://stackoverflow.com/a/7408245/4297846
	template<typename DelimType>
	std::vector<std::string> split(const std::string& text, DelimType sep, bool skip_empty_tokens = true)
	{
		std::vector<std::string> tokens;

		std::string::size_type start { }, end { };

		const std::string::size_type sep_length(std::is_same<DelimType, std::string::value_type>::value ? 1 : std::string(sep).length());

		while ((end = text.find(sep, start)) != std::string::npos)
		{
			if (!skip_empty_tokens || end != start)
			{
				tokens.push_back(text.substr(start, end - start));
			}

			start = end + sep_length;
		}

		if (!skip_empty_tokens || start < text.length())
		{
			tokens.push_back(text.substr(start));
		}

		return tokens;
	}

	template<typename... Args> std::string create_shm_name(Args... args)
	{
		return join(std::vector<std::string> { args... }, shm_name_delimiter());
	}
}

#endif // VIOLIN_NAME_H
