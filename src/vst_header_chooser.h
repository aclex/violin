/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_VST_HEADER_CHOOSER_H
#define VIOLIN_VST_HEADER_CHOOSER_H

#ifdef USE_VST2SDK

#include <aeffect.h>
#include <aeffectx.h>

#define VST_CALL_CONV VSTCALLBACK

#else

#include <cstdint>

#include <vestige.h>

struct ERect
{
	std::uint16_t left, top, bottom, right;
};

// Taken from Carla project (https://github.com/falkTX/Carla)
#define effSetProgramName 4
#define effGetParamLabel 6
#define effGetParamDisplay 7
#define effGetChunk 23
#define effSetChunk 24
#define effBeginLoadBank 75
#define effBeginLoadProgram 76

#endif

#endif // VIOLIN_VST_HEADER_CHOOSER_H
