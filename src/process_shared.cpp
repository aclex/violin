/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "process_shared.h"

#include <mutex>
#include <condition_variable>

#include <pthread.h>

using namespace std;

void violin::set_process_shared(mutex& m) noexcept
{
	pthread_mutex_t* const raw_mtx(m.native_handle());

	pthread_mutex_destroy(raw_mtx);

	pthread_mutexattr_t attrs;
	pthread_mutexattr_init(&attrs);

	pthread_mutexattr_setpshared(&attrs, PTHREAD_PROCESS_SHARED);

	pthread_mutex_init(raw_mtx, &attrs);

	pthread_mutexattr_destroy(&attrs);
}

void violin::set_process_shared(condition_variable& cv) noexcept
{
	pthread_cond_t* const raw_cv(cv.native_handle());

	pthread_cond_destroy(raw_cv);

	pthread_condattr_t attrs;
	pthread_condattr_init(&attrs);

	pthread_condattr_setpshared(&attrs, PTHREAD_PROCESS_SHARED);

	pthread_cond_init(raw_cv, &attrs);

	pthread_condattr_destroy(&attrs);
}
