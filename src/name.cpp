/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "name.h"

#include <link.h>
#include <dlfcn.h>
#include <limits.h>
#include <stdlib.h>

#include "random.h"

#include "config.h"

using namespace std;

using namespace violin;

namespace
{
	string base(const string& path)
	{
		size_t last_slash_pos(path.rfind('/'));

		if (last_slash_pos == string::npos)
		{
			last_slash_pos = 0;
		}
		else
		{
			++last_slash_pos; // advance to next character
		}

		return path.substr(last_slash_pos);
	}

	string dir(const string& path)
	{
		size_t last_slash_pos(path.rfind('/'));

		if (last_slash_pos == string::npos)
		{
			last_slash_pos = 0;
		}

		return path.substr(0, last_slash_pos);
	}

	string stem(const string& path)
	{
		size_t last_slash_pos(path.rfind('/'));

		if (last_slash_pos == string::npos)
		{
			last_slash_pos = 0;
		}
		else
		{
			++last_slash_pos; // advance to next character
		}

		size_t last_dot_pos(path.rfind('.'));

		if (last_dot_pos < last_slash_pos)
		{
			last_dot_pos = string::npos;
		}

		return path.substr(last_slash_pos, last_dot_pos - last_slash_pos);
	}

	constexpr const char* g_violin_shm_prefix = "/violin";

	template<transfer::chunk::request_type t> string create_chunk_shm_name_prefix(const string& plugin_name)
	{
		return create_shm_name(g_violin_shm_prefix, plugin_name, transfer::chunk::request_infix<t>, "chunk");
	}

}

string violin::basename(const string& path)
{
	return base(path);
}

string violin::dirname(const string& path)
{
	return dir(path);
}

string violin::plugin_name(const string& plugin_path)
{
	return stem(plugin_path);
}

string violin::so_path()
{
	Dl_info dl_info;
	dladdr(reinterpret_cast<const void*>(&so_path), &dl_info);
	char buf[PATH_MAX];

	if (realpath(dl_info.dli_fname, buf))
	{
		return buf;
	}
	else
	{
		const string msg { strerror(errno) };
		throw runtime_error(msg);
	}
}

optional<string> violin::dll_path(const string& wrapper_path)
{
	const config cfg(config::path());

	return cfg.plugin_path(wrapper_path);
}

string violin::control_shm_name_prefix(const string& plugin_name)
{
	return create_shm_name(g_violin_shm_prefix, plugin_name, "control");
}

string violin::audio_shm_name_prefix(const string& plugin_name)
{
	return create_shm_name(g_violin_shm_prefix, plugin_name, "audio");
}

template<> string violin::chunk_shm_name_prefix<transfer::chunk::request_type::get>(const string& plugin_name)
{
	return create_chunk_shm_name_prefix<transfer::chunk::request_type::get>(plugin_name);
}

template<> string violin::chunk_shm_name_prefix<transfer::chunk::request_type::set>(const string& plugin_name)
{
	return create_chunk_shm_name_prefix<transfer::chunk::request_type::set>(plugin_name);
}

string violin::generate_shm_name_suffix()
{
	return random_hex_string(shm_name_suffix_length);
}
