/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_RESOURCE_H
#define VIOLIN_RESOURCE_H

#include <utility> // for `std::swap`

namespace violin
{
	class resource
	{
	protected:
		resource() noexcept :
			m_owning(true)
		{

		}

		resource(const resource&) = delete;

		resource(resource&& other) noexcept :
			m_owning(false)
		{
			using std::swap;
			swap(m_owning, other.m_owning);
		}

		resource& operator=(const resource&) = delete;

		resource& operator=(resource&& other) noexcept
		{
			m_owning = false;

			using std::swap;
			swap(m_owning, other.m_owning);

			return *this;
		}

		bool owning() const noexcept
		{
			return m_owning;
		}

	private:
		bool m_owning;
	};
}

#endif // VIOLIN_RESOURCE_H
