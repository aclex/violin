/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_WRAPPER_H
#define VIOLIN_PLUGIN_WRAPPER_H

#include <memory>

#include "vst_header_chooser.h"

#include "info.h"

namespace violin
{
	namespace plugin
	{
		struct info;

		template<class Plugin> class wrapper
		{
		public:
			explicit wrapper(audioMasterCallback audio_master_callback);

			AEffect* effect() noexcept
			{
				return m_effect.get();
			}

			const AEffect* effect() const noexcept
			{
				return m_effect.get();
			}

			std::size_t input_count() const noexcept
			{
				return m_effect->numInputs;
			}

			std::size_t output_count() const noexcept
			{
				return m_effect->numOutputs;
			}

		protected:
			void update_info(const plugin::info& info) noexcept;
			intptr_t dispatch_to_master(std::int32_t opcode, std::int32_t index, std::int32_t value, void* ptr, float opt) noexcept
			{
				return m_audio_master_callback(m_effect.get(), opcode, index, value, ptr, opt);
			}

		private:
			struct accessor : public Plugin
			{
				template<typename SampleType> static void process(wrapper* const h, SampleType** inputs, SampleType** outputs, std::size_t size)
				{
					void (Plugin::* const fn)(SampleType**, SampleType**, std::size_t)(&accessor::process);
					(static_cast<Plugin*>(h)->*fn)(inputs, outputs, size);
				}

			private:
				using Plugin::process;
			};

			static constexpr Plugin* unwrap(AEffect* const effect) noexcept
			{
				auto* const ptr
				{
#ifdef USE_VST2SDK
					effect->object
#else
					effect->user
#endif
				};

				return static_cast<Plugin*>(ptr);
			}

			static std::intptr_t dispatch(AEffect* effect, std::int32_t opcode, std::int32_t index, intptr_t value, void* ptr, float opt)
			{
				Plugin* const p(wrapper::unwrap(effect));

				const intptr_t result(p->dispatch_to_plugin(opcode, index, value, ptr, opt));

				if (opcode == effClose)
				{
					delete p;
#ifdef USE_VST2SDK
					effect->object = nullptr;
#else
					effect->user = nullptr;
#endif

					return 1;
				}

				return result;
			}

			static float get_parameter(AEffect* effect, std::int32_t index) noexcept
			{
				return wrapper::unwrap(effect)->get_parameter(index);
			}

			static void set_parameter(AEffect* effect, std::int32_t index, float value) noexcept
			{
				return wrapper::unwrap(effect)->set_parameter(index, value);
			}

			static void process_replacing(AEffect* effect, float** inputs, float** outputs, std::int32_t size) noexcept
			{
				accessor::process(wrapper::unwrap(effect), inputs, outputs, size);
			}

			static void process_double_replacing(AEffect* effect, double** inputs, double** outputs, std::int32_t size) noexcept
			{
				accessor::process(wrapper::unwrap(effect), inputs, outputs, size);
			}

			audioMasterCallback const m_audio_master_callback;
			const std::unique_ptr<AEffect> m_effect;
		};
	}
}

#include "wrapper.tcc"

#endif // VIOLIN_PLUGIN_WRAPPER_H
