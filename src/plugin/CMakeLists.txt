set(VIOLIN_PLUGIN_SOURCES
	wine_starter.cpp
	native_starter.cpp
	main.cpp
	)

include_directories(${XCB_INCLUDE_DIRS})

add_library(plugin SHARED ${VIOLIN_PLUGIN_SOURCES})
target_link_libraries(plugin violin ${XCB_LIBRARIES})
install(TARGETS plugin LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/violin)

install(CODE "execute_process(COMMAND ${CMAKE_INSTALL_FULL_BINDIR}/violin-update)")
