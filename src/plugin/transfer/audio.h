/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_TRANSFER_AUDIO_H
#define VIOLIN_PLUGIN_TRANSFER_AUDIO_H

#include <cstddef>

#include "shared_state.h"
#include "processing_state.h"
#include "processing_format.h"
#include "name.h"

#include "shm/channel.h"

#include "transfer/audio.h"

namespace violin
{
	namespace plugin
	{
		namespace transfer
		{
			template<class Plugin> class audio : public violin::transfer::audio<Plugin>
			{
			public:
				audio(const std::string& plugin_name, const std::string& shm_name_suffix, const std::string& shm_name_infix = std::string()) :
					m_host_audio_channel(this->create_shm_name(plugin_name, shm_name_infix, shm_name_suffix), initial_channel_size())
				{

				}

				void resize_audio_buffer(std::size_t buffer_size)
				{
					violin::transfer::audio<Plugin>::resize_audio_buffer(m_host_audio_channel, buffer_size);
				}

				void initialize_state()
				{
					this->m_audio_state = m_host_audio_channel.make_placed<typename violin::transfer::audio<Plugin>::state_type>(processing_state::input);
					this->m_processing_format = m_host_audio_channel.make_placed<processing_format>();
					this->m_processing_size = m_host_audio_channel.make_placed<std::size_t>();
				}

				void deinitialize_state()
				{
					using DtorType = typename violin::transfer::audio<Plugin>::state_type;

					if (this->m_audio_state)
						this->m_audio_state->~DtorType();
				}

				template<typename SampleType> void process(SampleType** inputs, SampleType** outputs, std::size_t size)
				{
					const std::size_t input_count(static_cast<Plugin*>(this)->input_count()), output_count(static_cast<Plugin*>(this)->output_count());

					{
						auto tr(this->m_audio_state->request_transition(processing_state::input, processing_state::process));

						if (tr.quitting())
						{
							return;
						}

						for (std::size_t i = 0; i < input_count; ++i)
							std::copy(inputs[i], inputs[i] + size, this->m_audio_buffer->template input<SampleType>(i));

						*(this->m_processing_format) = get_processing_format<SampleType>();
						*(this->m_processing_size) = size;
					}

					{
						auto tr(this->m_audio_state->request_transition(processing_state::output, processing_state::input));

						if (tr.quitting())
						{
							return;
						}

						for (std::size_t i = 0; i < output_count; ++i)
							std::copy(this->m_audio_buffer->template output<SampleType>(i),
								this->m_audio_buffer->template output<SampleType>(i) + size, outputs[i]);
					}
				}

			private:
				static std::size_t initial_channel_size() noexcept
				{
					return sizeof(typename violin::transfer::audio<Plugin>::state_type) + sizeof(processing_format);
				}

				shm::channel<shm::creation::enabled, shm::resizing::full> m_host_audio_channel;
			};
		}
	}
}

#endif // VIOLIN_PLUGIN_TRANSFER_AUDIO_H
