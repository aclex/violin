/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_TRANSFER_CONTROL_H
#define VIOLIN_PLUGIN_TRANSFER_CONTROL_H

#include <thread>

#include "transfer/control.h"

#include "shm/channel.h"

namespace violin
{
	namespace plugin
	{
		namespace transfer
		{
			namespace control
			{
				class channel : public violin::transfer::control::basic_channel
				{
				public:
					explicit channel(const std::string& plugin_name, const std::string& shm_name_infix, const std::string& shm_name_suffix, const std::size_t stream_count) :
						m_shm_channel(create_shm_name(plugin_name, shm_name_infix, shm_name_suffix),
							calculate_channel_size(stream_count))
					{

					}

					violin::transfer::control::message_cell* make_cell()
					{
						return m_shm_channel.make_placed<violin::transfer::control::message_cell>();
					}

				private:
					shm::channel<shm::creation::enabled, shm::resizing::full> m_shm_channel;
				};

				template<class Plugin> class control : public violin::transfer::control::listener<Plugin>
				{
				public:
					control(const std::string& plugin_name, const std::string& shm_name_suffix, const std::string& shm_name_infix = std::string()) :
						m_channel(plugin_name, shm_name_infix, shm_name_suffix, 2),
						m_to_plugin(m_channel.make_cell()),
						m_to_master(m_channel.make_cell())
					{

					}

					void start_listening()
					{
						m_listening_thread = std::thread(&control::listen, this, m_to_master);
					}

					~control() noexcept
					{
						if (m_listening_thread.joinable())
						{
							m_listening_thread.join();
						}
					}

				protected:
					violin::transfer::control::message_cell* to_plugin()
					{
						return m_to_plugin;
					}

					violin::transfer::control::message_cell* to_master()
					{
						return m_to_master;
					}

					void quit() noexcept
					{
						// to not depend on host's good behaviour we stop master stream ourselves
						m_to_master->quit();
					}

				private:
					channel m_channel;
					std::thread m_listening_thread;

					violin::transfer::control::message_cell * const m_to_plugin, * const m_to_master;
				};
			}
		}
	}
}

#endif // VIOLIN_PLUGIN_TRANSFER_CONTROL_H
