/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_TRANSFER_CHUNK_H
#define VIOLIN_PLUGIN_TRANSFER_CHUNK_H

#include "transfer/chunk/chunk.h"

namespace violin
{
	namespace plugin
	{
		namespace transfer
		{
			template<violin::transfer::chunk::request_type t> class basic_chunk
			{
			public:
				static constexpr const violin::transfer::chunk::request_type request_type = violin::transfer::chunk::request_type::get;

				explicit basic_chunk(const std::string& plugin_name, const std::size_t size) :
					m_channel(create_shm_name(chunk_shm_name_prefix<request_type>(plugin_name), generate_shm_name_suffix()), size)
				{

				}

				std::string channel_name() const
				{
					return m_channel.name();
				}

				violin::transfer::chunk::cookie<request_type> create_cookie() noexcept
				{
					violin::transfer::chunk::cookie<request_type> cke { };
					std::strncpy(cke.name, m_channel.name().c_str(), violin::transfer::chunk::name_length);

					return cke;
				}

			private:
				static constexpr shm::resizing my_resizing() noexcept
				{
					if constexpr (request_type == violin::transfer::chunk::request_type::get)
					{
						return shm::resizing::remap;
					}
					else
					{
						return shm::resizing::disabled;
					}
				}

			protected:
				typedef shm::channel<shm::creation::enabled, my_resizing()> channel_type;

				channel_type& channel() noexcept
				{
					return m_channel;
				}

			private:
				channel_type m_channel;
			};

			template<violin::transfer::chunk::request_type t> class chunk;

			template<> class chunk<violin::transfer::chunk::request_type::get> : public basic_chunk<violin::transfer::chunk::request_type::get>
			{
			public:

				explicit chunk(const std::string& plugin_name) :
					basic_chunk<violin::transfer::chunk::request_type::get>(plugin_name, 0)
				{

				}

				void read(const std::size_t size)
				{
					channel().resize(size);
					m_local_storage = channel().read();
				}

				void* data()
				{
					return m_local_storage.data();
				}

			private:
				std::vector<unsigned char> m_local_storage;
			};

			template<> class chunk<violin::transfer::chunk::request_type::set> : public basic_chunk<violin::transfer::chunk::request_type::set>
			{
			public:
				using basic_chunk<violin::transfer::chunk::request_type::set>::basic_chunk;

				void write(void* const ptr)
				{
					channel().write(ptr);
				}
			};
		}
	}
}

#endif // VIOLIN_PLUGIN_TRANSFER_CHUNK_H
