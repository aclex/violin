/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using namespace std;

#include "name.h"

#include "plugin.h"
#include "wine_starter.h"

using namespace violin::plugin;

extern "C"
{
	AEffect* VSTPluginMain(audioMasterCallback master_callback);
}

namespace
{
	typedef basic_plugin<wine_starter> plugin;

	const string g_lib_dir { LIBDIR };
	const string g_host_wine_executable_path { violin::join(vector<string> { g_lib_dir, "violin", "host.exe.so" }, '/') };
}

AEffect* VSTPluginMain(audioMasterCallback master_callback)
{
	violin::log::initialize();
	// object is deleted in the wrapper on effClose request from master
	try
	{
		plugin* const p(new plugin(master_callback, g_host_wine_executable_path));
		return p->effect();
	}
	catch (const runtime_error&)
	{
		return nullptr; // error occurred, I would crash
	}

}
