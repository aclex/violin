/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_INFO_H
#define VIOLIN_PLUGIN_INFO_H

#include <cstdint>

namespace violin
{
	namespace plugin
	{
		struct info
		{
			std::int32_t numPrograms;
			std::int32_t numParams;
			std::int32_t numInputs;
			std::int32_t numOutputs;
			std::int32_t flags;
			std::int32_t initialDelay;
			std::int32_t uniqueID;
			std::int32_t version;
		};
	}
}

#endif // VIOLIN_PLUGIN_INFO_H
