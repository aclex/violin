/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "wine_starter.h"

#include <unistd.h>

using namespace std;

using namespace violin::plugin;

void wine_starter::start_host_process(const string& host_path, const string& so_path, const string& shm_channel_name_suffix)
{
	execlp("wine64", "wine64", host_path.c_str(), so_path.c_str(), shm_channel_name_suffix.c_str(), nullptr);
}
