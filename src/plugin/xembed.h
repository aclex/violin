/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_XEMBED_H
#define VIOLIN_PLUGIN_XEMBED_H

namespace violin
{
	namespace plugin
	{
		namespace xembed
		{
			enum class opcode : unsigned char
			{
				embedded_notify = 0,
				window_activate = 1,
				window_deactivate = 2,
				request_focus = 3,
				focus_in = 4,
				focus_out = 5,
				focus_next = 6,
				focus_prev = 7,
				/* 8-9 were used for grab_key/xembed_ungrab_key */
				modality_on = 10,
				modality_off = 11,
				register_accelerator = 12,
				unregister_accelerator = 13,
				activate_accelerator = 14
			};

			enum class detail : unsigned char
			{
				none = 0,
				focus_current = 0,
				focus_first = 1,
				focus_last = 2
			};

			constexpr const char atom_name[] = "_XEMBED";

			xcb_client_message_event_t create_event(xcb_atom_t xembed_atom, xcb_window_t window, xembed::opcode opcode, xembed::detail detail = xembed::detail::none, std::int32_t data1 = 0, std::int32_t data2 = 0)
			{
				xcb_client_message_event_t ev;
				ev.response_type = XCB_CLIENT_MESSAGE;
				ev.window = window;
				ev.type = xembed_atom;
				ev.format = 32;
				ev.data.data32[0] = XCB_CURRENT_TIME;
				ev.data.data32[1] = static_cast<std::uint32_t>(opcode);
				ev.data.data32[2] = static_cast<std::uint32_t>(detail);
				ev.data.data32[3] = data1;
				ev.data.data32[4] = data2;

				return ev;
			}
		}
	}
}

#endif // VIOLIN_PLUGIN_XEMBED_H
