/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_PLUGIN_H
#define VIOLIN_PLUGIN_PLUGIN_H

#include <string>
#include <atomic>
#include <stdexcept>
#include <cstdint>
#include <cstring>

#include <unistd.h>
#include <sys/wait.h>

#include "wrapper.h"

#include "message/message.h"

#include "name.h"

#include "transfer/dispatch.h"
#include "transfer/chunk.h"

#include "plugin/transfer/control.h"
#include "plugin/transfer/audio.h"

#include "plugin/editor_forwarding.h"

namespace violin
{
	namespace plugin
	{
		template<class Starter>
		class basic_plugin : public wrapper<basic_plugin<Starter>>,
			public violin::plugin::transfer::control::control<basic_plugin<Starter>>,
			public violin::plugin::transfer::audio<basic_plugin<Starter>>,
			public Starter,
			public editor_forwarding<basic_plugin<Starter>>
		{
		public:
			explicit basic_plugin(audioMasterCallback audio_master_callback, const std::string& host_path, const std::string& wrapper_path = so_path(), const std::string& instance_suffix = generate_shm_name_suffix()) :
				wrapper<basic_plugin<Starter>>(audio_master_callback),
				violin::plugin::transfer::control::control<basic_plugin<Starter>>(plugin_name(wrapper_path), instance_suffix),
				violin::plugin::transfer::audio<basic_plugin<Starter>>(plugin_name(wrapper_path), instance_suffix),
				m_wrapper_path(wrapper_path),
				m_host_pid(start_host(host_path, wrapper_path, instance_suffix)),
				m_remote_dispatcher(this->to_plugin())
			{
				this->start_listening();
				this->update_info(get_plugin_info());
				editor_forwarding<basic_plugin>::initialize();
			}

			std::intptr_t dispatch_to_plugin(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
			{
				switch (opcode)
				{
				case effCanDo:
				case effSetProgramName:
					return m_remote_dispatcher.set<std::string>(opcode, index, value, ptr, opt);

#ifdef USE_VST2SDK
				case effGetMidiKeyName:
					return m_remote_dispatcher.process<MidiKeyName>(opcode, index, value, ptr, opt);

				case effGetParameterProperties:
					return m_remote_dispatcher.get<VstParameterProperties>(opcode, index, value, ptr, opt);

				case effGetOutputProperties:
				case effGetInputProperties:
					return m_remote_dispatcher.get<VstPinProperties>(opcode, index, value, ptr, opt);

				case effSetSpeakerArrangement:
					return m_remote_dispatcher.set<VstSpeakerArrangement>(opcode, index, value, ptr, opt);
				case effBeginLoadBank:
				case effBeginLoadProgram:
					return m_remote_dispatcher.set<VstPatchChunkInfo>(opcode, index, value, ptr, opt);

#else
				case effBeginLoadBank:
				case effBeginLoadProgram:
					return 0;
#endif

				case effGetChunk:
				{
					constexpr const violin::transfer::chunk::request_type t(violin::transfer::chunk::request_type::get);
					m_get_chunk = std::make_unique<violin::plugin::transfer::chunk<t>>(plugin_name(m_wrapper_path));
					auto cke(m_get_chunk->create_cookie());
					const auto result(m_remote_dispatcher.process<violin::transfer::chunk::cookie<t>>(opcode, index, value, static_cast<void*>(&cke), opt));
					m_get_chunk->read(result);

					*reinterpret_cast<void**>(ptr) = m_get_chunk->data();

					return result;
				}

				case effSetChunk:
				{
					constexpr const violin::transfer::chunk::request_type t(violin::transfer::chunk::request_type::set);
					const std::size_t chunk_size(static_cast<std::size_t>(value));
					violin::plugin::transfer::chunk<t> chk(plugin_name(m_wrapper_path), chunk_size);
					chk.write(ptr);
					auto cke(chk.create_cookie());
					return m_remote_dispatcher.set<violin::transfer::chunk::cookie<t>>(opcode, index, value, static_cast<void*>(&cke), opt);
				}

				case effProcessEvents:
					return m_remote_dispatcher.set<VstEvents>(opcode, index, value, ptr, opt);

				case effGetVendorString:
				case effGetProductString:
				case effGetParamName:
				case effGetParamLabel:
				case effGetParamDisplay:
				case effGetProgramName:
				case effGetEffectName:
#ifdef USE_VST2SDK
				case effGetProgramNameIndexed:
				case effShellGetNextPlugin:
#endif
					return m_remote_dispatcher.get<std::string>(opcode, index, value, ptr, opt);

				case effSetBlockSize:
					this->resize_audio_buffer(value);
					return m_remote_dispatcher.process(opcode, index, value, ptr, opt);

				case effEditOpen:
				{
					if (!this->has_editor())
					{
						return false;
					}

					this->save_parent_handle(ptr);
					return m_remote_dispatcher.process(opcode, index, value, ptr, opt);
				}

				case effEditGetRect:
				{
					if (!this->has_editor())
					{
						return false;
					}

					ERect* rect;
					const std::intptr_t result(m_remote_dispatcher.get<ERect>(opcode, index, value, &rect, opt));
					// store just received ERect instance locally, as the host might rely on its existence well after the call, while the returned instance is overwritten on the next request
					this->set_rect(rect);

					*reinterpret_cast<ERect**>(ptr) = this->rect();

					return result;
				}

				case effMainsChanged:
				{
					if (!value)
						this->process_suspend();
					else
						this->process_resume();

					return m_remote_dispatcher.process(opcode, index, value, ptr, opt);
				}

				default:
					return m_remote_dispatcher.process(opcode, index, value, ptr, opt);
				}
			}

			float get_parameter(std::int32_t index)
			{
				return m_remote_dispatcher.get<violin::message::get_parameter>(index);
			}

			void set_parameter(std::int32_t index, float value)
			{
				m_remote_dispatcher.set<violin::message::set_parameter>(index, value);
			}

			~basic_plugin() noexcept
			{
				violin::plugin::transfer::control::control<basic_plugin>::quit();
				waitpid(m_host_pid, nullptr, 0);
			}

		protected:
			void visit(violin::message::message& m) noexcept
			{
				std::visit([this](auto&& arg) { this->process(arg); }, m);
			}

			template<typename PayloadType> bool process(violin::message::dispatch<PayloadType>& m) noexcept
			{
				typename violin::message::dispatch<PayloadType>::unfolder_type u(m);
				u.set_result(this->dispatch_to_master(u.opcode(), u.index(), u.value(), u.ptr(), u.opt()));


				return true;
			}

			bool process(const violin::message::parent_notify& m) noexcept
			{
				this->resize_parent(m.rect);
				this->reparent_and_embed(m.plugin_window_handle);

				return true;
			}

			template<typename MessageType> bool process(const MessageType&) noexcept
			{
				return false;
			}

			using violin::plugin::transfer::audio<basic_plugin<Starter>>::process;

		private:
			static int start_host(const std::string& host_path, const std::string& wrapper_path, const std::string& shm_name_suffix)
			{
				const int host_pid(fork());

				switch (host_pid)
				{
				case -1:
					// still in parent process
					throw std::runtime_error(std::strerror(errno));

				case 0:
					// in child process
					basic_plugin::start_host_process(host_path, wrapper_path, shm_name_suffix);

					// never reach here
					throw std::runtime_error("Can't start host process.");
				}

				return host_pid;
			}

			info get_plugin_info()
			{
				return m_remote_dispatcher.get<violin::message::request_info>();
			}

			void process_suspend()
			{
				m_get_chunk.reset();
			}

			void process_resume()
			{

			}

			const std::string m_wrapper_path;

			const int m_host_pid;

			violin::transfer::dispatch::remote_dispatcher m_remote_dispatcher;

			std::unique_ptr<violin::plugin::transfer::chunk<violin::transfer::chunk::request_type::get>> m_get_chunk;
		};
	}
}

#endif // VIOLIN_PLUGIN_PLUGIN_H
