/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_NATIVE_STARTER_H
#define VIOLIN_PLUGIN_NATIVE_STARTER_H

#include <string>

namespace violin
{
	namespace plugin
	{
		class native_starter
		{
		public:
			static void start_host_process(const std::string& host_path, const std::string& so_path, const std::string& shm_channel_name_suffix);
		};
	}
}

#endif // VIOLIN_PLUGIN_NATIVE_STARTER_H
