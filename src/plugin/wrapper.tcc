/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

template<class Plugin> violin::plugin::wrapper<Plugin>::wrapper(audioMasterCallback audio_master_callback) :
	m_audio_master_callback(audio_master_callback),
	m_effect(std::make_unique<AEffect>())
{
	m_effect->magic = kEffectMagic;
	m_effect->dispatcher = &wrapper::dispatch;
	m_effect->getParameter = &wrapper::get_parameter;
	m_effect->setParameter = &wrapper::set_parameter;
	m_effect->processReplacing = &wrapper::process_replacing;
	m_effect->processDoubleReplacing = &wrapper::process_double_replacing;
}

template<class Plugin> void violin::plugin::wrapper<Plugin>::update_info(const plugin::info& info) noexcept
{
#ifdef USE_VST2SDK
	m_effect->object = static_cast<Plugin*>(this); // would be early to call it in my constructor, so call it here instead
#else
	m_effect->user = static_cast<Plugin*>(this);
#endif

	m_effect->numPrograms = info.numPrograms;
	m_effect->numParams = info.numParams;
	m_effect->numInputs = info.numInputs;
	m_effect->numOutputs = info.numOutputs;
	m_effect->flags = info.flags;
#ifdef USE_VST2SDK
	m_effect->initialDelay = info.initialDelay;
	m_effect->version = info.version;
	m_effect->version = info.version;
#endif
	m_effect->uniqueID = info.uniqueID;
}
