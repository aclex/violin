/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_PLUGIN_EDITOR_FORWARDING_H
#define VIOLIN_PLUGIN_EDITOR_FORWARDING_H

#include <cstdint>

#include <xcb/xcb.h>

#include "plugin/xembed.h"

namespace violin
{
	namespace plugin
	{
		template<class Plugin> class editor_forwarding
		{
		public:
			editor_forwarding() :
				m_conn()
			{

			}

			void initialize()
			{
				if (has_editor())
				{
					m_conn = xcb_connect(nullptr, nullptr);
					m_xembed_cookie = xcb_intern_atom(m_conn, 0, std::strlen(xembed::atom_name), xembed::atom_name);
				}
			}

			ERect* rect() noexcept
			{
				return m_rect.get();
			}

			void set_rect(const ERect* const rect) noexcept
			{
				if (rect)
					m_rect = std::make_unique<ERect>(*rect);
			}

			bool has_editor() const noexcept
			{
				return static_cast<const Plugin*>(this)->effect()->flags & effFlagsHasEditor;
			}

			void save_parent_handle(void* const parent_xid) noexcept
			{
				m_parent_window = reinterpret_cast<unsigned long>(parent_xid);
			}

			void resize_parent(const ERect& rect)
			{
				const xcb_window_t parent_window(m_parent_window);

				const std::array<std::uint32_t, 2> coords
				{{
					static_cast<std::uint32_t>(rect.right - rect.left),
					static_cast<std::uint32_t>(rect.bottom - rect.top)
				}};

				const auto config_window_mask(XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT);

				call_xcb_func(xcb_configure_window_checked, m_conn, parent_window, config_window_mask, coords.data());
			}

			void reparent_and_embed(const unsigned long plugin_window_xid)
			{
				const xcb_window_t parent_window(m_parent_window);
				const xcb_window_t child_window(static_cast<xcb_window_t>(plugin_window_xid));

				call_xcb_func(xcb_reparent_window_checked, m_conn, child_window, parent_window, 0, 0);

				send_xembed_message(child_window, xembed::opcode::embedded_notify, xembed::detail::none, parent_window);
				send_xembed_message(child_window, xembed::opcode::focus_out);

				call_xcb_func(xcb_map_window_checked, m_conn, child_window);

				xcb_flush(m_conn);
			}

			~editor_forwarding() noexcept
			{
				if (m_conn)
				{
					xcb_disconnect(m_conn);
				}
			}

		private:
			template<typename Func, typename... Args> void call_xcb_func(Func f, Args... args)
			{
				const xcb_void_cookie_t cookie(f(std::forward<Args>(args)...));

				const std::unique_ptr<xcb_generic_error_t> error(xcb_request_check(m_conn, cookie));

				if (error)
				{
					throw std::runtime_error("Error occurred performing asynchronous XCB request.");
				}
			}


			void send_xembed_message(xcb_window_t window, xembed::opcode opcode, xembed::detail detail = xembed::detail::none, std::int32_t data1 = 0, std::int32_t data2 = 0)
			{
				const xcb_client_message_event_t& ev(xembed::create_event(xembed_atom(), window, opcode, detail, data1, data2));

				call_xcb_func(xcb_send_event_checked, m_conn, false, window, XCB_EVENT_MASK_NO_EVENT, reinterpret_cast<const char*>(&ev));
			}

			xcb_atom_t xembed_atom()
			{
				if (!m_xembed_atom)
					m_xembed_atom = get_atom(m_conn, m_xembed_cookie);

				return m_xembed_atom;
			}

			static xcb_atom_t get_atom(xcb_connection_t* conn, xcb_intern_atom_cookie_t cookie)
			{
				const std::unique_ptr<xcb_intern_atom_reply_t> reply(xcb_intern_atom_reply(conn, cookie, nullptr));

				if (reply)
				{
					xcb_atom_t result(reply->atom);
					return result;
				}
				else
				{
					throw std::runtime_error("Can't receive XCB atom reply.");
				}
			}

			xcb_connection_t* m_conn;

			xcb_intern_atom_cookie_t m_xembed_cookie;
			xcb_atom_t m_xembed_atom;

			xcb_window_t m_parent_window;

			std::unique_ptr<ERect> m_rect;
		};
	}
}

#endif // VIOLIN_PLUGIN_EDITOR_FORWARDING_H
