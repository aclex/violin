/*
  Violin
  Copyright (C) 2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_THREADING_TRAITS_WINAPI_H
#define VIOLIN_THREADING_TRAITS_WINAPI_H

#include "wine/thread.h"
#include "wine/critical_section.h"
#include "wine/condition_variable.h"

namespace violin
{
	namespace threading_traits
	{
		struct winapi
		{
			typedef wine::thread thread_type;
			typedef wine::critical_section lockable_type;
			typedef wine::condition_variable cv_type;
		};
	};
}

#endif // VIOLIN_THREADING_TRAITS_WINAPI_H
