/*
  Violin
  Copyright (C) 2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_THREADING_TRAITS_STD_H
#define VIOLIN_THREADING_TRAITS_STD_H

#include <thread>
#include <mutex>
#include <condition_variable>

namespace violin
{
	namespace threading_traits
	{
		struct std
		{
			typedef ::std::thread thread_type;
			typedef ::std::mutex lockable_type;
			typedef ::std::condition_variable cv_type;
		};
	}
}

#endif // VIOLIN_THREADING_TRAITS_STD_H
