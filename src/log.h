/*
  Violin
  Copyright (C) 2018-2020  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_LOG_H
#define VIOLIN_LOG_H

#include <string>

#include "name.h"

namespace violin
{
	namespace log
	{
		void initialize(int argc = 0, char* argv[] = nullptr);
		bool initialized() noexcept;
		void print(const std::string& s);
	}
}

#endif // VIOLIN_LOG_H
