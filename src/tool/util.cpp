/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tool/util.h"

#include <optional>
#include <algorithm>
#include <cstring>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>

#include <libintl.h>
#include <locale.h>

#include "config.h"
#include "name.h"

using namespace std;

using namespace violin;

namespace
{
	const string g_lib_dir { LIBDIR };
	const string g_wrapper_path { violin::join(vector<string> { g_lib_dir, "violin", "libplugin.so" }, '/') };

	optional<pair<string, string>> remove_if_exists(violin::config& cfg, const string& ref)
	{
		if (const auto& plugin_path = cfg.plugin_path(ref))
		{
			cfg.remove_mapping(ref);
			return optional<pair<string, string>>({ ref, plugin_path.value() });
		}

		return optional<pair<string, string>> { };
	}

}

vector<string> tool::parse_args(const int argc, const char* const argv[])
{
	vector<string> args;
	args.reserve(argc);
	copy(argv, argv + argc, back_inserter(args));

	return args;
}

string tool::current_directory()
{
	char cwd[PATH_MAX];

	if (getcwd(cwd, sizeof(cwd)))
	{
		return cwd;
	}
	else
	{
		const string msg { gettext("Cannot get path to current directory.") };
		throw runtime_error(msg);
	}
}

void tool::copy_file(const string& src_path, const string& dest_path)
{
	ifstream source(src_path, ios::binary);
	if (source.fail())
	{
		const string msg { string(gettext("Cannot find wrapper prototype library at")) + ' ' + src_path  + ". " + gettext("Check your Violin installation.") };
		throw runtime_error(msg);
	}
	source.exceptions(ios_base::badbit | ios_base::failbit);

	ofstream dest(dest_path, ios::binary);
	dest.exceptions(ios_base::badbit | ios_base::failbit);

	dest << source.rdbuf();
}

void tool::delete_file(const string& path)
{
	if (unlink(path.c_str()))
	{
		const string msg { string(gettext("Cannot delete")) + ' ' + path + ": " + strerror(errno) + '.' };
		throw runtime_error(msg);
	}
}

string tool::link_scheme_str(const string& target, const string& dest)
{
	return dest + " -> " + target;
}

bool tool::contains(const vector<string>& args, const string& key)
{
	return find(args.cbegin(), args.cend(), key) != args.cend();
}

bool tool::is_directory(const string& path)
{
	struct stat s;
	if (!stat(path.c_str(), &s))
	{
		if (s.st_mode & S_IFDIR)
		{
			return true;
		}
	}

	return false;
}

bool tool::exists(const string& path)
{
	return ifstream(path).good();
}

void tool::create_link(const string& target, const string& dest)
{
	if (exists(dest))
	{
		const string& msg { link_scheme_str(target, dest) + ": " + gettext("cannot create wrapper") + ". " + gettext("File exists.") };
		throw invalid_argument(msg);
	}

	violin::config cfg;

	if (cfg.plugin_path(dest))
	{
		const string& msg { link_scheme_str(target, dest) + ": " + gettext("cannot link") + ": " + gettext("link already exists") + '.' };
		throw invalid_argument(msg);
	}

	copy_file(g_wrapper_path, dest);

	cfg.add_mapping(dest, target);

	try
	{
		cfg.save();
	}
	catch (const ios_base::failure& e)
	{
		delete_file(dest);
		const string& msg { link_scheme_str(target, dest) + ": " + gettext("cannot link") + ": " + gettext("can't save") + " mapping.json: " + strerror(errno) + '.' };
		throw runtime_error(msg);
	}
}

void tool::update_link(const string& ref)
{
	try
	{
		copy_file(g_wrapper_path, ref);
	}
	catch (const ios_base::failure& e)
	{
		const string msg { string(gettext("Cannot update link at")) + ' ' + ref  + ". " + gettext("Check your permissions for the target.") };
		throw runtime_error(msg);
	}
}

void tool::remove_link(const string& ref)
{
	violin::config cfg;

	optional<pair<string, string>> record;

	record = remove_if_exists(cfg, ref); // try to remove as is
	if (!record)
	{
		string entity_path;
		try
		{
			entity_path = absolute_path(ref); // try to get absolute path of the wrapper (succeeds if it still exists)
		}
		catch (const exception&)
		{
			const string& dir_path = absolute_path(violin::dirname(ref));
			entity_path = violin::join({ dir_path, violin::basename(ref) }, '/');
		}

		record = remove_if_exists(cfg, entity_path);
		if (!record)
		{

			entity_path = violin::join({ current_directory(), ref }, '/'); // try to treat it as located in subdirectory of the current directory

			record = remove_if_exists(cfg, entity_path);

			if (!record)
			{
				entity_path = absolute_path(ref); // finally, try treating it as VST plugin path
				const auto& wrapper_path = cfg.wrapper_path(entity_path);

				if (wrapper_path)
				{
					record = remove_if_exists(cfg, wrapper_path.value());
				}

				if (!record)
				{
					const string msg { ref + ": " + gettext("cannot unlink") + ": " + gettext("no link with such wrapper or plugin relative or absolute path found. If you're absolutely sure, that the link still exists and know the wrapper location, try removing it manually and erase the corresponding link record in") + ' ' + violin::config::path() + ", " + gettext("if exists") + '.' };

					throw invalid_argument(msg);
				}
			}
		}
	}

	try
	{
		cfg.save();
	}
	catch (const ios_base::failure& e)
	{
		const string& msg { ref + ": " + gettext("cannot unlink") + ": " + gettext("can't save") + " mapping.json: " + strerror(errno) + '.' };
		throw runtime_error(msg);
	}

	const pair<string, string>& link = record.value();

	try
	{
		delete_file(link.first);
	}
	catch (const exception& e)
	{
		if (errno != EFAULT &&
			errno != EISDIR &&
			errno != ELOOP &&
			errno != ENOENT &&
			errno != ENOTDIR)
		{
			cfg.add_mapping(link.first, link.second);

			const string& msg { ref + ": " + gettext("cannot unlink") + ": " + gettext("cannot delete wrapper") + "): " + strerror(errno) + '.' };
			throw runtime_error(msg);
		}
	}
}

string tool::create_link_name(const string& directory, const string& target, const bool add_suffix)
{
	return violin::join({ directory, violin::basename(target) }, '/') + (add_suffix ? ".so" : "");
}

string tool::absolute_path(const string& relative_path)
{
	char buf[PATH_MAX];
	if (realpath(relative_path.c_str(), buf))
	{
		return buf;
	}
	else
	{
		const string msg { string(gettext("Cannot get absolute path for")) + ' ' + relative_path + ": " + strerror(errno) };
		throw runtime_error(msg);
	}
}

string tool::dir_name_from_link_name(const string& link_name)
{
	const auto& dirname = violin::dirname(link_name);

	return dirname.empty() ? current_directory() : dirname;
}
