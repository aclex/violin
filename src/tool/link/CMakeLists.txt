set (VIOLIN_TOOL_LINK_SOURCES
	main.cpp
	)

add_executable(violin-link ${VIOLIN_TOOL_LINK_SOURCES})
target_link_libraries(violin-link violin tool ${Intl_LIBRARIES})
install(TARGETS violin-link RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
