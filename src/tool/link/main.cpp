/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string>
#include <algorithm>
#include <ios>
#include <iostream>
#include <fstream>

#include "name.h"
#include "config.h"

#include "tool/util.h"

using namespace std;

using namespace violin::tool;

namespace
{
	constexpr const char* g_translation_domain { "violin" };
	constexpr const char* g_locale_destination { LOCALE_DESTINATION };

	const string g_lib_dir { LIBDIR };
	const string g_wrapper_path { violin::join(vector<string> { g_lib_dir, "violin", "libplugin.so" }, '/') };

	void print_usage()
	{
		cout << gettext("Usage") << ":" << endl;
		cout << "  " << "violin-link" << ' ' << "[" << "--help | -h" << "] " << '<' << gettext("target") << '>' << "..." << ' ' << "[<" << gettext("destination") << ">]" << endl;
		cout << endl;
	}

	void print_help()
	{
		cout << gettext("Violin link creation utility") << endl;
		print_usage();

		cout << gettext("Creates a wrapper at path depending on <destination> argument (see below)\n"
			"and registers a link to the specified VST plugin DLL (compiled for\n"
			"MS Windows, 64-bit).\n\n"
			"<destination> argument, if specified, can be a wrapper name or destination\n"
			"directory path. In the latter case, wrapper name is created as <target>\n"
			"filename plus \".so\" extension. If <destination> argument is omitted,\n"
			"wrappers are created in the current directory.\n\n"
			"In case of more, than two arguments specified, the latter is treated as\n"
			"<destination> and all the former ones are treated as <target>'s.\n"
			"No link or wrapper is created, if the corresponding file or link record in\n"
			"mapping.json exists.\n") << endl;

		cout << gettext("Options") << ':' << endl;
		cout << "  " << "-h" << ", " << "--help" << "    " << gettext("Print this help message") << endl;
		cout << endl;
	}

	int create_links(const vector<string>& targets, const string& destination)
	{
		if (targets.size() == 1)
		{
			try
			{
				const string& source = absolute_path(targets.at(0));
				const string& directory = absolute_path(is_directory(destination) ? destination : dir_name_from_link_name(destination));
				const string& link_nameref { is_directory(destination) ? source : destination };
				const bool add_suffix(is_directory(destination));
				const string& link_name { create_link_name(directory, link_nameref, add_suffix) };

				create_link(source, link_name);
				return 0;
			}
			catch (const invalid_argument& e)
			{
				cerr << e.what() << endl;
				return -1;
			}
			catch (const runtime_error& e)
			{
				cerr << e.what() << endl;
				return -2;
			}
		}
		else
		{
			try
			{
				const string& directory = absolute_path(destination);

				if (!is_directory(directory))
				{
					cerr << gettext("Cannot link") << ": " << gettext("destination is not a directory") << '.' << endl;
					return -1;
				}

				int return_code { };
				for (const string& target : targets)
				{
					const string& link_name = create_link_name(directory, target);
					try
					{
						const string& source = absolute_path(target);
						create_link(source, link_name);
					}
					catch (const invalid_argument& e)
					{
						cerr << e.what() << endl;
						return_code = -3;
					}
					catch (const runtime_error& e)
					{
						cerr << e.what() << endl;
						return_code = -3;
					}
					catch (const exception& e)
					{
						cerr << link_scheme_str(target, link_name) << ": " << e.what() << endl;
						return_code = -4;
					}
				}

				return return_code;
			}
			catch (const runtime_error& e)
			{
				cerr << gettext("Cannot link") << ": " << gettext("cannot get absolute path to destination") << ": " << strerror(errno) << '.' << endl;
				return -1;
			}
		}
	}
}

int main(const int argc, const char* const argv[])
{
	setlocale(LC_ALL, "");
	bindtextdomain(g_translation_domain, g_locale_destination);
	textdomain(g_translation_domain);

	if (argc < 2)
	{
		cout << gettext("Necessary <target> argument is omitted.") << endl;
		cout << endl;
		print_usage();
		return 1;
	}

	const auto& args { parse_args(argc, argv) };

	if (args.size() == 2 && (contains(args, "-h") || contains(args, "--help")))
	{
		print_help();
		return 0;
	}

	if (argc == 2)
		return create_links({ args[1] }, current_directory());

	vector<string> targets(args.cbegin() + 1, args.cend() - 1);
	return create_links(targets, args.back());
}
