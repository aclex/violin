/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TOOL_UTIL_H
#define VIOLIN_TOOL_UTIL_H

#include <vector>
#include <string>
#include <fstream>

namespace violin
{
	namespace tool
	{
		std::vector<std::string> parse_args(const int argc, const char* const argv[]);
		std::string current_directory();
		void copy_file(const std::string& src_path, const std::string& dest_path);
		void delete_file(const std::string& path);
		std::string link_scheme_str(const std::string& target, const std::string& dest);
		bool contains(const std::vector<std::string>& args, const std::string& key);
		bool is_directory(const std::string& path);
		bool exists(const std::string& path);
		void create_link(const std::string& target, const std::string& dest);
		void update_link(const std::string& ref);
		void remove_link(const std::string& ref);
		std::string create_link_name(const std::string& directory, const std::string& target, const bool add_suffix = true);
		std::string absolute_path(const std::string& relative_path);
		std::string dir_name_from_link_name(const std::string& link_name);
	}
}

#endif // VIOLIN_TOOL_UTIL_H
