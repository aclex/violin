/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string>
#include <algorithm>
#include <ios>
#include <iostream>
#include <fstream>
#include <optional>
#include <utility>
#include <cstdio>
#include <cstdlib>

#include "name.h"
#include "config.h"

#include "tool/util.h"

using namespace std;

using namespace violin::tool;

namespace
{
	constexpr const char* g_translation_domain { "violin" };
	constexpr const char* g_locale_destination { LOCALE_DESTINATION };

	void print_usage()
	{
		cout << gettext("Usage") << ":" << endl;
		cout << "  " << "violin-unlink" << ' ' << "[" << "--help | -h" << "] " << '<' << gettext("wrapper or plugin") << '>' << "..." << endl;
		cout << endl;
	}

	void print_help()
	{
		cout << gettext("Violin unlinking utility") << endl;
		print_usage();

		cout << gettext("Removes a wrapper and unregisters existing link to some VST plugin. Each\n"
			"of the arguments can either be a path to wrapper or to the corresponding\n"
			"VST plugin DLL.\n\n"

			"Does nothing, if no registered link for the argument found.\n") << endl;

		cout << gettext("Options") << ':' << endl;
		cout << "  " << "-h" << ", " << "--help" << "    " << gettext("Print this help message") << endl;
		cout << endl;
	}

	int remove_links(const vector<string>& keys)
	{
		int return_code { };
		for (const string& key : keys)
		{
			try
			{
				remove_link(key);
			}
			catch (const invalid_argument& e)
			{
				cerr << e.what() << endl;
				return_code = -3;
			}
			catch (const runtime_error& e)
			{
				cerr << e.what() << endl;
				return_code = -3;
			}
			catch (const exception& e)
			{
				cerr << key << ": " << e.what() << endl;
				return_code = -4;
			}
		}

		return return_code;
	}
}

int main(const int argc, const char* const argv[])
{
	setlocale(LC_ALL, "");
	bindtextdomain(g_translation_domain, g_locale_destination);
	textdomain(g_translation_domain);

	if (argc < 2)
	{
		cout << gettext("At least one argument is expected.") << endl;
		cout << endl;
		print_usage();
		return 1;
	}

	const auto& args { parse_args(argc, argv) };

	if (args.size() == 2 && (contains(args, "-h") || contains(args, "--help")))
	{
		print_help();
		return 0;
	}

	vector<string> keys(args.cbegin() + 1, args.cend());
	return remove_links(keys);
}
