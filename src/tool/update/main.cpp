/*
  Violin
  Copyright (C) 2018-2019 Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <vector>
#include <string>
#include <algorithm>
#include <ios>
#include <iostream>
#include <fstream>

#include "name.h"
#include "config.h"

#include "tool/util.h"

using namespace std;

using namespace violin::tool;

namespace
{
	constexpr const char* g_translation_domain { "violin" };
	constexpr const char* g_locale_destination { LOCALE_DESTINATION };

	void print_usage()
	{
		cout << gettext("Usage") << ":" << endl;
		cout << "  " << "violin-update" << ' ' << "[" << "--help | -h" << "]" << endl;
		cout << endl;
	}

	void print_help()
	{
		cout << gettext("Violin link updating utility") << endl;
		print_usage();

		cout << gettext("Updates all the registered links to the current version.\n") << endl;

		cout << gettext("Options") << ':' << endl;
		cout << "  " << "-h" << ", " << "--help" << "    " << gettext("Print this help message") << endl;
		cout << endl;
	}

	void update_links()
	{
		violin::config cfg;

		const auto& link_list { cfg.list_mappings() };

		for (const auto& [wrapper_path, _] : link_list)
		{
			try
			{
				update_link(wrapper_path);
			}
			catch (const runtime_error& e)
			{
				cerr << e.what() << endl;
			}
		}
	}
}

int main(const int argc, const char* const argv[])
{
	setlocale(LC_ALL, "");
	bindtextdomain(g_translation_domain, g_locale_destination);
	textdomain(g_translation_domain);

	const auto& args { parse_args(argc, argv) };

	if (args.size() == 2 && (contains(args, "-h") || contains(args, "--help")))
	{
		print_help();
		return 0;
	}
	else if (args.size() == 2)
	{
		print_usage();
		return 1;
	}

	if (args.size() > 2)
	{
		print_usage();
		return 1;
	}

	update_links();

	return 0;
}
