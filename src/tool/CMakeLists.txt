add_definitions(-DLOCALE_DESTINATION="${CMAKE_INSTALL_PREFIX}/${LOCALE_DESTINATION}")

set(TOOL_SOURCES
	util.cpp
	)

add_library(tool OBJECT ${TOOL_SOURCES})
target_link_libraries(tool violin)

add_subdirectory(link)
add_subdirectory(unlink)
add_subdirectory(list)
add_subdirectory(update)
