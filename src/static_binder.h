/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_STATIC_BINDER_H
#define VIOLIN_STATIC_BINDER_H

#include <unordered_map>

namespace violin
{
	template<typename KeyType, typename ValueType> class static_binder
	{
	public:
		static void bind(const KeyType k, const ValueType v) noexcept
		{
			s_binding.emplace(k, v);
		}

		static void unbind(const KeyType k) noexcept
		{
			s_binding.erase(k);
		}

		static ValueType find(const KeyType k) noexcept
		{
			const auto cit(s_binding.find(k));

			if (cit != s_binding.cend())
			{
				return cit->second;
			}
			else
			{
				return ValueType { };
			}
		}

	private:
		static std::unordered_map<KeyType, ValueType> s_binding;
	};

	template<typename KeyType, typename ValueType> std::unordered_map<KeyType, ValueType> static_binder<KeyType, ValueType>::s_binding;
}

#endif // VIOLIN_STATIC_BINDER_H
