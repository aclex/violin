/*
  Violin
  Copyright (C) 2018-2020  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "config.h"

#include <iostream>
#include <fstream>
#include <iterator>

#include "name.h"
#include "log.h"

using namespace std;

using namespace peli::json;

using namespace violin;

namespace
{
	constexpr const char* g_sys_conf_dir { SYSCONFDIR };

	const string g_config_file_path(join(vector<string> { g_sys_conf_dir, "violin", "mapping.json" }, '/'));

	constexpr const char* g_wrapper_path_key { "wrapper_path" };
	constexpr const char* g_plugin_path_key { "plugin_path" };
}

config::config(const string& config_file_path) :
	m_config_file_path(config_file_path),
	m_modified()
{
	ifstream file(config_file_path);

	if (file.is_open())
	{
		try
		{
			file >> m_config_file;
			return;
		}
		catch (const peli::parse_error&)
		{
			const auto& msg { "can't parse mapping file at "s + config_file_path + ", opening the blank mapping."s };

			if (violin::log::initialized())
				violin::log::print(msg);
			else
				cout << msg << endl;
		}
	}

	m_config_file = peli::json::object { };
}

bool config::empty() const
{
	try
	{
		return get<object>(m_config_file).empty();
	}
	catch (exception&)
	{
		return true;
	}
}

optional<string> config::plugin_path(const string& wrapper_path) const noexcept
{
	try
	{
		const object& records(get<object>(m_config_file));

		for (const auto& [key, value] : records)
		{
			if (wrapper_path == key)
			{
				const object& record(get<object>(value));
				return optional<string> { get<string>(record.at(g_plugin_path_key)) };
			}
		}
	}
	catch (const exception&)
	{

	}

	return optional<string> { };
}

optional<string> config::wrapper_path(const string& plugin_path) const noexcept
{
	try
	{
		const object& records(get<object>(m_config_file));

		for (const auto& [key, value] : records)
		{
			const object& record(get<object>(value));

			if (get<string>(record.at(g_plugin_path_key)) == plugin_path)
			{
				return optional<string> { key };
			}
		}
	}
	catch (const exception&)
	{

	}

	return optional<string> { };
}

void config::add_mapping(const string& wrapper_path, const string& target_plugin_path) noexcept
{
	try
	{
		if (m_config_file.null()) // mapping file doesn't exist
			m_config_file = make_value<object>();

		object& records(get<object>(m_config_file));

		const auto& plugin_record(plugin_path(wrapper_path));
		if (!plugin_record || plugin_record.value() != target_plugin_path)
		{
			const object plugin_obj { { g_plugin_path_key, { target_plugin_path } } };
			records[wrapper_path] = plugin_obj;
			m_modified = true;
		}
	}
	catch (const exception&)
	{

	}
}

void config::remove_mapping(const string& wrapper_path) noexcept
{
	try
	{
		object& records(get<object>(m_config_file));

		const auto removed_count { records.erase(wrapper_path) };

		if (removed_count)
			m_modified = true;
	}
	catch (const exception&)
	{

	}
}

unordered_map<string, string> config::list_mappings() noexcept
{
	unordered_map<string, string> result;
	try
	{
		object& records(get<object>(m_config_file));
		transform(begin(records), end(records), inserter(result, begin(result)),
			[](const auto& p)
			{
				const auto& [wrapper_path, v] = p;
				return make_pair(wrapper_path, get<string>(get<object>(v).at(g_plugin_path_key)));
			});
	}
	catch (const exception&)
	{

	}

	return result;
}

string config::path()
{
	return g_config_file_path;
}

void config::save()
{
	if (!m_modified)
		return;

	ofstream file(m_config_file_path);
	file.exceptions(ios_base::badbit | ios_base::failbit);

	file << peli::json::pretty << m_config_file;
	file << endl;

	m_modified = false;
}

config::~config()
{
	try
	{
		save();
	}
	catch (const exception& e)
	{
		violin::log::print("can't save mapping to " + m_config_file_path + ": " + e.what());
	}
}
