/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_UNFOLD_VST_EVENTS_H
#define VIOLIN_MESSAGE_UNFOLD_VST_EVENTS_H

#include "message/unfold/unfolder.h"

namespace violin
{
	namespace message
	{
		template<> class unfolder<VstEvents> : public common_unfolder<VstEvents>
		{
		public:
			explicit unfolder(violin::message::dispatch<VstEvents>& m) :
				common_unfolder<VstEvents>(m)
			{
				VstEvents* events(new (&m_events_storage) VstEvents);

				const std::size_t event_count(message()->m_event_count);
				events->numEvents = event_count;
				events->reserved = 0;

				for (std::size_t i = 0; i < event_count; ++i)
					events->events[i] = &(message()->m_events[i]);
			}

			void* ptr() noexcept
			{
				return &m_events_storage;
			}

		private:
			static constexpr const std::size_t s_maximum_memory_size = sizeof(VstEvents) + (maximum_event_count - 2) * sizeof(VstEvent*);

			std::aligned_storage_t<s_maximum_memory_size, alignof(VstEvents)> m_events_storage;
		};
	}
}

#endif // VIOLIN_MESSAGE_UNFOLD_VST_EVENTS_H
