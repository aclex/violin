/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_UNFOLD_UNFOLDER_H
#define VIOLIN_MESSAGE_UNFOLD_UNFOLDER_H

#include "message/dispatch.h"

namespace violin
{
	namespace message
	{
		template<typename PayloadType> class common_unfolder
		{
		public:
			explicit common_unfolder(violin::message::dispatch<PayloadType>& m) :
				m_message(&m)
			{

			}

			std::int32_t opcode() const noexcept
			{
				return m_message->opcode;
			}

			std::int32_t index() const noexcept
			{
				return m_message->index;
			}

			std::intptr_t value() noexcept
			{
				return m_message->value;
			}

			void* ptr() noexcept
			{
				return m_message->payload_ptr();
			}

			float opt() const noexcept
			{
				return m_message->opt;
			}

			std::intptr_t result() const noexcept
			{
				return m_message->result;
			}

			void set_result(std::intptr_t r) noexcept
			{
				m_message->result = r;
			}

			~common_unfolder() noexcept
			{

			}

		protected:
			violin::message::dispatch<PayloadType>* message() const noexcept
			{
				return m_message;
			}

		private:
			violin::message::dispatch<PayloadType>* const m_message;
		};

		template<> void* common_unfolder<void>::ptr() noexcept
		{
			return m_message->ptr;
		}

		template<typename PayloadType> class unfolder : public common_unfolder<PayloadType>
		{
		public:
			using common_unfolder<PayloadType>::common_unfolder;
		};
	}
}

#endif // VIOLIN_MESSAGE_UNFOLD_UNFOLDER_H
