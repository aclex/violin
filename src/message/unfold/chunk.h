/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_UNFOLD_CHUNK_H
#define VIOLIN_MESSAGE_UNFOLD_CHUNK_H

#include "shm/channel.h"

#include "transfer/chunk/chunk.h"
#include "message/payload/chunk.h"

namespace violin
{
	namespace message
	{
		template<> class unfolder<violin::transfer::chunk::cookie<violin::transfer::chunk::request_type::get>> : public common_unfolder<violin::transfer::chunk::cookie<violin::transfer::chunk::request_type::get>>
		{
		public:
			explicit unfolder(violin::message::dispatch<violin::transfer::chunk::cookie<violin::transfer::chunk::request_type::get>>& m) :
				common_unfolder(m),
				m_channel(m.payload_ptr()->name, 0),
				m_pointer(),
				m_size()
			{

			}

			std::intptr_t value() noexcept
			{
				return message()->value;
			}

			void* ptr() noexcept
			{
				return &m_pointer;
			}

			std::intptr_t result() const noexcept
			{
				return m_size;
			}

			void set_result(const std::intptr_t r) noexcept
			{
				m_size = static_cast<std::size_t>(r);
			}

			~unfolder() noexcept
			{
				m_channel.resize(m_size);
				m_channel.write(m_pointer);
				message()->result = m_size;
			}

		private:
			shm::channel<shm::creation::disabled, shm::resizing::full> m_channel;
			void* m_pointer;
			std::size_t m_size;
		};

		template<> class unfolder<violin::transfer::chunk::cookie<violin::transfer::chunk::request_type::set>> : public common_unfolder<violin::transfer::chunk::cookie<violin::transfer::chunk::request_type::set>>
		{
		public:
			explicit unfolder(violin::message::dispatch<violin::transfer::chunk::cookie<violin::transfer::chunk::request_type::set>>& m) :
				common_unfolder(m),
				m_channel(m.payload_ptr()->name, static_cast<std::size_t>(m.value))
			{

			}

			void* ptr() noexcept
			{
				return m_channel.frame_pointer();
			}

		private:
			shm::channel<shm::creation::disabled, shm::resizing::disabled> m_channel;
		};
	}
}

#endif // VIOLIN_MESSAGE_UNFOLD_CHUNK_H
