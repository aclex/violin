/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_UNFOLD_VST_TIME_INFO_H
#define VIOLIN_MESSAGE_UNFOLD_VST_TIME_INFO_H

#include "message/unfold/unfolder.h"

namespace violin
{
	namespace message
	{
		template<> common_unfolder<VstTimeInfo>::~common_unfolder() noexcept
		{
			if (m_message->result)
			{
				*(m_message->payload_ptr()) = *reinterpret_cast<VstTimeInfo*>(m_message->result);
				m_message->result = true;
			}
			else
			{
				m_message->result = false;
			}
		}
	}
}

#endif // VIOLIN_MESSAGE_UNFOLD_VST_TIME_INFO_H
