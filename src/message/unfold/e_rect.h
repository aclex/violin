/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_UNFOLD_E_RECT_H
#define VIOLIN_MESSAGE_UNFOLD_E_RECT_H

namespace violin
{
	namespace message
	{
		template<> class unfolder<ERect> : public common_unfolder<ERect>
		{
		public:
			explicit unfolder(violin::message::dispatch<ERect>& m) :
				common_unfolder<ERect>(m),
				m_rect_ptr(message()->payload_ptr())
			{

			}

			void* ptr() noexcept
			{
				return &m_rect_ptr;
			}

			~unfolder() noexcept
			{
				if (m_rect_ptr)
					*(message()->payload_ptr()) = *m_rect_ptr;
			}

		private:
			ERect* m_rect_ptr;
		};
	}
}

#endif // VIOLIN_MESSAGE_UNFOLD_E_RECT_H
