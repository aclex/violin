/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_DISPATCH_H
#define VIOLIN_MESSAGE_DISPATCH_H

#include "message/payload/payload.h"
#include "message/payload/string.h"
#include "message/payload/vst_events.h"

#ifdef USE_VST2SDK
#include "message/payload/vst_speaker_arrangement.h"
#endif

#include "message/payload/e_rect.h"

namespace violin
{
	namespace message
	{
		template<typename> class unfolder;

		struct common_dispatch
		{
			typedef std::intptr_t return_type;

			std::int32_t opcode;
			std::int32_t index;
			std::intptr_t value;
			void* ptr;
			float opt;
			std::intptr_t result;

			common_dispatch() = default;

			constexpr common_dispatch(std::int32_t oc, std::int32_t idx, std::intptr_t v, void* p, float o) noexcept :
				opcode(oc),
				index(idx),
				value(v),
				ptr(p),
				opt(o),
				result()
			{

			}

			constexpr return_type extract_result() const noexcept
			{
				return result;
			}
		};

		template<typename PayloadType = void> struct dispatch : common_dispatch, payload<PayloadType>
		{
			typedef unfolder<PayloadType> unfolder_type;

			dispatch() = default;

			dispatch(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt) noexcept :
				common_dispatch(opcode, index, value, nullptr, opt),
				payload<PayloadType>(opcode, index, value, ptr, opt)
			{

			}
		};

		template<> struct dispatch<void> : common_dispatch
		{
			typedef unfolder<void> unfolder_type;

			using common_dispatch::common_dispatch;
		};
	}
}

#include "message/unfold/unfolder.h"
#include "message/unfold/vst_events.h"

#ifdef USE_VST2SDK
#include "message/unfold/vst_speaker_arrangement.h"
#endif

#include "message/unfold/vst_time_info.h"
#include "message/unfold/e_rect.h"
#include "message/unfold/chunk.h"

#endif // VIOLIN_MESSAGE_DISPATCH_H
