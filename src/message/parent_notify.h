/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_PARENT_NOTIFY_H
#define VIOLIN_MESSAGE_PARENT_NOTIFY_H

#include "vst_header_chooser.h"

namespace violin
{
	namespace message
	{
		struct parent_notify
		{
			typedef void return_type;

			ERect rect;
			unsigned long plugin_window_handle;

			constexpr explicit parent_notify(const ERect& r, unsigned long h) noexcept :
				rect(r),
				plugin_window_handle(h)
			{

			}

			constexpr void extract_result() const noexcept { }
		};
	}
}

#endif // VIOLIN_MESSAGE_PARENT_NOTIFY_H
