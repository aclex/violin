/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_MESSAGE_H
#define VIOLIN_MESSAGE_MESSAGE_H

#include <variant>
#include <string>

#include "vst_header_chooser.h"

#include "message/empty.h"
#include "message/dispatch.h"
#include "message/request_info.h"
#include "message/get_parameter.h"
#include "message/set_parameter.h"
#include "message/parent_notify.h"

#include "transfer/chunk/chunk.h"

namespace violin
{
	namespace message
	{
		using message = std::variant
		<
			message::empty,
			message::dispatch<>,
			message::dispatch<std::string>,
#ifdef USE_VST2SDK
			message::dispatch<VstParameterProperties>,
			message::dispatch<VstPinProperties>,
			message::dispatch<MidiKeyName>,
			message::dispatch<VstPatchChunkInfo>,
			message::dispatch<VstSpeakerArrangement>,
#endif
			message::dispatch<VstTimeInfo>,
			message::dispatch<VstEvents>,
			message::dispatch<ERect>,
			message::dispatch<transfer::chunk::cookie<transfer::chunk::request_type::get>>,
			message::dispatch<transfer::chunk::cookie<transfer::chunk::request_type::set>>,
			message::request_info,
			message::get_parameter,
			message::set_parameter,
			message::parent_notify
		>;
	}
}

#endif // VIOLIN_MESSAGE_MESSAGE_H
