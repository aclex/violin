/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_PAYLOAD_E_RECT_H
#define VIOLIN_MESSAGE_PAYLOAD_E_RECT_H

#include "vst_header_chooser.h"

namespace violin
{
	namespace message
	{
		template<> void payload<ERect>::copy(void* const ptr) const noexcept
		{
			if (!ptr)
				return;

			if (m_null)
				return;

			const ERect** const rptr(static_cast<const ERect** const>(ptr));

			*rptr = &m_payload;
		}
	}
}

#endif // VIOLIN_MESSAGE_PAYLOAD_E_RECT_H
