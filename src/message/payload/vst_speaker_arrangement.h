/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_PAYLOAD_VST_SPEAKER_ARRANGEMENT_H
#define VIOLIN_MESSAGE_PAYLOAD_VST_SPEAKER_ARRANGEMENT_H

#include "vst_header_chooser.h"

namespace violin
{
	namespace message
	{
		template<> struct payload<VstSpeakerArrangement>
		{
			payload(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt) noexcept :
				m_input(*reinterpret_cast<VstSpeakerArrangement*>(value)),
				m_output(*static_cast<VstSpeakerArrangement*>(ptr))
			{

			}

			void* input() noexcept
			{
				return &m_input;
			}

			void* output() noexcept
			{
				return &m_output;
			}

			void copy(const std::intptr_t value, void* const ptr) const noexcept
			{
				if (!ptr || !value)
					return;

				VstSpeakerArrangement& other_input(*reinterpret_cast<VstSpeakerArrangement* const>(value));
				VstSpeakerArrangement& other_output(*static_cast<VstSpeakerArrangement* const>(ptr));

				other_input = m_input;
				other_output = m_output;
			}

		private:
			VstSpeakerArrangement m_input, m_output;
		};
	}
}

#endif // VIOLIN_MESSAGE_PAYLOAD_VST_SPEAKER_ARRANGEMENT_H
