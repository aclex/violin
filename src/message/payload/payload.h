/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_PAYLOAD_H
#define VIOLIN_MESSAGE_PAYLOAD_H

#include <cstddef>

namespace violin
{
	namespace message
	{
		template<typename PayloadType> struct payload
		{
			payload(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt) noexcept :
				m_payload(ptr ? *static_cast<PayloadType*>(ptr) : PayloadType { }),
				m_null(!ptr)
			{

			}

			PayloadType* payload_ptr() noexcept
			{
				return &m_payload;
			}

			void copy(void* const ptr) const noexcept
			{
				if (!ptr)
					return;

				if (m_null)
					return;

				PayloadType& other(*static_cast<PayloadType* const>(ptr));

				other = m_payload;
			}

			void copy(std::intptr_t ptr) const noexcept
			{
				copy(reinterpret_cast<void*>(ptr));
			}

		protected:
			void set_null() noexcept
			{
				m_null = true;
			}

		private:
			PayloadType m_payload;
			bool m_null;
		};
	}
}

#endif // VIOLIN_MESSAGE_PAYLOAD_H
