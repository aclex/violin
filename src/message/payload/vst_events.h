/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_PAYLOAD_VST_EVENTS_H
#define VIOLIN_MESSAGE_PAYLOAD_VST_EVENTS_H

#include <array>
#include <type_traits>
#include <cassert>

#include "vst_header_chooser.h"

#include "message/payload/payload.h"

namespace violin
{
	namespace message
	{
		constexpr const std::size_t maximum_event_count = 16;

		template<typename> class unfolder;

		template<> struct payload<VstEvents>
		{
			payload(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt) noexcept :
				payload(static_cast<VstEvents*>(ptr))
			{

			}

			void copy(void* const ptr) const noexcept
			{

			}

		private:
			friend class unfolder<VstEvents>;

			explicit payload(VstEvents* const events) noexcept :
				m_null(!events)
			{
				if (!events)
				{
					return;
				}

				m_event_count = events->numEvents;

				assert(m_event_count <= maximum_event_count);

				for (std::size_t i = 0; i < m_event_count; ++i)
				{
					m_events[i] = *events->events[i];
				}
			}

			std::array<VstEvent, maximum_event_count> m_events;
			std::size_t m_event_count;
			bool m_null;
		};
	}
}

#endif // VIOLIN_MESSAGE_PAYLOAD_VST_EVENTS_H
