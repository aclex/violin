/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_PAYLOAD_STRING_H
#define VIOLIN_MESSAGE_PAYLOAD_STRING_H

#include <string>
#include <cstring>
#include <cassert>

#include "message/payload/payload.h"

#include "limit.h"

namespace violin
{
	namespace message
	{
		template<> struct payload<std::string>
		{
		public:
			explicit payload(const std::string& str) noexcept :
				payload(str.c_str(), str.length())
			{

			}

			payload(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt) noexcept :
				payload(static_cast<const char*>(ptr))
			{

			}

			explicit payload(const char* const payload, const std::size_t len = s_maximum_string_length - 1) noexcept
			{
				if (payload)
				{
					const std::size_t copy_len
					{
						(len < s_maximum_string_length - 1) ? len : safe_strlen(payload, s_maximum_string_length - 1)
					};

					std::strncpy(m_payload, payload, copy_len);
					m_payload[copy_len] = 0;

					m_len = copy_len;
				}
				else
				{
					m_payload[0] = 0;
					m_len = 0;
				}
			}

			char* payload_ptr() noexcept
			{
				return m_payload;
			}

			std::string str() const
			{
				return std::string(m_payload, m_len);
			}

			const char* c_str() const
			{
				return m_payload;
			}

			void copy(void* const ptr, const std::size_t max_length = s_maximum_string_length - 1) const noexcept
			{
				if (!ptr)
					return;

				const std::size_t copy_len { std::min(m_len, max_length) };
				char* const dest(static_cast<char* const>(ptr));
				std::strncpy(dest, m_payload, copy_len);
				dest[copy_len] = 0;
			}

		private:
			std::size_t safe_strlen(const char* str, const std::size_t max_len)
			{
				const char* end { static_cast<const char*>(std::memchr(str, 0, max_len)) };

				return end ? end - str : max_len;
			}

			static constexpr const std::size_t s_maximum_string_length = max_transfer_string_length;
			char m_payload[s_maximum_string_length];
			std::size_t m_len;
		};
	}
}

#endif // VIOLIN_MESSAGE_PAYLOAD_STRING_H
