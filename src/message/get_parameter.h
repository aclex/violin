/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MESSAGE_GET_PARAMETER_H
#define VIOLIN_MESSAGE_GET_PARAMETER_H

namespace violin
{
	namespace message
	{
		struct get_parameter
		{
			typedef float return_type;

			std::int32_t index;
			float value;

			constexpr explicit get_parameter(std::int32_t idx) noexcept :
				index(idx),
				value()
			{

			}

			constexpr return_type extract_result() const noexcept
			{
				return value;
			}
		};
	}
}

#endif // VIOLIN_MESSAGE_GET_PARAMETER_H
