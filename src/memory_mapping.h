/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_MEMORY_MAPPING_H
#define VIOLIN_MEMORY_MAPPING_H

#include <vector>
#include <cstddef>

#include "resource.h"

namespace violin
{
	class memory_mapping : resource
	{
	public:
		explicit memory_mapping(const int fd, const std::size_t size);

		memory_mapping(const memory_mapping&) = delete;
		memory_mapping(memory_mapping&&) = default;

		memory_mapping& operator=(const memory_mapping&) = delete;

		unsigned char* frame_pointer() const noexcept
		{
			return m_ptr + m_offset;
		}

		template<typename T> T* map() noexcept
		{
			T* const pointer(reinterpret_cast<T*>(frame_pointer()));
			map(sizeof(T));

			return pointer;
		}

		void map(const std::size_t size) noexcept
		{
			m_offset += size;
		}

		std::size_t size() const noexcept
		{
			return m_size;
		}

		void resize(const std::size_t new_size);

		std::vector<unsigned char> read() const;
		void write(void* const ptr) noexcept;

		~memory_mapping() noexcept;

	private:
		static unsigned char* mmap(const int fd, const std::size_t size);

		std::size_t m_size;
		std::size_t m_offset;
		unsigned char* m_ptr;
		const int m_fd;
	};
}

#endif // VIOLIN_MEMORY_MAPPING_H
