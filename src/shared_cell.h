/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_SHARED_CELL_H
#define VIOLIN_SHARED_CELL_H

#include "shared_state.h"

namespace violin
{
	template<typename, typename, state_process_scope, class> class shared_cell_transition;

	template
	<
		typename T,
		typename StateType,
		state_process_scope sc,
		class ThreadingTraits = threading_traits::std
	>
	class shared_cell : public shared_state<StateType, sc, ThreadingTraits>
	{
	public:
		typedef ThreadingTraits threading_traits;
		typedef shared_cell_transition<T, StateType, sc, threading_traits> transition_type;

		shared_cell() :
			shared_state<StateType, sc, ThreadingTraits>(StateType())
		{

		}

		transition_type request_transition(StateType const expected, StateType const next)
		{
			return transition_type(*this, expected, next);
		}

	private:
		friend transition_type;

		T m_value;
	};

	template
	<
		typename T,
		typename StateType,
		state_process_scope sc,
		class ThreadingTraits = threading_traits::std
	>
	class shared_cell_transition : public shared_state_transition<StateType, sc, ThreadingTraits>
	{
	public:
		typedef ThreadingTraits threading_traits;
		typedef shared_cell<T, StateType, sc, ThreadingTraits> cell_type;

		shared_cell_transition(cell_type& cell, StateType from, StateType to) :
			shared_state_transition<StateType, sc, threading_traits>(cell, from, to),
			m_value(cell.m_value)
		{

		}

		shared_cell_transition(const shared_cell_transition&) = delete;
		shared_cell_transition(shared_cell_transition&&) = default;

		shared_cell_transition& operator=(const shared_cell_transition&) = delete;

		T& value()
		{
			return m_value;
		}

	private:
		T& m_value;
	};
}

#endif // VIOLIN_SHARED_CELL_H
