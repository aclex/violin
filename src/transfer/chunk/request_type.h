/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef VIOLIN_TRANSFER_CHUNK_REQUEST_TYPE_H
#define VIOLIN_TRANSFER_CHUNK_REQUEST_TYPE_H

namespace violin
{
	namespace transfer
	{
		namespace chunk
		{
			enum class request_type : unsigned char
			{
				get,
				set
			};

			template<request_type t> constexpr inline const char request_infix[4] { "nop" };

			template<> constexpr inline const char request_infix<request_type::get>[] = "get";
			template<> constexpr inline const char request_infix<request_type::set>[] = "set";
		}
	}
}

#endif // VIOLIN_TRANSFER_CHUNK_REQUEST_TYPE_H
