/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TRANSFER_CHUNK_CHUNK_H
#define VIOLIN_TRANSFER_CHUNK_CHUNK_H

#include "vst_header_chooser.h"

#include "name.h"

#include "transfer/chunk/request_type.h"

namespace violin
{
	namespace transfer
	{
		namespace chunk
		{
			constexpr const std::size_t name_length(static_cast<std::size_t>(max_effect_name_length) +
				1 /* '_' */ +
				3 /* "get" or "set" */ +
				1 /* '_' */ +
				5 /* "chunk" */ +
				1 /* '_' */ +
				shm_name_suffix_length +
				1 /* null terminator */);

			class chunk
			{

			};

			template<request_type t> struct cookie
			{
				char name[name_length];
				static constexpr const request_type type = t;
			};
		}
	}
}

#endif // VIOLIN_TRANSFER_CHUNK_CHUNK_H
