/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TRANSFER_AUDIO_H
#define VIOLIN_TRANSFER_AUDIO_H

#include <memory>
#include <cstddef>

#include "shared_state.h"
#include "audio_buffer.h"
#include "processing_state.h"
#include "processing_format.h"
#include "name.h"

#include "shm/channel.h"

namespace violin
{
	namespace transfer
	{
		template<class Plugin> class audio
		{
		protected:
			typedef shared_state<processing_state, state_process_scope::shared> state_type;

			audio() noexcept :
				m_audio_state(),
				m_processing_format(),
				m_processing_size()
			{

			}

			template<class ShmChannelType>
			void resize_audio_buffer(ShmChannelType& shm_channel, std::size_t buffer_size)
			{
				static_cast<Plugin*>(this)->deinitialize_state();
				shm_channel.resize(calculate_audio_channel_size(buffer_size));
				static_cast<Plugin*>(this)->initialize_state();
				m_audio_buffer = std::make_unique<audio_buffer>(shm_channel.frame_pointer(), buffer_size, static_cast<Plugin*>(this)->input_count(), static_cast<Plugin*>(this)->output_count());
				shm_channel.map(m_audio_buffer->memory_size());
			}

			static std::string create_shm_name(const std::string& plugin_name, const std::string& infix, const std::string& suffix)
			{
				return violin::create_shm_name(audio_shm_name_prefix(plugin_name), infix, suffix);
			}

			static std::size_t initial_channel_size() noexcept
			{
				return sizeof(state_type) + sizeof(processing_format) + sizeof(std::size_t);
			}

			std::size_t calculate_audio_channel_size(std::size_t buffer_size) const noexcept
			{
				return initial_channel_size() + audio_buffer::memory_size(static_cast<const Plugin*>(this)->input_count(), static_cast<const Plugin*>(this)->output_count(), buffer_size);
			}

			void deinitialize_state() noexcept { }

			~audio() = default;

			state_type* m_audio_state;
			processing_format* m_processing_format;
			std::size_t* m_processing_size;
			std::unique_ptr<audio_buffer> m_audio_buffer;
		};
	}
}

#endif // VIOLIN_TRANSFER_AUDIO_H
