/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TRANSFER_DISPATCH_H
#define VIOLIN_TRANSFER_DISPATCH_H

#include "transfer/control.h"

#include "log.h"

namespace violin
{
	namespace transfer
	{
		namespace dispatch
		{
			class remote_dispatcher
			{
			public:
				explicit remote_dispatcher(violin::transfer::control::message_cell* const cell) noexcept :
					m_cell(cell)
				{

				}

				// general template methods versions, for any violin::message
				template<class MessageType, typename... Args>
				typename MessageType::return_type process(Args... args)
				{
					return process_message<MessageType, true, true>(std::forward<Args>(args)...);
				}

				template<class MessageType, typename... Args>
				typename MessageType::return_type get(Args... args)
				{
					return process_message<MessageType, false, true>(std::forward<Args>(args)...);
				}

				template<class MessageType, typename... Args>
				void set(Args... args)
				{
					return process_message<MessageType, true, false>(std::forward<Args>(args)...);
				}

				// special non-template function for violin::message::dispatch<void>,
				// as it's the most frequent use case
				std::intptr_t process(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					return process<void, false, false>(opcode, index, value, ptr, opt);
				}

				// more specialized template method versions for violin::message::dispatch<T>
				template<typename PayloadType> std::intptr_t get(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					return process<PayloadType, false, true>(opcode, index, value, ptr, opt);
				}

				template<typename PayloadType> std::intptr_t set(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					return process<PayloadType, true, false>(opcode, index, value, ptr, opt);
				}

				template<typename PayloadType> std::intptr_t process(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					return process<PayloadType, true, true>(opcode, index, value, ptr, opt);
				}

			private:
				template<typename PayloadType, bool DoPreCopy, bool DoPostCopy> std::intptr_t process(std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					return process_message<violin::message::dispatch<PayloadType>, DoPreCopy, DoPostCopy>(opcode, index, value, ptr, opt);
				}

				template
				<
					class MessageType,
					bool DoPreCopy,
					bool DoPostCopy,
					class... Args
				>
				typename MessageType::return_type process_message(Args... args)
				{
					{
						auto tr(m_cell->request_transition(processing_state::input, processing_state::process));

						if (tr.quitting())
						{
							return typename MessageType::return_type();
						}

						emplace<DoPreCopy, MessageType>(tr.value(), std::forward<Args>(args)...);
					}

					try
					{
						auto tr(m_cell->request_transition(processing_state::output, processing_state::input));

						if (tr.quitting())
						{
							return typename MessageType::return_type();
						}

						const auto& resp(std::get<MessageType>(tr.value()));

						if constexpr (DoPostCopy)
						{
							post_copy(resp, std::forward<Args>(args)...);
						}

						return resp.extract_result();
					}
					catch (const std::bad_variant_access&)
					{
						throw std::runtime_error("Peer misbehaves at dispatching.");
					}
				}

				template<bool, class MessageType, typename... Args>
				static void emplace(violin::message::message& c, Args... args)
				{
					c.emplace<MessageType>(std::forward<Args>(args)...);
				}

				template<bool DoPreCopy, template<class PayloadType> class MessageType>
				static void emplace(violin::message::message& c, std::int32_t opcode, std::int32_t index, std::intptr_t value, void* ptr, float opt)
				{
					if constexpr (DoPreCopy)
						c.emplace<MessageType>(opcode, index, value, ptr, opt);
					else
						c.emplace<MessageType>(opcode, index, value, nullptr, opt);
				}

				template<class MessageType, typename... Args>
				static void post_copy(const MessageType& m, Args... args)
				{
					// in general just do nothing
				}

				template<typename PayloadType>
				static void post_copy(const violin::message::dispatch<PayloadType>& m, std::int32_t, std::int32_t, std::intptr_t, void* ptr, float)
				{
					m.copy(ptr);
				}

#ifdef USE_VST2SDK
				void post_copy(const violin::message::dispatch<std::string>& m, std::int32_t opcode, std::int32_t, std::intptr_t, void* ptr, float)
				{
					switch (opcode)
					{
					case effGetProgramName:
					case effGetProgramNameIndexed:
						m.copy(ptr, kVstMaxProgNameLen);
						break;

					case effGetVendorString:
					case audioMasterGetVendorString:
						m.copy(ptr, kVstMaxVendorStrLen);
						break;

					case effGetProductString:
					case audioMasterGetProductString:
						m.copy(ptr, kVstMaxProductStrLen);
						break;

					case effGetEffectName:
					case effShellGetNextPlugin:
						m.copy(ptr, kVstMaxEffectNameLen);
						break;

					case effGetParamName:
					case effGetParamLabel:
					case effGetParamDisplay:
						m.copy(ptr, kVstExtMaxParamStrLen);
						break;

					default:
						m.copy(ptr);
						break;
					}
				}

				void post_copy(const violin::message::dispatch<VstSpeakerArrangement>& m, std::int32_t opcode, std::int32_t, std::intptr_t value, void* ptr, float)
				{
					m.copy(value, ptr);
				}
#else
				void post_copy(const violin::message::dispatch<std::string>& m, std::int32_t opcode, std::int32_t, std::intptr_t, void* ptr, float)
				{
					switch (opcode)
					{
					case effGetProgramName:
					case effGetVendorString:
					case audioMasterGetVendorString:
					case effGetProductString:
					case audioMasterGetProductString:
					case effGetEffectName:
					case effGetParamName:
						m.copy(ptr, max_name_length);
						break;

					default:
						m.copy(ptr);
						break;
					}
				}
#endif

				violin::transfer::control::message_cell* m_cell;
			};
		}
	}
}

#endif // VIOLIN_TRANSFER_DISPATCH_H
