/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_TRANSFER_CONTROL_H
#define VIOLIN_TRANSFER_CONTROL_H

#include <utility>

#include "shm/channel.h"
#include "shared_cell.h"
#include "processing_state.h"

#include "message/message.h"

namespace violin
{
	namespace transfer
	{
		namespace control
		{
			typedef shared_cell<violin::message::message, violin::processing_state, violin::state_process_scope::shared> message_cell;

			class basic_channel
			{
			public:
				basic_channel() = default;
				basic_channel(const basic_channel&) = delete;
				basic_channel(basic_channel&&) = default;

				basic_channel& operator=(const basic_channel&) = delete;

			protected:
				static std::string create_shm_name(const std::string& plugin_name, const std::string& infix, const std::string& suffix)
				{
					return violin::create_shm_name(control_shm_name_prefix(plugin_name), infix, suffix);
				}

				static constexpr std::size_t calculate_channel_size(const std::size_t stream_count) noexcept
				{
					return stream_count * sizeof(message_cell);
				}
			};

			template<class Host> class listener
			{
			public:
				void listen(message_cell* const stream)
				{
					while (true)
					{
						auto tr(stream->request_transition(processing_state::process, processing_state::output));

						if (tr.quitting())
						{
							break;
						}

						accessor::visit(this, tr.value());
					}
				}

			private:
				class accessor : public Host
				{
				public:
					static void visit(listener* const h, violin::message::message& m)
					{
						void (Host::* const fn)(violin::message::message&)(&accessor::visit);
						return (static_cast<Host*>(h)->*fn)(m);
					}

				private:
					using Host::visit;
				};
			};
		}
	}
}

#endif // VIOLIN_TRANSFER_CONTROL_H
