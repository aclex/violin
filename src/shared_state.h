/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_SHARED_STATE_H
#define VIOLIN_SHARED_STATE_H

#include <mutex>
#include <condition_variable>
#include <atomic>
#include <variant>

#include "resource.h"

#include "message/message.h"

#include "process_shared.h"

#include "threading_traits/std.h"

namespace violin
{
	enum class state_process_scope : unsigned char
	{
		local,
		shared
	};

	template<typename, state_process_scope, class> class shared_state_transition;

	template
	<
		typename StateType,
		state_process_scope sc,
		class ThreadingTraits = threading_traits::std
	>
	class shared_state
	{
	public:
		typedef ThreadingTraits threading_traits;
		typedef shared_state_transition<StateType, sc, threading_traits> transition_type;

		explicit shared_state(StateType initial_state) :
			m_quitting(false),
			m_current_state(initial_state)
		{
			apply_process_scope();
		}

		transition_type request_transition(StateType expected, StateType next)
		{
			return transition_type(*this, expected, next);
		}

		void quit()
		{
			m_quitting.store(true);

			m_cond.notify_all();
		}

	private:
		friend transition_type;

		void apply_process_scope()
		{
			if constexpr (sc == state_process_scope::shared)
			{
				set_process_shared(m_mutex);
				set_process_shared(m_cond);
			}
		}

		typename threading_traits::lockable_type m_mutex;
		typename threading_traits::cv_type m_cond;
		std::atomic<bool> m_quitting;

		StateType m_current_state;
	};

	template
	<
		typename StateType,
		state_process_scope sc,
		class ThreadingTraits = threading_traits::std
	>
	class shared_state_transition : protected violin::resource
	{
	public:
		typedef ThreadingTraits threading_traits;
		typedef shared_state<StateType, sc, ThreadingTraits> state_type;

		shared_state_transition(state_type& state_object, StateType from, StateType to) :
			m_state_object(state_object),
			m_lock(state_object.m_mutex),
			m_quitting(false),
			m_from(from),
			m_to(to)
		{
			m_state_object.m_cond.wait(m_lock,
				[this]()
				{
					return m_state_object.m_current_state == m_from || (m_quitting = m_state_object.m_quitting.load());
				});
		}

		shared_state_transition(const shared_state_transition&) = delete;
		shared_state_transition(shared_state_transition&&) = default;

		shared_state_transition& operator=(const shared_state_transition&) = delete;

		bool quitting() const noexcept
		{
			return m_quitting;
		}

		~shared_state_transition() noexcept
		{
			if (!owning())
				return;

			if (!m_quitting)
				m_state_object.m_current_state = m_to;

			m_state_object.m_cond.notify_all();
		}

	private:
		state_type& m_state_object;
		std::unique_lock<typename threading_traits::lockable_type> m_lock;
		bool m_quitting;
		const StateType m_from, m_to;
	};
}

#endif // VIOLIN_SHARED_STATE_H
