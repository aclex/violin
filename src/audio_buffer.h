/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_AUDIO_BUFFER_H
#define VIOLIN_AUDIO_BUFFER_H

#include <cstddef>

namespace violin
{
	template<typename WidestSampleType> class basic_audio_buffer
	{
	public:
		basic_audio_buffer(unsigned char* pointer, std::size_t buffer_size, std::size_t input_channel_count, std::size_t output_channel_count) noexcept :
			m_buffer_size(buffer_size),
			m_input_channel_count(input_channel_count),
			m_output_channel_count(output_channel_count),
			m_memory(pointer),
			m_inputs(collect_inputs(pointer, buffer_size, input_channel_count)),
			m_outputs(collect_outputs(pointer, buffer_size, input_channel_count, output_channel_count))
		{

		}

		template<typename SampleType> SampleType** inputs() noexcept
		{
			return reinterpret_cast<SampleType**>(m_inputs.data());
		}

		template<typename SampleType> SampleType** outputs() noexcept
		{
			return reinterpret_cast<SampleType**>(m_outputs.data());
		}

		template<typename SampleType> SampleType* input(std::size_t index) noexcept
		{
			return reinterpret_cast<SampleType*>(m_memory + index * m_buffer_size * sizeof(WidestSampleType));
		}

		template<typename SampleType> SampleType* output(std::size_t index) noexcept
		{
			return reinterpret_cast<SampleType*>(m_memory + (m_input_channel_count + index) * m_buffer_size * sizeof(WidestSampleType));
		}

		std::size_t buffer_size() const noexcept
		{
			return m_buffer_size;
		}

		std::size_t memory_size() const noexcept
		{
			return memory_size(m_input_channel_count, m_output_channel_count, m_buffer_size);
		}

		static constexpr std::size_t memory_size(std::size_t input_channel_count, std::size_t output_channel_count, std::size_t buffer_size) noexcept
		{
			return sizeof(WidestSampleType) * buffer_size * (input_channel_count + output_channel_count);
		}

	private:
		static std::vector<unsigned char*> collect_array(unsigned char* pointer, const std::size_t buffer_size, const std::size_t channel_count)
		{
			std::vector<unsigned char*> result;
			result.reserve(channel_count);

			for (size_t i = 0; i < channel_count; ++i)
				result.push_back(pointer + i * buffer_size * sizeof(WidestSampleType));

			return result;
		}

		static constexpr std::size_t outputs_offset(const std::size_t input_channel_count, const std::size_t buffer_size) noexcept
		{
			return input_channel_count * buffer_size * sizeof(WidestSampleType);
		}

		static std::vector<unsigned char*> collect_inputs(unsigned char* pointer, const std::size_t buffer_size, const std::size_t input_channel_count)
		{
			return collect_array(pointer, buffer_size, input_channel_count);
		}

		static std::vector<unsigned char*> collect_outputs(unsigned char* pointer, const std::size_t buffer_size, const std::size_t input_channel_count, const std::size_t output_channel_count)
		{
			return collect_array(pointer + outputs_offset(buffer_size, input_channel_count), buffer_size, output_channel_count);
		}

		const std::size_t m_buffer_size;
		const std::size_t m_input_channel_count, m_output_channel_count;
		std::vector<unsigned char*> m_inputs, m_outputs;

		unsigned char* const m_memory;
	};

	using audio_buffer = basic_audio_buffer<double>;
}

#endif // VIOLIN_AUDIO_BUFFER_H
