/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "shm/file.h"

#include <stdexcept>
#include <cstring>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

using namespace violin;
using namespace violin::shm;

string file::name() const
{
	return m_name;
}

int file::get_fd() const noexcept
{
	return m_fd;
}

size_t file::size() const noexcept
{
	return m_size;
}

void file::resize(const size_t new_size)
{
	ftruncate(m_fd, new_size);
}

int file::open(const string& name, const int open_flags)
{
	const int fd(::shm_open(name.c_str(), open_flags, 0600));

	if (fd == -1)
	{
		const string& msg = "Opening shared memory failed: " + string(strerror(errno));
		throw runtime_error(msg);
	}

	return fd;
}

void file::ftruncate(const int fd, const size_t new_size)
{
	const int result(::ftruncate(fd, new_size));

	if (result == -1)
	{
		const string& msg = "Resizing shared memory region failed: " + string(strerror(errno));
		throw runtime_error(msg);
	}
}

int file::collect_flags(const creation cr) noexcept
{
	switch (cr)
	{
	case creation::disabled:
		return O_RDWR;

	case creation::enabled:
		return O_CREAT | O_EXCL | O_RDWR;
	}
}

file::~file() noexcept
{
	if (!owning())
	{
		return;
	}

	shm_unlink(m_name.c_str());
}
