/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_SHM_CHANNEL_H
#define VIOLIN_SHM_CHANNEL_H

#include <string>
#include <cstddef>

#include "resource.h"

#include "memory_mapping.h"

#include "shm/file.h"

namespace violin
{
	namespace shm
	{
		enum class resizing : unsigned char
		{
			disabled,
			remap,
			full
		};

		template<creation cr, resizing rs> class channel
		{
		public:
			explicit channel(const std::string& shm_name, std::size_t size) :
				m_file(shm_name, size, cr),
				m_mapping(m_file.get_fd(), size)
			{

			}

			channel(const channel&) = delete;
			channel(channel&&) = default;

			std::string name() const
			{
				return m_file.name();
			}

			std::size_t size() const noexcept
			{
				return m_mapping.size();
			}

			template<typename T> T* map() noexcept
			{
				return m_mapping.map<T>();
			}

			void map(const std::size_t size) noexcept
			{
				m_mapping.map(size);
			}

			channel& operator=(const channel&) = delete;

			unsigned char* frame_pointer() const noexcept
			{
				return m_mapping.frame_pointer();
			}

			std::vector<unsigned char> read() const
			{
				return m_mapping.read();
			}

			void write(void* const ptr) noexcept
			{
				m_mapping.write(ptr);
			}

			template<creation ccr> using can_create = std::enable_if_t<ccr == creation::enabled>;
			template<resizing crs> using can_resize = std::enable_if_t<crs != resizing::disabled>;

			template<typename T, typename... Args, creation ccr = cr, typename = can_create<ccr>>
			T* make_placed(Args... args) noexcept(noexcept(T(std::forward<Args>(args)...)))
			{
				return new (this->map<T>()) T(std::forward<Args>(args)...);
			}

			template<resizing crs = rs, typename = can_resize<crs>>
			void resize(const std::size_t new_size)
			{
				if constexpr (rs == resizing::full)
				{
					m_file.resize(new_size);
				}

				m_mapping.resize(new_size);
			}

		private:
			file m_file;
			memory_mapping m_mapping;
		};
	}
}

#endif // VIOLIN_SHM_CHANNEL_H
