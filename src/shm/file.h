/*
  Violin
  Copyright (C) 2018  Alexey Chernov

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VIOLIN_SHM_FILE_H
#define VIOLIN_SHM_FILE_H

#include <string>
#include <cstddef>

#include "resource.h"

namespace violin
{
	namespace shm
	{
		enum class creation : bool
		{
			disabled,
			enabled
		};

		class file : resource
		{
		public:
			file(const std::string& name, const std::size_t size = 0, const creation cr = creation::disabled) :
				m_name(name),
				m_size(size),
				m_fd(open(name, collect_flags(cr)))
			{
				if (m_size)
					resize(m_size);
			}

			file(const file&) = delete;
			file(file&&) = default;

			file& operator=(const file&) = delete;

			std::string name() const;
			int get_fd() const noexcept;
			std::size_t size() const noexcept;
			void resize(const std::size_t new_size);

			~file() noexcept;

		private:
			static int open(const std::string& name, const int open_flags);
			static void ftruncate(const int fd, const std::size_t new_size);

			static int collect_flags(const creation cr) noexcept;

			const std::string m_name;
			std::size_t m_size;
			const int m_fd;
		};
	}
}

#endif // VIOLIN_SHM_FILE_H
